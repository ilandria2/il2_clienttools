@echo off
rem SETLOCAL EnableDelayedExpansion
set cmdzip="c:\Program Files\7-zip\7z.exe"
set pubdir=c:\_mydocs\games\Neverwinter Nights To publish\

rem delete publish folder
echo   Force deleting "%pubdir%*"
rmdir /S /Q "%pubdir%"

rem create publish folder
echo   Creating "%pubdir%*"
mkdir "%pubdir%"

REM copy definition files to publish folder
echo   Copying definition files (*.xml) to "%pubdir%"
copy /Y "il2_admintools.xml" "%pubdir%" >nul
copy /Y "il2_clienttools.xml" "%pubdir%" >nul
copy /Y "il2_content.xml" "%pubdir%" >nul
copy /Y "il2_content_test.xml" "%pubdir%" >nul
del /F /Q "il2_admintools.xml" >nul
del /F /Q "il2_clienttools.xml" >nul
del /F /Q "il2_content.xml" >nul
del /F /Q "il2_content_test.xml" >nul

REM copy updater explicitly to publish folder
echo   Copying il2_updater.exe to "%pubdir%"
copy /Y "il2_updater.exe" "%pubdir%" >nul

rem zip Client tools
cd \_mydocs\games\Neverwinter Nights Client tools\
call :treeProcess

rem zip Admin tools
cd \_mydocs\games\Neverwinter Nights Admin tools\
call :treeProcess

rem zip Content TEST
set pubdir=c:\_mydocs\games\Neverwinter Nights To publish\TEST\
cd \_mydocs\games\Neverwinter Nights TEST\
call :treeProcess

rem zip Content PUBLIC
set pubdir=c:\_mydocs\games\Neverwinter Nights To publish\PUBLIC\
cd \_mydocs\games\Neverwinter Nights PUBLIC\
call :treeProcess


goto :eof


rem ******************************

rem zip each non-zip file
:treeProcess
	echo   Processing "%cd%"
	for /R %%f in (*) do (
		set fullfilepath=%%f
		set fullfile=%%~nxf
		set file=%%~nf
		set exten=%%~xf
		set pubfilezip=%pubdir%%%~nf.zip
		set pubfile=%pubdir%%%~nxf
		call :compress_or_skip
	)

	exit /b

:compress_or_skip
	if not "%EXTEN%"==".zip" (
		echo   - Compressing "%fullfile%"
		%cmdzip% a "%pubfilezip%" "%fullfilepath%" -y >nul
	) else (
		echo   - Copying "%fullfile%"
		copy /Y "%fullfilepath%" "%pubfile%" >nul
	)
	exit /b
