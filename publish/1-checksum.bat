REM copy latest versionf of the admin and client tools to the source folders
copy /Y il2_launcher.exe "..\..\..\games\Neverwinter Nights Client tools\"
copy /Y il2_updater.exe "..\..\..\games\Neverwinter Nights Client tools\"
copy /Y il2_corelib.dll "..\..\..\games\Neverwinter Nights Client tools\"
copy /Y il2_resources.dll "..\..\..\games\Neverwinter Nights Client tools\"
copy /Y il2_admin.exe "..\..\..\games\Neverwinter Nights Admin tools\"
copy /Y il2_corelib.dll "..\..\..\games\Neverwinter Nights Admin tools\"

REM calculate checksum for client and admin content
il2_admin.exe -b -c -f "c:\_mydocs\games\Neverwinter Nights Admin tools\il2_admintools.xml"
il2_admin.exe -b -c -f "c:\_mydocs\games\Neverwinter Nights Client tools\il2_clienttools.xml"
il2_admin.exe -b -c -f "c:\_mydocs\games\Neverwinter Nights PUBLIC\il2_content.xml"
il2_admin.exe -b -c -f "c:\_mydocs\games\Neverwinter Nights TEST\il2_content_test.xml"

REM copy definition files to here
copy /Y "c:\_mydocs\games\Neverwinter Nights Admin tools\il2_admintools.xml" .
copy /Y "c:\_mydocs\games\Neverwinter Nights Client tools\il2_clienttools.xml" .
copy /Y "c:\_mydocs\games\Neverwinter Nights PUBLIC\il2_content.xml" .
copy /Y "c:\_mydocs\games\Neverwinter Nights TEST\il2_content_test.xml" .

REM delete old definition files
del "c:\_mydocs\games\Neverwinter Nights Admin tools\il2_admintools.xml"
del "c:\_mydocs\games\Neverwinter Nights Client tools\il2_clienttools.xml"
del "c:\_mydocs\games\Neverwinter Nights PUBLIC\il2_content.xml"
del "c:\_mydocs\games\Neverwinter Nights TEST\il2_content_test.xml"
