﻿
namespace il2_clienttools
{
    #region Namespaces

    using il2_clienttools.Models;
    using Microsoft.Win32;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Security;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Xml;

    #endregion

    public static class Procedures
    {
        public static bool CheckAndRepairWebLaunch(DirectoryInfo asmDir)
        {
            var asmPath = new FileInfo(Path.Combine(asmDir.FullName, "il2_updater.exe"));
            const string METHOD = "CheckAndRepairWebLaunch";
            Helpers.DebugLog(METHOD, string.Empty, "Enter with:", asmPath);

            if (!asmPath.Exists)
            {
                Helpers.DebugLog(METHOD, "Error - file not exists");
                il2_updater.FatalErrors++;
                return false;
            }

            var path = Helpers.ReadRegistryKey(RegistryHive.ClassesRoot, @"il2\Shell\Open\Command", null);
            Helpers.DebugLog(METHOD, "Read registry key: {1}\\{1} = {2}", RegistryHive.ClassesRoot, @"il2\Shell\Open\Command", path);

            if (path == null || !path.Contains("il2_updater.exe"))
            {
                Helpers.DebugLog(METHOD, "path is null or doesnt contain il2_updater.exe - prepare to install uri scheme");

                var install = InstallURIScheme("il2", asmPath.FullName, "\"%1\" \"%2\" \"%3\"");
                
                Helpers.DebugLog(METHOD, "install result of the 'il2' uri scheme to: {0} = {1}", asmPath.FullName, install);

                if (!install)
                {
                    il2_updater.FatalErrors++;
                    return false;
                }
            }

            return true;
        }

        public static bool InstallURIScheme(string name, string path, string args)
        {
            const string METHOD = "InstallURIScheme";
            Helpers.DebugLog(METHOD, string.Empty, "Enter with:", name, path, args);

            try
            {
                Helpers.DebugLog(METHOD, "Setting multiple registry keys in {0}\\{1}***", RegistryHive.ClassesRoot, name);
                Helpers.SetRegistryKey(RegistryHive.ClassesRoot, name, null, "URL:" + name + " Protocol");
                Helpers.SetRegistryKey(RegistryHive.ClassesRoot, name, "URL Protocol", "");
                Helpers.SetRegistryKey(RegistryHive.ClassesRoot, name + @"\DefaultIcon", "", "\"" + path.Replace("\"", "") + ",1\"");
                Helpers.SetRegistryKey(RegistryHive.ClassesRoot, name + @"\Shell\Open\Command", "", "\"" + Path.GetFileName(path) + "\" " + args);
            }
            catch (Exception ex)
            {

                if (ex is UnauthorizedAccessException ||
                    ex is SecurityException)
                {
                    Helpers.DebugLog(METHOD, "Access denied - preparing restart cmd line");
                    Helpers.ConsoleWrite(Helpers.C_NEU, 0, true, false, "Access denied");
                    Helpers.ConsoleWrite(Helpers.C_NEU, 1, true, false, "Application will now restart in Elevated access");
                    Thread.Sleep(1000);

                    Helpers.DelayCmdLine(1, Path.GetFileName(path), new DirectoryInfo(Path.GetDirectoryName(path)), true);
                    Finish(DontWaitInDebug: true);
                }

                else
                {
                    Helpers.DebugLog(METHOD, "Error occured: {0} - {1}", ex.GetType().Name, ex.Message);
                    return false;
                }
            }

            return true;
        }

        public static bool PerformCheck<T1>(Func<T1, bool> check, T1 arg1, string message)
        {
            string method = string.Format("{0}({1})", check.Method.Name, Helpers.ObjectsToString(arg1));
            return PerformCheck(() => check(arg1), message, method);
        }

        public static bool PerformCheck<T1, T2>(Func<T1, T2, bool> check, T1 arg1, T2 arg2, string message)
        {
            string method = string.Format("{0}({1}, {2})", check.Method.Name, Helpers.ObjectsToString(arg1), Helpers.ObjectsToString(arg2));
            return PerformCheck(() => check(arg1, arg2), message, method);
        }

        public static bool PerformCheck(Func<bool> check, string message)
        {
            string method = string.Format("{0}()", check.Method.Name);
            return PerformCheck(check, message, method);
        }

        private static bool PerformCheck(Func<bool> check, string message, string method)
        {
            const string METHOD = "PerformCheck";

            Helpers.DebugLog(METHOD, "Enter: {0}", method, message);

            if (!string.IsNullOrWhiteSpace(message))
            {
                Helpers.ConsoleWrite(Helpers.C_STD, 0, false, true, message);

                if (il2_updater.PreviousChecksErrored)
                {
                    Helpers.DebugLog(METHOD, "Previous check errored - returning false");
                    Helpers.ConsoleWrite(Helpers.C_NEU, 0, true, false, "Skipped");
                    return false;
                }
            }

            var checkResult = false;

            if (!il2_updater.PreviousChecksErrored)
            {
                Helpers.DebugLog(METHOD, "Invoking: {0}", method);
                checkResult = check();
                Helpers.DebugLog(METHOD, "Check result: " + checkResult);

                il2_updater.PreviousChecksErrored = !checkResult;

                if (!string.IsNullOrWhiteSpace(message))
                    Helpers.ConsoleWrite(checkResult ? Helpers.C_POS : Helpers.C_NEG, 0, true, false, checkResult ? "Success" : "Failure");
            }

            return checkResult;
        }

        public static bool CheckOperatingSystem()
        {
            const string METHOD = "CheckOperatingSystem";
            Helpers.DebugLog(METHOD, string.Empty, "Enter");

            var check = Environment.OSVersion.Version.Major >= 6;

            Helpers.DebugLog(METHOD, "OS version check: {0} ({1})", Environment.OSVersion.Version.Major, check);

            if (!check)
                il2_updater.FatalErrors++;

            return check;
        }

        public static bool CheckWorkingDirectory(DirectoryInfo baseDir, string asmDir)
        {
            const string METHOD = "CheckWorkingDirectory";
            Helpers.DebugLog(METHOD, string.Empty, "Enter with:", baseDir.FullName, asmDir);
            Helpers.ConsoleWrite(Helpers.C_STD, 0, false, true, "Checking Working directory");

            var check = Helpers.CompareDirectoryName(baseDir.FullName, asmDir);
            Helpers.DebugLog(METHOD, "Compared directories [{0}] with [{1}]: {2}", baseDir.FullName, asmDir, check);

            Helpers.ConsoleWrite(check ? Helpers.C_POS : Helpers.C_NEU, 0, true, false, check ? "Success" : "Failure");

            if (!check)
            {
                Helpers.DebugLog(METHOD, "Prepare to change working directory to: {0}", baseDir);
                ChangeWorkingDirectory(baseDir);
            }

            return check;
        }

        public static bool CheckGameBase(DirectoryInfo baseDir)
        {
            const string METHOD = "CheckGameBase";
            Helpers.DebugLog(METHOD, string.Empty, "Enter: ", baseDir);

            if (baseDir != null)
            {
                var check = File.Exists(Path.Combine(baseDir.FullName, "nwmain.exe"));
                Helpers.DebugLog(METHOD, "Chec file: [{0}] = {1}", Path.Combine(baseDir.FullName, "nwmain.exe"), check);

                if (!check)
                    il2_updater.FatalErrors++;

                return check;
            }

            Helpers.DebugLog(METHOD, "Error - base dir is null");
            il2_updater.FatalErrors++;
            return false;
        }

        public static void SetBaseDir(DirectoryInfo baseDir)
        {
            const string METHOD = "SetBaseDir";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", baseDir);

            var key = Environment.Is64BitOperatingSystem ?
                @"SOFTWARE\Wow6432Node\BioWare\NWN\Neverwinter" :
                @"SOFTWARE\BioWare\NWN\Neverwinter";

            Helpers.DebugLog(METHOD, "registry key path to check = {0}", key);

            try
            {
                Helpers.DebugLog(METHOD, "Setting multiple registry keys...");
                Helpers.SetRegistryKey(RegistryHive.LocalMachine, key, "Location", baseDir.FullName);
                Helpers.SetRegistryKey(RegistryHive.LocalMachine, key, "Path", baseDir.FullName);
                Helpers.SetRegistryKey(RegistryHive.LocalMachine, key, "Version", "1.69");
            }

            catch (SecurityException)
            {
                Helpers.DebugLog(METHOD, "Access denied - preparing to restart in elevated access");
                Helpers.ConsoleWrite(Helpers.C_NEU, 1, true, false, "Application will now restart in Elevated access");
                Thread.Sleep(1000);

                Helpers.DelayCmdLine(1, Helpers.AssemblyFileName, new DirectoryInfo(Helpers.AssemblyDirectory), true);
                //Finish(DontWaitInDebug: true);
                Finish();
            }
        }

        public static void HandleException(Exception ex)
        {
            const string METHOD = "HandleException";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", ex.GetType().FullName, ex.Message, ex.InnerException == null ? string.Empty : ex.InnerException.GetType().FullName + ex.InnerException.Message);
            Helpers.ConsoleWrite(Helpers.C_NEG, 1, true, false, "Unhandled error [{0}] occured:\n{1}", ex.GetType().FullName, ex.Message);
        }

        public static void ChangeWorkingDirectory(DirectoryInfo workDir)
        {
            const string METHOD = "ChangeWorkingDirectory";

            Helpers.DebugLog(METHOD, string.Empty, "Entering with work dir: ", workDir.FullName);

            if (il2_updater.PreviousChecksErrored)
                return;

            if (File.Exists(Path.Combine(workDir.FullName, "il2_updater.exe")))
            {
                Helpers.DebugLog(METHOD, "Deleting existing file: {0}", Path.Combine(workDir.FullName, "il2_updater.exe"));
                File.Delete(Path.Combine(workDir.FullName, "il2_updater.exe"));
            }

            Helpers.DebugLog(METHOD, "Copying [{0}] to [{1}]", Path.Combine(Helpers.AssemblyDirectory, Helpers.AssemblyFileName), Path.Combine(workDir.FullName, "il2_updater.exe"));
            File.Copy(Path.Combine(Helpers.AssemblyDirectory, Helpers.AssemblyFileName), Path.Combine(workDir.FullName, "il2_updater.exe"));

            string cmdline = "il2_updater.exe";

            Helpers.ConsoleWrite(Helpers.C_STD, 1, true, false, "Changing working directory:");

            Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, "From:\t");
            Helpers.ConsoleWrite(Helpers.C_NEU, 0, true, false, Helpers.AssemblyDirectory);

            Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, "To:\t");
            Helpers.ConsoleWrite(Helpers.C_POS, 0, true, false, workDir.FullName);

            Thread.Sleep(1000);
            Helpers.DebugLog(METHOD, "Preparing cmd line: [{0}] on dir [{1}]", cmdline, workDir);
            Helpers.DelayCmdLine(1, cmdline, workDir, false);
            Finish(DontWaitInDebug: true);
        }

        public static void Start()
        {
            const string METHOD = "Start";
            Helpers.DebugLog(METHOD, string.Empty, "Enter - clearing console and setting title to assembly title");
            Console.Clear();
            Console.Title = Helpers.AssemblyTitle;
            Helpers.ConsoleWrite(Helpers.C_STD, 0, false, false, "--- Starting ");
            Helpers.ConsoleWrite(Helpers.C_NEU, 0, false, false, Helpers.AssemblyTitle);
            Helpers.ConsoleWrite(Helpers.C_STD, 0, true, false, " ---");

            Helpers.ConsoleWrite(Helpers.C_STD, 1, false, true, "Working directory");

            var workingDirectory = Helpers.GetShortPathString(Helpers.AssemblyDirectory, 40);
            Helpers.DebugLog(METHOD, "Printing short version of working directory path: {0}", workingDirectory);

            Helpers.ConsoleWrite(Helpers.C_NEU, 0, true, false, workingDirectory);
        }

        public static void Finish(bool DontWaitInDebug = false)
        {
            const string METHOD = "Finish";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", DontWaitInDebug);
            Helpers.ConsoleWrite(Helpers.C_STD, 1, false, false, "--- Finished ");
            Helpers.ConsoleWrite(Helpers.C_NEU, 0, false, false, Helpers.AssemblyTitle);
            Helpers.ConsoleWrite(Helpers.C_STD, 0, true, false, " ---");

#if DEBUG
            if (il2_updater.FatalErrors == 0)
            {
                if (!DontWaitInDebug)
                {
                    Helpers.DebugLog(METHOD, "DEBUG ENV ONLY - Console.ReadKey()");
                    Helpers.ConsoleWrite(Helpers.C_PAR, 2, true, false, "--- Press any key to exit ---");
                    Console.ForegroundColor = Helpers.C_DEF;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ReadKey();
                }
            }
#endif

            if (il2_updater.FatalErrors > 0)
            {
                Helpers.DebugLog(METHOD, "There were errors during processing - Console.ReadKey and return -1");
                Helpers.ConsoleWrite(Helpers.C_NEG, 2, true, false, "--- Errors occured during execution - Press any key to exit ---");
                Console.ForegroundColor = Helpers.C_DEF;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ReadKey();
                Environment.Exit(-1);
            }
            else
                Environment.Exit(0);
        }

        public static bool StartLauncher()
        {
            const string METHOD = "StartLauncher";
            Helpers.DebugLog(METHOD, string.Empty, "Enter");

            if (il2_updater.PreviousChecksErrored)
                return false;

            Helpers.ConsoleWrite(Helpers.C_STD, 1, true, false, "Starting Launcher...");

            try
            {
                var launcher = Path.Combine(Helpers.AssemblyDirectory, "il2_launcher.exe");
                var args = @"/noupdate";
                Helpers.DebugLog(METHOD, "Launcher dir: {0} args: {1}", launcher, args);

                using (var process = new Process())
                {
                    var psi = new ProcessStartInfo(launcher, args);
                    process.StartInfo = psi;

                    Helpers.DebugLog(METHOD, "Starting new process...");
                    var exec = process.Start();

                    Helpers.DebugLog(METHOD, "Process execution result: {0}", exec);
                    return exec;
                }
            }
            catch
            {
                Helpers.DebugLog(METHOD, "Error occured");
                il2_updater.FatalErrors++;
                return false;
            }
        }

        public static bool DownloadFile(FileInfo file)
        {
            const string METHOD = "DownloadFile";

            try
            {
                Helpers.DebugLog(METHOD, string.Empty, "Enter:", file);

                using (var webClient = new WebClient())
                {
                    var remoteFileName = file.Name;
                    FileInfo localFile = file;

                    Helpers.DebugLog(METHOD, "Remote file [{0}], local file [{1}]", remoteFileName, localFile);
                    Helpers.DebugLog(METHOD, "Check if local file exists: {0}", localFile.Exists);

                    if (localFile.Exists)
                        localFile.Delete();

                    Helpers.DebugLog(METHOD, "Download file: {0}/{1} as {2}", Helpers.URI_FTP, remoteFileName, localFile.FullName);
                    webClient.DownloadFile(Helpers.URI_FTP + "/" + remoteFileName, localFile.FullName);

                    Helpers.DebugLog(METHOD, "Refreshing local file");
                    localFile.Refresh();
                }
            }
            catch
            {
                Helpers.DebugLog(METHOD, "Error occured");
                return false;
            }

            return true;
        }

        public static bool CheckForUpdates(bool SelfOnly, IList<Model_FileItemLight> RemoteFileItems)
        {
            const string METHOD = "CheckForUpdates";

            Helpers.DebugLog(METHOD, string.Empty, "Enter:", SelfOnly, RemoteFileItems);
            Helpers.DebugLog(METHOD, "SelfOnly: {0}; DebuggerIsAttached: {1}", SelfOnly, Debugger.IsAttached);
            if (SelfOnly && Debugger.IsAttached) 
                return true;

            var error = false;

            Helpers.ConsoleWrite(Helpers.C_STD, 1, true, false, "Checking for updates ({0}):", SelfOnly ? "Self" : "Client tools");

            var row = 0;
            var part = string.Empty;
            var msg = string.Empty;

            foreach (var item in RemoteFileItems)
            {
                Helpers.DebugLog(METHOD, "Iterate item [{0}]", item);
                part = string.Format("{0:000} - ", ++row);
                msg = item.FileInfo.Name;

                Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, part);
                Helpers.ConsoleWrite(Helpers.C_STD, 0, false, false, msg);

                if ((SelfOnly && !Helpers.CompareFileName(Helpers.AssemblyFileName, item.FileInfo.Name)) ||
                    (!SelfOnly && Helpers.CompareFileName(Helpers.AssemblyFileName, item.FileInfo.Name)))
                {
                    Helpers.DebugLog(METHOD, "Skipping item [{0}]", item);
                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, "".PadRight(Helpers.DOTS - part.Length - msg.Length, '.'));
                    Helpers.ConsoleWrite(Helpers.C_NEU, 0, true, false, "Skipped");
                    continue;
                }

                var compare = false;
                var download = false;
                var unpack = false;

                compare = Procedures.CompareLocalFileAgainstRemote(item.FileInfo, RemoteFileItems);
                Helpers.DebugLog(METHOD, "Compare local against remote: {0}", compare);

                if (compare)
                {
                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, "".PadRight(Helpers.DOTS - part.Length - msg.Length, '.'));
                }

                else
                {
                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, false, ":");
                    Helpers.ConsoleWrite(Helpers.C_PAR, 1, false, true, "    - Checking local file");
                }

                Helpers.ConsoleWrite(compare ? Helpers.C_POS : Helpers.C_NEU, 0, true, false, compare ? "Success" : "Update needed");

                if (!compare)
                {
                    Helpers.DebugLog(METHOD, "Preparing to download remote file...");
                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, true, "    - Downloading remote file");

                    var zipFile = new FileInfo(item.FileInfo.FullName.Replace(item.FileInfo.Extension, ".zip"));
                    var localFile = item.FileInfo;

                    Helpers.DebugLog(METHOD, "zipFile: {0}", zipFile.FullName);
                    Helpers.DebugLog(METHOD, "localFile: {0}", localFile.FullName);
                    download = DownloadFile(zipFile);

                    Helpers.ConsoleWrite(download ? Helpers.C_POS : Helpers.C_NEG, 0, true, false, download ? "Success" : "Failure");
                    Helpers.DebugLog(METHOD, "Download result: {0}", download);

                    if (!download)
                    {
                        error = true;
                        break;
                    }

                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, true, "    - Unpacking remote file");

                    var same = Helpers.CompareFileName(localFile.Name, Helpers.AssemblyFileName);

                    Helpers.DebugLog(METHOD, "Compare file names: {0} vs {1} - {2}", localFile.Name, Helpers.AssemblyFileName, same ? "Same" : "Different");

                    if (same)
                        localFile = new FileInfo(localFile.FullName.Replace(localFile.Extension, localFile.Extension + ".new"));

                    Helpers.DebugLog(METHOD, "Change local file due to being same - New name: {0}", localFile.FullName);

                    localFile = new FileInfo(localFile.FullName.Replace(localFile.DirectoryName, Helpers.AssemblyDirectory));

                    Helpers.DebugLog(METHOD, "Final local file path: {0}", localFile.FullName);

                    unpack = UnpackFile(zipFile, localFile);

                    Helpers.DebugLog(METHOD, "Unpack file result: {0}", unpack);
                    Helpers.ConsoleWrite(unpack ? Helpers.C_POS : Helpers.C_NEG, 0, true, false, unpack ? "Success" : "Failure");

                    if (!unpack)
                    {
                        error = true;
                        break;
                    }
                    else
                        zipFile.Delete();

                    Helpers.ConsoleWrite(Helpers.C_PAR, 0, false, true, "    - Checking remote file");

                    compare = CompareLocalFileAgainstRemote(localFile, RemoteFileItems);
                    Helpers.DebugLog(METHOD, "Compare local against remote: {0}", compare);

                    if (!compare)
                    {
                        //Helpers.OpenUrl("http://www.ilandria.com/il2_clienttools_error1.html");

                        error = true;
                    }

                    if (same)
                    {
                        if (compare)
                        {
                            Helpers.ConsoleWrite(compare ? Helpers.C_NEU : Helpers.C_NEG, 0, true, false, "Assembly restart needed");
                            Helpers.ConsoleWrite(Helpers.C_NEU, 1, true, false, "Application will now restart to install it's new updated version.");
                            Thread.Sleep(1000);

                            var cmd = string.Empty;

                            cmd = "move /Y il2_updater.exe.new il2_updater.exe && il2_updater.exe";

                            Helpers.DebugLog(METHOD, "Preparing cmd line [{0}] on assembly dir", cmd);
                            Helpers.DelayCmdLine(1, cmd, new DirectoryInfo(Helpers.AssemblyDirectory), false);
                        }

                        else
                        {
                            if (File.Exists(Helpers.AssemblyFileName + ".new"))
                                File.Delete(Helpers.AssemblyFileName + ".new");

                            Helpers.ConsoleWrite(compare ? Helpers.C_NEU : Helpers.C_NEG, 0, true, false, "Failure");
                            error = true;
                        }
                    }

                    else
                        Helpers.ConsoleWrite(compare ? Helpers.C_POS : Helpers.C_NEG, 0, true, false, compare ? "Success" : "Failure");

                    if (error)
                        il2_updater.FatalErrors++;

                    if (same || !compare)
                        Finish(DontWaitInDebug: true);
                }
            }

            Helpers.ConsoleWrite(Helpers.C_STD, 0, false, true, "Checking for updates ({0})", SelfOnly ? "Self" : "Client tools");
            Helpers.ConsoleWrite(error ? Helpers.C_NEG : Helpers.C_POS, 0, true, false, error ? "Failure" : "Success");
            Helpers.ConsoleWrite(Helpers.C_STD, 0, true, false, null);

            if (error)
                il2_updater.FatalErrors++;

            return !error;
        }

        public static bool UnpackFile(FileInfo packedFile, FileInfo unpackedFile)
        {
            const string METHOD = "UnpackFile";

            Helpers.DebugLog(METHOD, string.Empty, "Entering:", packedFile, unpackedFile);

            if (packedFile.Extension.ToUpper() != ".ZIP")
                return false;

            try
            {
                Helpers.DebugLog(METHOD, "Opening packed file");
                ZipStorer zip = ZipStorer.Open(packedFile.FullName, FileAccess.Read);

                // Read all directory contents
                var dir = zip.ReadCentralDir();
                Helpers.DebugLog(METHOD, "Read central directory of zip: {0}", dir);

                // Extract all files in target directory
                string path;
                bool result;
                var entry = dir[0];

                //path = Path.Combine(packedFile.Directory.FullName, Path.GetFileName(entry.FilenameInZip));
                path = Path.Combine(packedFile.Directory.FullName, unpackedFile.FullName);
                
                Helpers.DebugLog(METHOD, "Combining paths: [{0}] with [{1}] into [{2}]", packedFile.Directory.FullName, unpackedFile.FullName, path);
                
                result = zip.ExtractFile(entry, path);

                Helpers.DebugLog(METHOD, "Result: {0}", result);

                zip.Close();
            }
            catch
            {
                Helpers.DebugLog(METHOD, "Unknown unzip error occured");
                return false;
            }

            return true;
        }

        public static bool DownloadAndSetupHashList(IList<Model_FileItemLight> list, FileInfo fileXML)
        {
            const string METHOD = "DownloadAndSetupHashList";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", list, fileXML);

            using (var webClient = new WebClient())
            {
                Helpers.DebugLog(METHOD, "Prepare to download file: {0}", fileXML);

                if (!DownloadFile(fileXML))
                {
                    Helpers.DebugLog(METHOD, "Download of [{0}] failure", fileXML);
                    return false;
                }

                Helpers.DebugLog(METHOD, "Prepare to load list from xml: {0}", fileXML);
                LoadListFromXML(list, fileXML);
            }

            return true;
        }

        public static bool LoadListFromXML(IList<Model_FileItemLight> fileItems, FileInfo inXmlFile)
        {
            const string METHOD = "LoadListFromXML";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", fileItems, inXmlFile, "Attached debugger: " + Debugger.IsAttached);

            Helpers.DebugLog(METHOD, "Check if xml file exists: {0}", inXmlFile.Exists);
            if (!inXmlFile.Exists)
                return false;

            if (inXmlFile.Extension.ToLower() != ".xml")
                return false;

            try
            {
                string sPath;
                string sCheckSum;
                string sDir = Debugger.IsAttached ? il2_updater.BaseDir.FullName : Helpers.AssemblyDirectory;

                Helpers.DebugLog(METHOD, "Directory used: {0} or {1}? -> {2}", il2_updater.BaseDir.FullName, Helpers.AssemblyDirectory, sDir);
                
                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();

                //  XML Start
                Helpers.DebugLog(METHOD, "Creating xml reader on file: {0}", inXmlFile.FullName);
                XmlReader xmlReader = XmlReader.Create(inXmlFile.FullName, xmlReaderSettings);

                //  <Root> missing - exit
                Helpers.DebugLog(METHOD, "Reading to 'Root'");
                if (!xmlReader.ReadToFollowing("Root"))
                {
                    Helpers.DebugLog(METHOD, "Unable to read to 'Root' element - exit false");
                    return false;
                }

                //  <Root>
                else
                {
                    //  <FileItems>
                    Helpers.DebugLog(METHOD, "Reading to 'FileItems'");
                    xmlReader.ReadToFollowing("FileItems");

                    if (xmlReader.IsEmptyElement)
                    {
                        Helpers.DebugLog(METHOD, "Element is empty");
                        return false;
                    }

                    //  <FileItem>
                    Helpers.DebugLog(METHOD, "Preparing to loop to read to 'FileItem' elements");
                    while (xmlReader.ReadToFollowing("FileItem"))
                    {
                        //  <Path>
                        Helpers.DebugLog(METHOD, "Reading to 'Path'");
                        xmlReader.ReadToFollowing("Path");

                        sPath = xmlReader.ReadElementContentAsString();
                        Helpers.DebugLog(METHOD, "Read element content as string: {0}", sPath);

                        //  <Checksum>
                        Helpers.DebugLog(METHOD, "Reading to 'Checksum'");
                        xmlReader.ReadToFollowing("Checksum");
                        sCheckSum = xmlReader.ReadElementContentAsString();
                        Helpers.DebugLog(METHOD, "Read element content as string: {0}", sCheckSum);

                        sPath = sPath.Replace("{NWN_DIR}", sDir);
                        Helpers.DebugLog(METHOD, "Replaced {0} by {1} -> {2}", "{NWN_DIR}", sDir, sPath);

                        var item = new Model_FileItemLight()
                        {
                            FileInfo = new FileInfo(sPath),
                            Checksum = sCheckSum
                        };

                        fileItems.Add(item);
                        Helpers.DebugLog(METHOD, "Added new file item model: {0}", item);

                    }//FileItem

                }// </Root>

                //  XML End
                Helpers.DebugLog(METHOD, "Closed xml reader");
                xmlReader.Close();
            }

            catch
            {
                Helpers.DebugLog(METHOD, "An error occured");
                return false;
            }

            return true;
        }

        public static string ComputeCheksum(FileInfo fileInfo)
        {
            const string METHOD = "ComputeChecksum";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", fileInfo);

            if (!fileInfo.Exists)
            {
                Helpers.DebugLog(METHOD, "File doesnt exist - return empty string");
                return string.Empty;
            }

            byte[] hash;

            Helpers.DebugLog(METHOD, "Opening file for read access and read sharing");
            using (var stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var md5 = new MD5CryptoServiceProvider())
            {
                Helpers.DebugLog(METHOD, "Computing md5...");
                hash = md5.ComputeHash(stream);
            }

            var sb = new StringBuilder(15);

            hash.ToList().ForEach(b => sb.Append(b.ToString("X2")));
            Helpers.DebugLog(METHOD, "Got hash code: {0}", sb.ToString());

            return sb.ToString();
        }

        public static bool CompareLocalFileAgainstRemote(FileInfo localFile, IList<Model_FileItemLight> remoteList)
        {
            const string METHOD = "CompareLocalFileAgainstRemote";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", localFile, remoteList);

            var localHash = string.Empty;
            var remoteHash = string.Empty;

            if (localFile == null)
            {
                Helpers.DebugLog(METHOD, "Local file is null - return false");
                return false;
            }

            Helpers.DebugLog(METHOD, "Refreshing local file");
            localFile.Refresh();

            if (!localFile.Exists)
            {
                Helpers.DebugLog(METHOD, "Local file doesnt exist - return false");
                return false;
            }

            localHash = ComputeCheksum(localFile);
            Helpers.DebugLog(METHOD, "Received hash of file: {0} = {1}", localFile, localHash);

            if (localHash == string.Empty)
            {
                Helpers.DebugLog(METHOD, "Received empty string - return false");
                return false;
            }

            var item = Helpers.FindFileInList(localFile, remoteList);
            Helpers.DebugLog(METHOD, "Result of finding file in list: {0} = {1}", localHash, item == null);

            if (item == null)
                return false;

            Helpers.DebugLog(METHOD, "Found item with checksum: {0} with {1}", item, item.Checksum);
            remoteHash = item.Checksum;

            Helpers.DebugLog(METHOD, "Return local hash equals remote hash: {0} == {1} ? {2}", localHash, remoteHash, localHash == remoteHash);
            return localHash == remoteHash;
        }

        public static bool CheckGameVersion(DirectoryInfo baseDir)
        {
            const string METHOD = "CheckGameVersion";
            Helpers.DebugLog(METHOD, string.Empty, "Enter", baseDir.FullName);

            if (baseDir == null)
            {
                Helpers.DebugLog(METHOD, "Error - base dir is null");
                il2_updater.FatalErrors++;
                return false;
            }

            var check = new FileInfo(Path.Combine(baseDir.FullName, "xp3.key")).Exists;
            Helpers.DebugLog(METHOD, "Checing if file exists: {0} = {1}", Path.Combine(baseDir.FullName, "xp3.key"), check);

            if (!check)
                il2_updater.FatalErrors++;

            return check;
        }

        public static bool CheckGameExpansions(DirectoryInfo baseDir)
        {
            const string METHOD = "CheckGameExpansions";
            Helpers.DebugLog(METHOD, string.Empty, "Enter:", baseDir.FullName);

            if (baseDir == null)
            {
                Helpers.DebugLog(METHOD, "Error - base dir is null");
                il2_updater.FatalErrors++;
                return false;
            }

            var check =
                new FileInfo(Path.Combine(baseDir.FullName, "xp1.key")).Exists &&
                new FileInfo(Path.Combine(baseDir.FullName, "xp2.key")).Exists &&
                new FileInfo(Path.Combine(baseDir.FullName, "xp2patch.key")).Exists;

            Helpers.DebugLog(METHOD, "Check if files {0}, {1} and {2} exist in dir {3} = {4}", Path.Combine(baseDir.FullName, "xp1.key"), Path.Combine(baseDir.FullName, "xp2.key"), Path.Combine(baseDir.FullName, "xp2patch.key"), baseDir.FullName, check);

            if (!check)
                il2_updater.FatalErrors++;

            return check;
        }

        public static DirectoryInfo GetBaseDir()
        {
            const string METHOD = "GetBaseDir";
            Helpers.DebugLog(METHOD, string.Empty, "Enter");

            string sSubKey, sKey, sBasePath;

            if (Environment.Is64BitOperatingSystem)
                sSubKey = @"Software\Wow6432Node\BioWare\NWN\Neverwinter";
            else
                sSubKey = @"Software\BioWare\NWN\Neverwinter";

            sKey = "Location";

            Helpers.DebugLog(METHOD, "Using key {0} sub key {1}", sKey, sSubKey);

            sBasePath = Helpers.ReadRegistryKey(RegistryHive.LocalMachine, sSubKey, sKey);

            Helpers.DebugLog(METHOD, "Using base path {0}", sBasePath);

            if (string.IsNullOrWhiteSpace(sBasePath))
            {
                Helpers.DebugLog(METHOD, "Base path is blank");

                if (File.Exists(Path.Combine(Helpers.AssemblyDirectory, "nwmain.exe")))
                {
                    Helpers.DebugLog(METHOD, "Found nwmain.exe in assembly directory: {0}", Helpers.AssemblyDirectory);

                    sBasePath = Helpers.AssemblyDirectory;

                    Helpers.DebugLog(METHOD, "Using base path {0} and setting base dir...", sBasePath);
                    SetBaseDir(new DirectoryInfo(sBasePath));
                }

                else
                {
                    Helpers.DebugLog(METHOD, "Not found - return null");
                    return null;
                }
            }

            if (File.Exists(Path.Combine(sBasePath, "nwmain.exe")))
            {
                Helpers.DebugLog(METHOD, "Final result - found in {0}", sBasePath);
                return new DirectoryInfo(sBasePath);
            }

            Helpers.DebugLog(METHOD, "Final result - Not found - return null");
            return null;
        }

        public static bool CheckNetwork()
        {
            const string METHOD = "CheckNetwork";

            try
            {
                Helpers.DebugLog(METHOD, "Enter");
                Helpers.DebugLog(METHOD, "Pinging files.ilandria.com");
                using (var ping = new Ping())
                {
                    var result = ping.Send("files.ilandria.com", 1000, new byte[1024], new PingOptions());
                    Helpers.DebugLog(METHOD, "Ping results: {0} ({1}ms - {2})", result.Status, result.RoundtripTime, result.Address);
                }
            }
            catch (PingException)
            {
                Helpers.DebugLog(METHOD, "Ping error");
                il2_updater.FatalErrors++;
                return false;
            }

            return true;
        }

        public static bool CheckDotNetVersion()
        {
            const string METHOD = "CheckDotNetVersion";
            Helpers.DebugLog(METHOD, "Enter");

            Helpers.DebugLog(METHOD, "Opening base key: {0} and sub key: {1}", RegistryHive.LocalMachine, "SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\");
            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
            {
                int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                var check = CheckFor45DotVersion(releaseKey);
                Helpers.DebugLog(METHOD, "Got value: {0} - check if is valid: {1}", releaseKey, check);

                if (!check)
                    il2_updater.FatalErrors++;

                return check;
            }
        }

        public static bool CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 393295)
            {
                return true;
            }
            if ((releaseKey >= 379893))
            {
                return true;
            }
            if ((releaseKey >= 378675))
            {
                return true;
            }
            if ((releaseKey >= 378389))
            {
                return true;
            }
            // This line should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return false;
        }
    }
}
