﻿
using System.IO;

namespace il2_clienttools.Models
{
    public class Model_FileItemLight
    {
        public FileInfo FileInfo { get; set; }
        public string Checksum { get; set; }
        public override string ToString()
        {
            return FileInfo.Name;
        }
    }
}
