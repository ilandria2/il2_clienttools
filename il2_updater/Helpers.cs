﻿
namespace il2_clienttools
{
    #region Namespaces

    using il2_clienttools.Models;
    using Microsoft.Win32;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    #endregion

    public static class Helpers
    {
        #region Constants

        public const string URI_FTP = "http://files.ilandria.com/content";
        public const string URI_FILEITEMS = "il2_clienttools.xml";

        public const byte DOTS = 50;

        public static readonly ConsoleColor C_POS = ConsoleColor.Green;
        public static readonly ConsoleColor C_NEG = ConsoleColor.Red;
        public static readonly ConsoleColor C_STD = ConsoleColor.White;
        public static readonly ConsoleColor C_PAR = ConsoleColor.DarkGray;
        public static readonly ConsoleColor C_DEF = ConsoleColor.Gray;
        public static readonly ConsoleColor C_NEU = ConsoleColor.Yellow;

        #endregion

        #region Fields

        private static string _assemblyFileName;
        private static string _assemblyDisplayName;
        private static string _assemblyDirectory;
        private static string _assemblyVersion;
        private static string _assemblyTitle;

        #endregion

        #region Properties

        public static string AssemblyDisplayName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_assemblyDisplayName))
                    _assemblyDisplayName = Helpers.GetAssemblyDisplayName(Assembly.GetExecutingAssembly());

                return _assemblyDisplayName;
            }
        }

        public static string AssemblyFileName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_assemblyFileName))
                    _assemblyFileName = Helpers.GetAssemblyFileName(Assembly.GetExecutingAssembly());
                return _assemblyFileName;
            }
        }

        public static string AssemblyDirectory
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_assemblyDirectory))
                    _assemblyDirectory = Helpers.GetAssemblyDirectory(Assembly.GetExecutingAssembly());

                return _assemblyDirectory;
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_assemblyVersion))
                    _assemblyVersion = Helpers.GetAssemblyVersion(Assembly.GetExecutingAssembly());
                return _assemblyVersion;
            }
        }

        public static string AssemblyTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_assemblyTitle))
                    _assemblyTitle = Helpers.GetAssemblyTitle(Assembly.GetExecutingAssembly());

                return _assemblyTitle;
            }
        }

        #endregion

        #region Helper methods

        public static string GetAssemblyTitle(Assembly asm)
        {
            const string METHOD = "GetAssemblyTitle";
            DebugLog(METHOD, string.Empty, "Enter:", asm);

            var name = GetAssemblyDisplayName(asm);
            var version = GetAssemblyVersion(asm);

#if DEBUG
            var title = string.Format("{0} (v{1}) *** Developer", name, version);
#else
            var title = string.Format("{0} (v{1})", name, version);
#endif
            DebugLog(METHOD, "Returning title: {0}", title);

            return title;
        }

        public static string GetAssemblyVersion(Assembly asm)
        {
            const string METHOD = "GetAssemblyVersion";
            DebugLog(METHOD, string.Empty, "Enter:", asm);
            
            var fvi = FileVersionInfo.GetVersionInfo(asm.Location);

            DebugLog(METHOD, "Returning version: {0}", fvi.FileVersion);
            return fvi.FileVersion;
        }

        public static string GetAssemblyFileName(Assembly asm)
        {
            var uri = new UriBuilder(asm.CodeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var file = new FileInfo(path);

            return file.Name.Replace(file.Extension, file.Extension.ToLower());
        }

        public static string GetAssemblyDisplayName(Assembly asm)
        {
            const string METHOD = "GetAssemblyDisplayName";
            DebugLog(METHOD, string.Empty, "Enter:", asm);
            
            var asmTitle = asm.GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0] as AssemblyTitleAttribute;

            DebugLog(METHOD, "Returning: {0}", asmTitle.Title);
            return asmTitle.Title;
        }

        public static string GetAssemblyDirectory(Assembly asm)
        {
            var uri = new UriBuilder(asm.CodeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var dir = Path.GetDirectoryName(path);

            return dir;
        }

        public static string GetWorkingDirectory()
        {
            const string METHOD = "GetWorkingDirectory";
            DebugLog(METHOD, string.Empty, "Enter");

            var dir = GetAssemblyDirectory(Assembly.GetExecutingAssembly());
            DebugLog(METHOD, "Returning: {0}", dir);

            return dir;
        }

        public static string Pad(string value)
        {
            return value.PadRight(DOTS, '.');
        }

        public static bool CompareDirectoryName(string dir1, string dir2)
        {
            const string METHOD = "CompareDirectoryName";
            DebugLog(METHOD, string.Empty, "Enter:", dir1, dir2);

            var compare = dir1.ToUpper().TrimEnd('/', '\\') == dir2.ToUpper().TrimEnd('/', '\\');
            DebugLog(METHOD, "Returning: {0}", compare);

            return compare;
        }

        public static bool CompareFileName(string file1, string file2)
        {
            const string METHOD = "CompareFileName";
            DebugLog(METHOD, string.Empty, "Enter:", file1, file2);

            var compare = file1.ToUpper() == file2.ToUpper();
            DebugLog(METHOD, "Returning: ", compare);

            return compare;
        }

        public static void OpenUrl(string url)
        {
            const string METHOD = "OpenUrl";
            DebugLog(METHOD, string.Empty, "Enter:", url);

            string browserPath = GetBrowserPath();
            DebugLog(METHOD, "Got browser path: {0}", browserPath);

            if (browserPath == string.Empty)
            {
                DebugLog(METHOD, "Using default browser path: iexplore");
                browserPath = "iexplore";
            }

            Process process = new Process();
            process.StartInfo = new ProcessStartInfo(browserPath);
            process.StartInfo.Arguments = "\"" + url + "\"";

            DebugLog(METHOD, "Starting process: {0} {1}", browserPath, process.StartInfo.Arguments);
            process.Start();
        }

        public static string GetBrowserPath()
        {
            const string METHOD = "GetBrowserPath";
            DebugLog(METHOD, string.Empty, "Enter");

            string browser = string.Empty;
            RegistryKey key = null;

            try
            {
                DebugLog(METHOD, "Opening registry subkey in XP: {0}\\{1}", RegistryHive.ClassesRoot, @"HTTP\shell\open\command");

                // try location of default browser path in XP
                key = Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                // try location of default browser path in Vista
                if (key == null)
                {
                    DebugLog(METHOD, "Key is null - using default papth in Vista", RegistryHive.CurrentUser, @"Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http");
                    key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http", false); ;
                }

                if (key != null)
                {
                    DebugLog(METHOD, "Got key - getting value...");

                    //trim off quotes
                    browser = key.GetValue(null).ToString().ToLower().Replace("\"", "");

                    DebugLog(METHOD, "Got path: {0}", browser);
                    
                    if (!browser.EndsWith("exe"))
                    {
                        //get rid of everything after the ".exe"
                        browser = browser.Substring(0, browser.LastIndexOf(".exe") + 4);
                        DebugLog(METHOD, "Cleaned path: {0}", browser);
                    }

                    DebugLog(METHOD, "Closing key...");
                    key.Close();
                }
            }
            catch
            {
                DebugLog(METHOD, "An error occured");
                return string.Empty;
            }

            DebugLog(METHOD, "Returning path: {0}", browser);
            return browser;
        }

        public static string GetShortPathString(string path, int maxLength)
        {
            const string METHOD = "GetShortPathString";
            DebugLog(METHOD, string.Empty, "Enter:", path, maxLength);

            if (path.Length <= maxLength || path.Length < 15)
            {
                DebugLog(METHOD, "Returning {0} (being smaller than {1} or 15", path, maxLength);
                return path;
            }

            path = path.Substring(0, 10) + "..~" + path.Substring(path.Length - maxLength + 13 - 1, maxLength - 13 + 1).TrimEnd(new[] { '\\' }) + @"\";
            DebugLog(METHOD, "Returning cleaned path: {0}", path);

            return path;
        }

        public static Model_FileItemLight FindFileInList(FileInfo file, IList<Model_FileItemLight> list)
        {
            const string METHOD = "FindFileInList";
            DebugLog(METHOD, string.Empty, "Enter:", file, list);

            var fileName = file.Name.ToUpper();

            if (fileName.EndsWith(".NEW"))
                fileName = fileName.Remove(fileName.LastIndexOf(".NEW"));

            DebugLog(METHOD, "Using filename: {0}", fileName);

            var item = list.SingleOrDefault(f => f.FileInfo.Name.ToUpper() == fileName);

            DebugLog(METHOD, "Returning item: {0}", item);
            return item;
        }

        public static void DelayCmdLine(int seconds, string cmdline, DirectoryInfo workDir, bool ElevateAccess)
        {
            const string METHOD = "DelayCmdLine";
            DebugLog(METHOD, string.Empty, "Enter:", seconds, cmdline, "WorkDir: " + workDir, "Elevate: " + ElevateAccess);

            if (File.Exists(workDir.FullName))
            {
                workDir = new DirectoryInfo(Path.GetDirectoryName(workDir.FullName));
                DebugLog(METHOD, "Using work dir: {0}", workDir);
            }

            string workDirPath = workDir.FullName;

            if (workDirPath.Substring(1, 1) == ":")
                workDirPath = workDirPath.Substring(2, workDirPath.Length - 2);

            DebugLog(METHOD, "Using work dir path: {0}", workDirPath);

            cmdline = "/K \"echo " + workDirPath + " & cd \"" + workDirPath + "\"\" & ping 127.0.0.1 -n " + (seconds + 1).ToString() + " > nul & " + cmdline + " &exit";

            DebugLog(METHOD, "Using cmdline: {0}", cmdline);

            using (var process = new Process())
            {
                var startInfo = new ProcessStartInfo();

                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = cmdline;

                if (ElevateAccess)
                    startInfo.Verb = "runas";

                process.StartInfo = startInfo;
                process.Start();
            }
        }

        public static void SetRegistryKey(RegistryHive regHive, string sSubKey, string sKey, string sValue)
        {
            const string METHOD = "SetRegistryKey";
            DebugLog(METHOD, string.Empty, "Enter:", regHive, sSubKey, sKey, sValue);
            
            DebugLog(METHOD, "Opening base key: {0}...", regHive);

            RegistryKey rkBaseKey = RegistryKey.OpenBaseKey(regHive, RegistryView.Default);
            RegistryKey rkTemp = null;

            sSubKey = sSubKey.Replace('/', '\\');

            var subkeys = sSubKey.Split(new char[] { '\\' });
            DebugLog(METHOD, string.Empty, "Split sub keys:", subkeys);

            rkTemp = rkBaseKey;

            foreach (var item in subkeys)
            {
                DebugLog(METHOD, "Opening / creating sub key: {0}", item);

                rkTemp =
                    rkTemp.OpenSubKey(item, true) ??
                    rkTemp.CreateSubKey(item, RegistryKeyPermissionCheck.ReadWriteSubTree);
            }

            DebugLog(METHOD, "Setting subkey value: {0}, {1} = {2}", sKey, sSubKey, sValue);
            rkTemp.SetValue(sKey, sValue, RegistryValueKind.String);
        }

        public static string ReadRegistryKey(RegistryHive regHive, string sSubKey, string sKey)
        {
            const string METHOD = "ReadRegistryKey";
            DebugLog(METHOD, string.Empty, "Enter:", regHive, sSubKey, sKey);

            sSubKey = sSubKey.Replace('/', '\\');

            DebugLog(METHOD, "Us9ng subkey: {0}", sSubKey);
            DebugLog(METHOD, "Opening base key: {0}", regHive);
            RegistryKey rkBaseKey = RegistryKey.OpenBaseKey(regHive, RegistryView.Default);
            
            DebugLog(METHOD, "Opening sub key: {0}", sSubKey);
            RegistryKey rkSubKey = rkBaseKey.OpenSubKey(sSubKey);

            if (rkSubKey == null)
            {
                DebugLog(METHOD, "Sub key is null - returning null");
                return null;
            }

            try
            { 
                var value = (string)rkSubKey.GetValue(sKey);

                DebugLog(METHOD, "Returning value: {0}", value);
                return value;
            }
            catch
            { }

            DebugLog(METHOD, "Unable to retrieve value - returning null");
            return null;
        }

        /// <summary>
        /// Writes colored and formatted text to the console output
        /// </summary>
        public static void ConsoleWrite(ConsoleColor color, int skipLines, bool WriteLine, bool AlsoPadValue, string format, params object[] args)
        {
            const string METHOD = "ConsoleWrite";
            Helpers.DebugLog(METHOD, new string(' ', 20) + "Message: \"" + format + "\"", args);
            Console.ForegroundColor = color;

            for (int i = 0; i < skipLines; i++)
                Console.WriteLine();

            format = format ?? string.Empty;
            var text = string.Format(format, args);

            Console.Write(text);

            if (AlsoPadValue)
            {
                if (text.Length < DOTS)
                {
                    Console.ForegroundColor = C_PAR;
                    Console.Write("".PadRight(DOTS - text.Length, '.'));
                    Console.ForegroundColor = color;
                }
            }

            if (WriteLine)
                Console.WriteLine();
        }

        public static string ObjectsToString(object objList)
        {
            if (objList == null)
                return "<null>";

            if (objList is string)
                return objList.ToString();

            else if (objList is IEnumerable)
            {
                var sb = new StringBuilder();

                foreach (var sub in objList as IEnumerable)
                    sb.AppendFormat("{0};", sub.ToString());

                return sb.ToString().TrimEnd(new[] { ';' });
            }

            else 
                return objList.ToString();
        }

        public static void DebugLog(string method, string format, params object[] args)
        {
#if !DEBUG
            return;
#endif
            string file = string.Format(@"{0}\{1}.log", Helpers.AssemblyDirectory, Helpers.AssemblyFileName);

            string[] strings = new string[args.Length];
            for (int i = 0; i < args.Length; i++)
                strings[i] = ObjectsToString(args[i]);

            if (string.IsNullOrWhiteSpace(format) && strings.Length > 0)
            {
                var sb = new StringBuilder();
                
                foreach (var item in strings)
                    sb.AppendFormat("{0};", item);

                format = sb.ToString();
            }

            string message = string.Format(format, args);
            message = string.Format("{0:yyyy-MM-dd HH:mm:ss} - {1} - {2}", DateTime.Now, method, message);
            message = message.Replace("\r\n", " ");

            using (var fs = new FileStream(file, FileMode.Append))
            using (var sw = new StreamWriter(fs))
                sw.WriteLine(message);
        }

        #endregion
    }
}
