﻿
namespace il2_clienttools
{
    #region Namespaces

    using il2_clienttools.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;

    #endregion

    public class il2_updater
    {
        #region Fields

        private static Collection<Model_FileItemLight> _remoteFileItems;
        private static DirectoryInfo _baseDir;
        public static bool PreviousChecksErrored;
        public static int FatalErrors;

        #endregion

        #region Properties

        public static IList<Model_FileItemLight> RemoteFileItems
        {
            get
            {
                if (_remoteFileItems == null)
                {
                    var dir = BaseDir ?? new DirectoryInfo(".");

                    _remoteFileItems = new Collection<Model_FileItemLight>();
                    var download = Procedures.DownloadAndSetupHashList(_remoteFileItems, new FileInfo(Path.Combine(dir.FullName, Helpers.URI_FILEITEMS)));

                    if (!download)
                        throw new ApplicationException("Error downloading hash list.");
                }

                return _remoteFileItems;
            }
        }

        public static bool IsBaseDirSet { get; set; }

        public static DirectoryInfo BaseDir
        {
            get 
            {
                if (!IsBaseDirSet)
                {
                    _baseDir = Procedures.GetBaseDir();
                    IsBaseDirSet = true;
                }

                return _baseDir; 
            }
        }

        public static string[] CommandLineArguments { get; set; }

        #endregion

        #region Dev-purpose

        static void Dev()
        {
            if (Debugger.IsAttached)
                Debugger.Break();

            #region test code here

            var b = Procedures.CheckForUpdates(true, RemoteFileItems);

            #endregion//test code here

            Procedures.Finish();
        }

        #endregion

        #region Entry point

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            CommandLineArguments = args;
            
            const string METHOD = "Main";
            Helpers.DebugLog(METHOD, "Entering: Parameters[{0}]", string.Join(",", args));

            Procedures.Start();

            //Dev();
            Procedures.PerformCheck(Procedures.CheckNetwork, "Checking Network");

            Procedures.PerformCheck(Procedures.CheckForUpdates, true, RemoteFileItems, null);

            Procedures.PerformCheck(Procedures.CheckOperatingSystem, "Checking Operating system");
            Procedures.PerformCheck(Procedures.CheckDotNetVersion, "Checking Assembly framework");
            Procedures.PerformCheck(Procedures.CheckGameBase, BaseDir, "Checking Game base");
            Procedures.PerformCheck(Procedures.CheckWorkingDirectory, BaseDir, Helpers.AssemblyDirectory, null);
            Procedures.PerformCheck(Procedures.CheckGameExpansions, BaseDir, "Checking Game expansions");
            Procedures.PerformCheck(Procedures.CheckGameVersion, BaseDir, "Checking Game version");
            Procedures.PerformCheck(Procedures.CheckAndRepairWebLaunch, BaseDir, "Checking Game URL-launch");

            Procedures.PerformCheck(Procedures.CheckForUpdates, false, RemoteFileItems, null);

            Procedures.StartLauncher();
            Procedures.Finish(DontWaitInDebug: true);
        }

        #endregion

        #region Event handlers
        
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            FatalErrors++;
            Procedures.HandleException(ex);
            Procedures.Finish();
        }

        #endregion
    }
}
