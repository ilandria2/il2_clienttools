using System.Reflection;
using System.Runtime.InteropServices;



/************ Local assembly info *************/

[assembly: AssemblyTitle("Updater")]
[assembly: AssemblyDescription("Self updater application for Ilandria 2 client tools.")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("B56F0A64-7ED3-4863-8AF0-F4C0F6B9FDAE")]

//VERSION=0.6.18
[assembly: AssemblyInformationalVersion("0.6.18")]
[assembly: AssemblyFileVersion("0.6.18")]
[assembly: AssemblyVersion("0.6.18")]
