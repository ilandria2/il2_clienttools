using System.Reflection;
using System.Runtime.InteropServices;

/************ Local assembly info *************/

[assembly: AssemblyTitle("Resources")]
[assembly: AssemblyDescription("Client tools for Ilandria 2 players.")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("5bf7f9a6-c171-4364-a4e8-3b3e5d013a6a")]

//VERSION=0.6.16
[assembly: AssemblyInformationalVersion("0.6.16")]
[assembly: AssemblyFileVersion("0.6.16")]
[assembly: AssemblyVersion("0.6.16")]
