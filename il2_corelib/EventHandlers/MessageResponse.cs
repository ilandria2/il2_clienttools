﻿
namespace il2_clienttools.EventHandlers
{
    #region Namespaces

    using il2_clienttools.Data;
    using System;

    #endregion

    public sealed class MessageResponseEventArgs : EventArgs
    {
        #region Fields

        private Data_MessageResponse? _response;
        private object _userState;

        #endregion

        #region Construction

        /// <summary>
        /// Constructs a new instance of the <see cref="MessageResponseEventArgs"/> class using individual parameters of the
        /// <see cref="Data_MessageResponse"/> structure.
        /// </summary>
        /// <param name="requestor">Username</param>
        /// <param name="requestorId">User id</param>
        /// <param name="clientType">SMF | GAME</param>
        /// <param name="isTest">bool</param>
        /// <param name="type">Message Type</param>
        /// <param name="subType">Message SubType</param>
        /// <param name="rawDataId">RawData id</param>
        /// <param name="param">Message Parameter</param>
        /// <param name="data">Response data</param>
        /// <param name="exception">Error object</param>
        public MessageResponseEventArgs(string requestor, int requestorId, string clientType, bool isTest, int type, int subType, int rawDataId, string param, string data, Exception exception, object userState = null)
            : this(new Data_MessageResponse(requestor, requestorId, clientType, isTest, type, subType, rawDataId, param, data, exception), userState)
        {
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MessageResponseEventArgs"/> class using only exception object of the
        /// <see cref="Data_MessageResponse"/> structure.
        /// </summary>
        /// <param name="exception"></param>
        public MessageResponseEventArgs(Exception exception, object userState = null)
            : this(new Data_MessageResponse(exception), userState)
        {
        }

        /// <summary>
        /// Constructs a new instance of the <see cref="MessageResponseEventArgs"/> using an existing instance of the
        /// <see cref="Data_MessageResponse"/> structure.
        /// </summary>
        /// <param name="response"></param>
        public MessageResponseEventArgs(Data_MessageResponse? response, object userState = null)
        {
            Response = response;
            UserState = userState;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Response data
        /// </summary>
        public Data_MessageResponse? Response
        {
            get { return _response; }
            private set { _response = value; }
        }

        /// <summary>
        /// User parameter
        /// </summary>
        public object UserState
        {
            get { return _userState; }
            private set { _userState = value; }
        }

        #endregion
    }

    /// <summary>
    /// A method delegate for handling the MessageResponse event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="eventArgs"></param>
    public delegate void MessageResponseEventHandler(object sender, MessageResponseEventArgs eventArgs);
}
