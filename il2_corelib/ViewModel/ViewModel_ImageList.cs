﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Model;
    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// Interaction logic for the image list
    /// </summary>
    public class ViewModel_ImageList : ViewModel_Base
    {
        #region Fields

        private Model_ImageItem _selectedItem;
        private int _selectedIndex;
        private IList<Model_ImageItem> _itemsSource;

        #endregion

        #region Properties

        /// <summary>
        /// Selected item from the list
        /// </summary>
        public Model_ImageItem SelectedItem
        {
            get { return _selectedItem; }
            set 
            {
                if (_selectedItem == value) return;

                _selectedItem = value;

                base.OnPropertyChanged("SelectedItem");
            }
        }

        /// <summary>
        /// Selected item index from the list
        /// </summary>
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set 
            {
                if (_selectedIndex == value) return;

                _selectedIndex = value;

                base.OnPropertyChanged("SelectedIndex");
            }
        }

        /// <summary>
        /// Source of items for the horizontal list
        /// </summary>
        public IList<Model_ImageItem> ItemsSource
        {
            get 
            {
                if (_itemsSource == null)
                    _itemsSource = new List<Model_ImageItem>();

                return _itemsSource; 
            }
            set 
            {
                //_itemsSource.Clear();
                _itemsSource = value;

                base.OnPropertyChanged("ItemsSource");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the header (DisplayName) of the view model
        /// </summary>
        /// <param name="header"></param>
        public void SetHeader(string header)
        {
            base.DisplayName = header;
        }

        #endregion
    }
}
