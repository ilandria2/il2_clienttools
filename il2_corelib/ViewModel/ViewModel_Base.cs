﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.ComponentModel;

    #endregion

    /// <summary>
    /// A base/common ViewModel (interaction logic)
    /// </summary>
    public abstract class ViewModel_Base : NotifyBase, IDisposable
    {
        #region Properties
        
        /// <summary>
        /// Returns the user-friendly name of this object
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public virtual string DisplayName { get; protected set; }

        #endregion  //  Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of this object
        /// </summary>
        protected ViewModel_Base()
        {
        }

        #endregion

        #region Members of IDisposable

        /// <summary>
        /// Invoked when this object is being removed from the
        /// application and will be subject to garbage collection
        /// </summary>
        void IDisposable.Dispose()
        {
            this.OnDispose();
        }

        /// <summary>
        /// Child classes can override this method to perform a clean-up logic,
        /// such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose()
        {
        }

#if DEBUG
        /// <summary>
        /// Useful for ensuring that ViewModel objects are properly garbage collected
        /// </summary>
        ~ViewModel_Base()
        {
            string msg = string.Format("{0} ({1}) ({2}) finalized", this.GetType().Name, this.DisplayName, this.GetHashCode());

            System.Diagnostics.Debug.WriteLine(msg);
        }
#endif

        #endregion  //  Members of IDisposable
    }
}
