﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using System;
    using System.Windows.Input;

    #endregion



    /// <summary>
    /// Represents an actionable item displayed by a view
    /// </summary>
    public class ViewModel_Command : ViewModel_Base
    {
        #region Properties

        /// <summary>
        /// The ICommand instance of this view model
        /// </summary>
        public ICommand Command { get; private set; }
        
        /// <summary>
        /// Indicates whether an instance is just a placeholder
        /// </summary>
        public bool IsEnabled { get; private set; }

        /// <summary>
        /// Returns the KeyBinding associated to this command view model
        /// </summary>
        public KeyBinding KeyBinding { get; private set; }

        /// <summary>
        /// A dynamic property returning the modifiers of KeyBinding
        /// </summary>
        public ModifierKeys Modifiers { get { return this.KeyBinding.Modifiers; } }
        
        /// <summary>
        /// A dynamic property returning the Key of KeyBinding
        /// </summary>
        public Key Key { get { return this.KeyBinding.Key; } }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of a Command view model
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="command"></param>
        public ViewModel_Command(string displayName, ICommand command)
            : this(displayName, command, null, true)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of a Command view model
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="command"></param>
        /// <param name="keyBinding"></param>
        public ViewModel_Command(string displayName, ICommand command, KeyBinding keyBinding)
            : this(displayName, command, keyBinding, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of a Command view model
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="command"></param>
        /// <param name="IsEnabled"></param>
        public ViewModel_Command(string displayName, ICommand command, bool IsEnabled)
            : this(displayName, command, null, IsEnabled)
        {
        }

        /// <summary>
        /// Initializes a new instance of a Command view model
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="command"></param>
        /// <param name="keyBinding"></param>
        /// <param name="IsEnabled"></param>
        public ViewModel_Command(string displayName, ICommand command, KeyBinding keyBinding, bool IsEnabled)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            base.DisplayName = displayName;
            this.Command = command;
            this.IsEnabled = IsEnabled;
            this.KeyBinding = keyBinding;
        }

        #endregion
    }
}
