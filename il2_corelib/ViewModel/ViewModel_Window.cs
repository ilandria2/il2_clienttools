﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_corelib.Controls;
    using il2_corelib.Interfaces;
    using System;
    using System.Windows.Input;

    #endregion

    /// <summary>
    /// This is an abstract sub-class of the ViewModel_Workspace
    /// It requests to be hidden from UI (Minimized) when tis Command_Minimize executes.
    /// </summary>
    public abstract class ViewModel_Window : ViewModel_Workspace
    {
        #region Fields

        private ICommand _command_Minimize;
        private bool _canMinimize;
        private float _busyProgress;
        private bool _isBusy;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the command that, when invoked, attempts to hide (minimize) this workspace
        /// from the user interface
        /// </summary>
        public ICommand Command_Minimize
        {
            get
            {
                if (this._command_Minimize == null)
                    this._command_Minimize = new RelayCommand(param => this.OnRequestMinimize(), param => this.CanMinimize);

                return this._command_Minimize;
            }
        }

        /// <summary>
        /// Indicates whether the view model has the power to minimize the view
        /// </summary>
        public bool CanMinimize
        {
            get { return this._canMinimize; }
        }

        /// <summary>
        /// Indicates whether the launcher is busy doing something time consuming
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy == value)
                    return;

                string M = string.Format("{0}.{1}", GetType().Name, "IsBusy");
                Common.LogEntry(M, "Setting IsBusy={0}", value);

                _isBusy = value;
                BusyProgress = _isBusy ? 0 : 100;

                base.OnPropertyChanged("IsBusy");
            }
        }

        /// <summary>
        /// Gets or sets the progress stage of being busy
        /// </summary>
        public float BusyProgress
        {
            get { return _busyProgress; }
            set
            {
                if (_busyProgress == value)
                    return;

                string M = string.Format("{0}.{1}", GetType().Name, "BusyProgress");
                Common.LogEntry(M, "Setting BusyProgress={0}", value);

                _busyProgress = value;

                if (_busyProgress > 100)
                    _busyProgress = 100;
                else if (_busyProgress < 0)
                    _busyProgress = 0;

                base.OnPropertyChanged("BusyProgress");
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new Workspace for window (closeable ViewModel) object
        /// </summary>
        public ViewModel_Window(ICloseable close)
            : base(close.Close)
        { }

        /// <summary>
        /// Creates a new Workspace for window (minimalizable and closeable ViewModel) object
        /// </summary>
        public ViewModel_Window(IMinimizeable minimizeClose)
            : base(minimizeClose.Close)
        {
            this.RequestMinimize += (s, a) => minimizeClose.Minimize();
            this._canMinimize = true;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Fires the RequestMinimize event
        /// </summary>
        private void OnRequestMinimize()
        {
            EventHandler handler = this.RequestMinimize;

            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion

        #region Events

        /// <summary>
        /// Raised when this view model should be hidden (Minimized) from the user interface
        /// </summary>
        public event EventHandler RequestMinimize;

        #endregion

    }
}
