﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.Windows.Input;

    #endregion

    /// <summary>
    /// This is an abstract sub-class of the ViewModel_Base.
    /// It requests to be removed from UI when tis Command_Close executes.
    /// </summary>
    public abstract class ViewModel_Workspace : ViewModel_Base
    {
        #region Fields
        
        private ICommand _closeCommand;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the command that, when invoked, attempts to remove this workspace
        /// from the user interface
        /// </summary>
        public ICommand Command_Close
        {
            get 
            {
                if (this._closeCommand == null)
                    this._closeCommand = new RelayCommand(param => this.OnRequestClose());

                return _closeCommand; 
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new Workspace (closeable ViewModel) object
        /// </summary>
        public ViewModel_Workspace()
        {

        }

        /// <summary>
        /// Creates a new Workspace (closeable ViewModel) object with a registered close handler
        /// </summary>
        public ViewModel_Workspace(Action viewCloseHandler)
        {
            EventHandler closeHandler = null;

            closeHandler = delegate
            {
                this.RequestClose -= closeHandler;
                viewCloseHandler();
            };

            this.RequestClose += closeHandler;
        }

        #endregion
        
        #region Methods

        /// <summary>
        /// Fires the RequestClose event
        /// </summary>
        private void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;

            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion

        #region Events

        /// <summary>
        /// Raised when this ViewModel should be removed from user interface
        /// </summary>
        public event EventHandler RequestClose;

        #endregion
    }
}
