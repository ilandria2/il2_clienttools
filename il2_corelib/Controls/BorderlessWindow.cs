﻿namespace il2_corelib.Controls
{
    #region Namespaces

    using il2_corelib.Interfaces;
    using System;
    using System.Windows;
    using System.Windows.Input;

    #endregion

    public class BorderlessWindow : Window, ICloseable, IMinimizeable
    {
        public BorderlessWindow() : base()
        {
            this.MouseDown += BorderlessWindow_MouseDown;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //this.Style = (Style)Application.Current.Resources["styleLauncherWindow"];
        }

        void BorderlessWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Minimizes the window
        /// </summary>
        public void Minimize()
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
    }
}
