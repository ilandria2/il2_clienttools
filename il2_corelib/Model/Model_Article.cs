﻿
namespace il2_clienttools.Model
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    #endregion

    [DataContract(Name = "Model_Article")]
    public class Model_Article
    {
        #region Properties

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "header")]
        public string Header { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "avatar")]
        public string Avatar { get; set; }

        [DataMember(Name = "deleted_at")]
        public string DateDeletedString { get; set; }
        public DateTime? DateDeleted 
        {
            get
            {
                DateTime result;
                if (!DateTime.TryParse(DateDeletedString, out result))
                    return null;

                return result;
            }
        }

        [DataMember(Name = "created_at")]
        public string DateCreatedString { get; set; }
        public DateTime? DateCreated
        {
            get
            {
                DateTime result;
                if (!DateTime.TryParse(DateCreatedString, out result))
                    return null;

                return result;
            }
        }

        [DataMember(Name = "updated_at")]
        public string DateUpdatedString { get; set; }
        public DateTime? DateUpdated
        {
            get
            {
                DateTime result;
                if (!DateTime.TryParse(DateUpdatedString, out result))
                    return null;

                return result;
            }
        }

        [DataMember(Name = "published_at")]
        public string DatePublishedString { get; set; }
        public DateTime? DatePublished
        {
            get
            {
                DateTime result;
                if (!DateTime.TryParse(DatePublishedString, out result))
                    return null;

                return result;
            }
        }

        [DataMember(Name = "published_by")]
        public string Publisher { get; set; }

        [DataMember(Name = "slug")]
        public string Slug { get; set; }

        #endregion

        #region Methods.Overrides

        public override string ToString()
        {
            return string.Format("Author = '{0}', Title = '{1}', Header = '{2}'", Publisher, Title, Header);
        }

        #endregion
    }

    [CollectionDataContract(Name = "Model_ArticleList")]
    public class Model_ArticleList : List<Model_Article> { }

}
