﻿
namespace il2_clienttools.Model
{
    #region Namespaces

    using Data;
    using Helper;
    using System;
    using System.IO;

    #endregion

    public class Model_FileItem : NotifyBase
    {
        #region Fields

        private decimal _status;
        private long _statusSizeVerified;
        private long _statusSizeVerifiedIncrease;
        private Data_Checksum _checksum;
        private string _size;
        private long _fileSize;
        private bool _isSensitive;
        private string _definitionFileName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the file size in bytes
        /// </summary>
        public long FileSize
        {
            get
            {
                if (this._fileSize == 0)
                {
                    if (!FileInfo.Exists)
                        return 0;

                    this._fileSize = FileInfo.Length;
                }

                return this._fileSize;
            }

            set 
            {
                if (_fileSize == value) return;

                _fileSize = value;

                base.OnPropertyChanged("FileSize");
            }
        }

        /// <summary>
        /// Gets or sets the status verified increased amount from previous value
        /// </summary>
        public long StatusSizeVerifiedIncrease
        {
            get 
            { 
                var result = _statusSizeVerifiedIncrease;
                _statusSizeVerifiedIncrease = 0;

                return result; 
            }
            private set 
            {
                if (_statusSizeVerifiedIncrease == value) return;

                _statusSizeVerifiedIncrease = value;
            }
        }

        /// <summary>
        /// Gets or sets the status verified so far
        /// </summary>
        public long StatusSizeVerified
        {
            get { return _statusSizeVerified; }
            set 
            {
                if (_statusSizeVerified == value) return;

                _statusSizeVerifiedIncrease += value - _statusSizeVerified;
                _statusSizeVerified = value;
            }
        }

        /// <summary>
        /// Gets or sets the FileInfo (source path) of the model
        /// </summary>
        public FileInfo FileInfo { get; protected set; }

        /// <summary>
        /// Gets or sets the checksum value of this model
        /// </summary>
        public Data_Checksum Checksum
        {
            get { return this._checksum; }
            set
            {
                if (this._checksum.Equals(value)) return;

                this._checksum = value;

                base.OnPropertyChanged("Checksum");
            }
        }
        
        /// <summary>
        /// Gets or sets the checksum-computing progress
        /// </summary>
        public decimal Status 
        { 
            get { return this._status; }
            set
            {
                var statusprev = this._status;

                if (value + statusprev < 100M && Math.Abs(value - statusprev) < 1M) return;

                this._status = value;

                //base.OnPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets the formatted file size
        /// </summary>
        public string Size
        {
            get 
            {
                if (string.IsNullOrEmpty(this._size))
                    this._size = Common.GetFileSizeFormat(this.FileInfo.Length);

                return this._size; 
            }
        }

        /// <summary>
        /// Indicates whether the file item is sensitive or not
        /// * Sensitive file items are verified by comparing MD5 checksum values
        /// * Non-Sensitive file items are verified by comparing file size only
        /// </summary>
        public bool IsSensitive 
        {
            get { return _isSensitive; }
            set
            {
                if (_isSensitive == value) return;

                _isSensitive = value;

                base.OnPropertyChanged("IsSensitive");
            }
        }
        
        /// <summary>
        /// Gets the original base dir for this file item instance
        /// </summary>
        public string DefinitionFileName 
        { 
            get
            {
                return this._definitionFileName;
            }
            private set
            {
                if (value == this._definitionFileName)
                    return;

                this._definitionFileName = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of the FileItem object using FileInfo object instance as a parameter
        /// </summary>
        /// <param name="fileInfo"></param>
        public Model_FileItem(FileInfo fileInfo)
            : this(fileInfo, null)
        {
        }

        /// <summary>
        /// Creates a new instance of the FileItem object using another FileItem object instance as a parameter
        /// </summary>
        /// <param name="fileItem"></param>
        public Model_FileItem(Model_FileItem fileItem)
            : this(fileItem.FileInfo, null)
        {
        }

        /// <summary>
        /// Creates a new instance of the FileItem object using FileInfo object instance as a parameter
        /// Additionally stores the definition file's path as an indicator of file info's origin
        /// </summary>
        /// <param name="fileInfo"/>
        /// <param name="defFile"/>
        public Model_FileItem(FileInfo fileInfo, string defFile)
        {
            this.FileInfo = fileInfo;
            this.DefinitionFileName = defFile;
        }

        /// <summary>
        /// Creates a new instance of the FileItem object using another FileItem object instance as a parameter
        /// Additionally stores the definition file's path as an indicator of file item's origin
        /// </summary>
        /// <param name="fileItem"/>
        /// <param name="defFile"/>
        public Model_FileItem(Model_FileItem fileItem, string defFile)
            : this(fileItem.FileInfo, defFile)
        {
            this.FileSize = fileItem.FileSize;
        }

        #endregion

        #region Methods

        #region Methods.Overrides

        /// <summary>
        /// Retruns the string-based representation of the object instance
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            FileInfo fileInfo = FileInfo;

            if (DefinitionFileName != null)
                return Common.GetRelativePath(FileInfo, Path.GetDirectoryName(DefinitionFileName));
            else
                return Common.GetRelativePath(FileInfo, Common.RegistryBaseDirType.BaseDir_NeverwinterNights);
        }

        #endregion

        #region Methods.Helpers

        /// <summary>
        /// Raises the PropertyChanged event for the Status property of this file item instance
        /// </summary>
        public void IndicateStatusChanged()
        {
            base.OnPropertyChanged("Status");
        }

        #endregion

        #endregion
    }
}
