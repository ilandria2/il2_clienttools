﻿namespace il2_clienttools.Model
{
    #region Namespaces

    using ViewModel;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System;
    using il2_clienttools.Helper;

    #endregion

    #region Enum

    /// <summary>
    /// Defines the Gender type
    /// </summary>
    public enum Gender : int
    {
        [Description("(Unknown gender)")]
        UnknownGender = 255,

        Male = 0,

        Female = 1,

        [Description("(Other gender)")]
        OtherGender = 2,
    }

    /// <summary>
    /// Defines the Race type
    /// </summary>
    public enum Race : int
    {
        [Description("(Unknown race")]
        UnknownRace = 255,
        
        Human = 6,

        [Description("(Other race)")]
        OtherRace = 99
    }

    #endregion

    /// <summary>
    /// Light version of the BIC gff type with the ability to export the structure to a .bic file
    /// </summary>
    public class Model_Character : ViewModel_Base
    {
        #region Constructors

        /// <summary>
        /// Constructs a new instance of a player character
        /// </summary>
        public Model_Character()
        {
            this.Age = 18;
            this.CurrentHitPoints = -11;
            this.Deity = string.Empty;
            this.Description = string.Empty;
            this.Gender = Gender.Male;
            this.HairType = 0x00;
            this.HeadType = 0x01;
            this.MaxHitPoints = 100;
            this.MovementRate = 0x00;
            this.Phenotype = 0x00;
            this.PrivateName = string.Empty;
            this.Race = Race.Human;
            this.ResRef = string.Empty;
            this.SkinType = 0x00;
            this.SoundSetType = 0x00;
            this.Tag = string.Empty;
            this.RegisteredOn = default(DateTime);
        }

        /// <summary>
        /// Constructs a new instance of a player character with a player id
        /// </summary>
        /// <param name="playerId"></param>
        public Model_Character(int playerId) : this()
        {
            this.PlayerID = playerId;
        }

        /// <summary>
        /// Constructs a new instance of a player character with a player id and lsit of quick slot feats
        /// </summary>
        /// <param name="playerId">The player's persistent ID</param>
        /// <param name="qbSlots">Dictionary of quickSlotId and FeatId values</param>
        /// <param name="feats">Array of featId values</param>
        public Model_Character(int playerId, Dictionary<int, int> qbSlots, int[] feats ) : this(playerId)
        {
            foreach (var slot in qbSlots)
                QuickSlots.Add(new QuickSlotFeat(slot.Key, slot.Value));

            Feats.AddRange(feats);
        }

        #endregion

        #region Fields

        private Gender _gender;
        private Race _race;
        private byte _skinType;
        private byte _headType;
        private byte _hairType;
        private byte _soundSetType;
        private string _privateName;
        private string _description;
        private int _age;
        private List<int> _feats;
        private short _maxHitPoints;
        private byte _movementRate;
        private int _phenotype;
        private string _tag;
        private string _resRef;
        private List<QuickSlotFeat> _quickSlots;
        private string _deity;
        private int _currentHitPoints;
        private DateTime _registeredOn;
        private bool _creationMode;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets whether the character is currently in creation mode
        /// </summary>
        public bool CreationMode
        {
            get { return _creationMode; }
            set 
            {
                if (_creationMode == value) return;
                _creationMode = value;
                base.OnPropertyChanged("CreationMode");
                base.OnPropertyChanged("CharacterInfo");
            }
        }

        /// <summary>
        /// Gets or sets the player id (Deity, ResRef and Tag)
        /// </summary>
        public int PlayerID 
        {
            get 
            {
                int playerId = 0;
                if (!string.IsNullOrEmpty(Deity) && int.TryParse(Deity, out playerId))
                    return playerId;

                if (!string.IsNullOrEmpty(ResRef) && int.TryParse(ResRef, out playerId))
                    return playerId;
                
                if (!string.IsNullOrEmpty(Tag) && int.TryParse(Tag, out playerId))
                    return playerId;

                return Common.PLAYER_ID_NULL;
            }
            set
            {
                string playerID = value == Common.PLAYER_ID_NULL ? string.Empty : value.ToString();
                Deity = playerID;
                ResRef = playerID;
                Tag = playerID;
            }
        }

        /// <summary>
        /// Gets or sets when this character model was registered in game
        /// </summary>
        public DateTime RegisteredOn
        {
            get { return _registeredOn; }
            set { _registeredOn = value; }
        }

        /// <summary>
        /// Gets whether this character model was ever registered in game
        /// </summary>
        public bool Registered 
        {
            get { return RegisteredOn != default(DateTime); }
        }

        /// <summary>
        /// Gets whether this character model was never registered in game
        /// </summary>
        public bool NotRegistered 
        {
            get { return !Registered; }
        }

        /// <summary>
        /// Gets whether the character is dead
        /// </summary>
        public bool IsDead
        {
            get { return CurrentHitPoints < -10 && Registered; }
        }

        /// <summary>
        /// Gets or sets the current hit points of the character
        /// </summary>
        public int CurrentHitPoints
        {
            get { return _currentHitPoints; }
            set 
            {
                if (_currentHitPoints == value) return;

                _currentHitPoints = value;

                base.OnPropertyChanged("CurrentHitPoints");
                base.OnPropertyChanged("IsDead");
            }
        }

        /// <summary>
        /// Gender type (see gender.2da)
        /// </summary>
        public Gender Gender
        {
            get { return _gender; }
            set 
            {
                if (_gender == value) return;

                _gender = value;

                base.OnPropertyChanged("Gender");
                base.OnPropertyChanged("CharacterInfo");
            }
        }

        /// <summary>
        /// Race type (see racialtypes.2da)
        /// </summary>
        public Race Race
        {
            get { return _race; }
            set 
            {
                if (_race == value) return;

                _race = value;

                base.OnPropertyChanged("Race");
                base.OnPropertyChanged("CharacterInfo");
            }
        }

        /// <summary>
        /// Head appearance type (0-255)
        /// </summary>
        public byte HeadType
        {
            get { return _headType; }
            set 
            {
                if (_headType == value) return;

                _headType = value;

                base.OnPropertyChanged("HeadType");
            }
        }

        /// <summary>
        /// Skin color type (0-255)
        /// </summary>
        public byte SkinType
        {
            get { return _skinType; }
            set 
            {
                if (_skinType == value) return;

                _skinType = value;

                base.OnPropertyChanged("SkinType");
            }
        }

        /// <summary>
        /// hair color type (0-255)
        /// </summary>
        public byte HairType
        {
            get { return _hairType; }
            set 
            {
                if (_hairType == value) return;

                _hairType = value;

                base.OnPropertyChanged("HairType");
            }
        }

        /// <summary>
        /// SoundSet ID (see soundset.2da)
        /// </summary>
        public byte SoundSetType
        {
            get { return _soundSetType; }
            set 
            {
                if (_soundSetType == value) return;

                _soundSetType = value;

                base.OnPropertyChanged("SoundSetType");
            }
        }

        /// <summary>
        /// Private character name (only player itself will see it)
        /// </summary>
        public string PrivateName
        {
            get { return _privateName; }
            set 
            {
                if (_privateName == value) return;

                _privateName = value;

                base.OnPropertyChanged("PrivateName");
                base.OnPropertyChanged("DisplayName");
            }
        }

        /// <summary>
        /// Public visible description (displayed upon object examine)
        /// </summary>
        public string Description
        {
            get { return _description; }
            set 
            {
                if (_description == value) return;

                _description = value;

                base.OnPropertyChanged("Description");
            }
        }

        /// <summary>
        /// Age
        /// </summary>
        public int Age
        {
            get { return _age; }
            set 
            {
                if (_age == value) return;

                _age = value;

                base.OnPropertyChanged("Age");
                base.OnPropertyChanged("CharacterInfo");
            }
        }

        /// <summary>
        /// List of feat IDs (see feat.2da)
        /// </summary>
        public List<int> Feats
        {
            get
            {
                if (_feats == null)
                    _feats = new List<int>();

                return _feats;
            }
        }

        /// <summary>
        /// Max hitpoints of the character
        /// </summary>
        public short MaxHitPoints
        {
            get { return _maxHitPoints; }
            set 
            {
                if (_maxHitPoints == value) return;

                _maxHitPoints = value;

                base.OnPropertyChanged("MaxHitPoints");
            }
        }

        /// <summary>
        /// Creature's speed - see creaturespeed.2da and appearance.2da
        /// </summary>
        public byte MovementRate
        {
            get { return _movementRate; }
            set 
            {
                if (_movementRate == value) return;

                _movementRate = value;

                base.OnPropertyChanged("MovementRate");
            }
        }

        /// <summary>
        /// Phenotype (see phenotypes.2da)
        /// </summary>
        public int Phenotype
        {
            get { return _phenotype; }
            set 
            {
                if (_phenotype == value) return;

                _phenotype = value;

                base.OnPropertyChanged("Phenotype");
            }
        }

        /// <summary>
        /// Tag
        /// </summary>
        public string Tag
        {
            get { return _tag; }
            set 
            {
                if (_tag == value) return;

                _tag = value;

                base.OnPropertyChanged("Tag");
            }
        }

        /// <summary>
        /// blueprint template ResRef
        /// </summary>
        public string ResRef
        {
            get { return _resRef; }
            set 
            {
                if (_resRef == value) return;

                _resRef = value;

                base.OnPropertyChanged("ResRef");
            }
        }

        /// <summary>
        /// List of quickslots holding a feat id
        /// see QuickSlotFeat for definition
        /// </summary>
        public List<QuickSlotFeat> QuickSlots
        {
            get
            {
                if (_quickSlots == null)
                    _quickSlots = new List<QuickSlotFeat>();

                return _quickSlots;
            }
        }

        /// <summary>
        /// Deity (or player id)
        /// </summary>
        public string Deity
        {
            get { return _deity; }
            set 
            {
                if (_deity == value) return;

                _deity = value;

                base.OnPropertyChanged("Deity");
            }
        }

        /// <summary>
        /// Inline short info of the character type (gender, race, age)
        /// </summary>
        public string CharacterInfo
        {
            get 
            {
                if (Registered || CreationMode)
                    return string.Format("{0}, {1}, {2} yr", this.Gender, this.Race, this.Age);
                else
                    return "Registration required";
            }
        }

        /// <summary>
        /// Gets the raw data of the generated Gff structure
        /// </summary>
        public byte[] RawData
        {
            get
            {
                return GenerateStructure(new Gff("bic")).RawData;
            }
        }

        #endregion

        #region Properties.Overrides

        public override string DisplayName
        {
            get
            {
                return ToString();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new test character
        /// </summary>
        public static void CreateNewTestCharacter()
        {
            string file = @"c:\_mydocs\games\Neverwinter Nights\servervault\test";

            var pc = new Model_Character(Common.PLAYER_ID_NULL);

            pc.QuickSlots.AddRange(new[] 
                { 
                    new QuickSlotFeat(0, 1600), 
                    new QuickSlotFeat(1, 1601),
                    new QuickSlotFeat(20, 1625)
                });

            pc.Race = Race.Human;
            pc.Age = 21;
            pc.Description = "Generated character";
            pc.Feats.AddRange(new[] { 1600, 1601, 1625 });
            pc.Gender = Gender.Female;
            pc.HairType = 0x03;
            pc.SkinType = 0x01;
            pc.HeadType = 0x01;
            pc.SoundSetType = 230;
            pc.Tag = pc.Deity;
            pc.ResRef = pc.Deity;
            pc.MaxHitPoints = 1784;
            pc.MovementRate = 9;
            pc.Phenotype = 0;
            pc.PrivateName = "Lady Avariel White";

            pc.Export(file);
        }

        /// <summary>
        /// Export to a file
        /// </summary>
        /// <param name="folder"></param>
        public void Export(string folder)
        {
            var gff = GenerateStructure(new Gff("bic"));

            gff.Export(string.Format(@"{0}\{1}_{2}.bic", Path.GetDirectoryName(folder + @"\"), "il2_bic", PlayerID));
        }

        private Gff GenerateStructure(Gff gff)
        {
            gff.Add("Conversation", new TCResRef(string.Empty));
            gff.Add("Age", new TInt(this.Age));
            gff.Add("Con", new TByte(10));
            gff.Add("AmbientAnimState", new TByte(0));
            gff.Add("AnimationDay", new TDword(0));
            gff.Add("AnimationTime", new TDword(0));
            gff.Add("Appearance_Head", new TByte(this.HeadType));
            gff.Add("Appearance_Type", new TWord(6));
            gff.Add("AreaId", new TDword(Gff.OBJECT_INVALID));
            gff.Add("ArmorClass", new TShort(0));
            gff.Add("ArmorPart_RFoot", new TByte(1));
            gff.Add("BaseAttackBonus", new TByte(0));

            #region Body parts

            gff.Add("BodyBag", new TByte(0));
            gff.Add("BodyBagId", new TDword(Gff.OBJECT_INVALID));

            gff.Add("BodyPart_Belt", new TByte(0));

            gff.Add("BodyPart_LBicep", new TByte(1));
            gff.Add("BodyPart_LFArm", new TByte(1));
            gff.Add("BodyPart_LFoot", new TByte(1));
            gff.Add("BodyPart_LHand", new TByte(1));
            gff.Add("BodyPart_LShin", new TByte(1));
            gff.Add("BodyPart_LShoul", new TByte(0));
            gff.Add("BodyPart_LThigh", new TByte(1));

            gff.Add("BodyPart_Neck", new TByte(1));
            gff.Add("BodyPart_Pelvis", new TByte(1));

            gff.Add("BodyPart_RBicep", new TByte(1));
            gff.Add("BodyPart_RFArm", new TByte(1));
            gff.Add("BodyPart_RHand", new TByte(1));
            gff.Add("BodyPart_RShin", new TByte(1));
            gff.Add("BodyPart_RShoul", new TByte(0));
            gff.Add("BodyPart_RThigh", new TByte(1));

            gff.Add("BodyPart_Torso", new TByte(1));

            #endregion

            gff.Add("Cha", new TByte(10));
            gff.Add("ChallengeRating", new TFloat(0.0f));

            #region ClassList

            var class1 = new TStruct(0x0002);
            var classList = new TList();

            classList.Add(class1);

            gff.Add("ClassList", classList);
            gff.Add(class1, "Class", new TInt(4));
            gff.Add(class1, "ClassLevel", new TShort(10));

            #endregion

            gff.Add("Color_Hair", new TByte(this.HairType));
            gff.Add("Color_Skin", new TByte(this.SkinType));
            gff.Add("Color_Tattoo1", new TByte(0));
            gff.Add("Color_Tattoo2", new TByte(0));

            #region CombatInfo

            var combatInfo = new TStruct(0xCAAA);

            gff.Add("CombatInfo", combatInfo);
            gff.Add(combatInfo, "ArcaneSpellFail", new TByte(0));
            gff.Add(combatInfo, "ArmorCheckPen", new TByte(0));

            #region AttackList

            var attackList = new TList();

            gff.Add(combatInfo, "AttackList", attackList);

            #endregion

            gff.Add(combatInfo, "CreatureDice", new TByte(0));
            gff.Add(combatInfo, "CreatureDie", new TByte(0));
            gff.Add(combatInfo, "DamageDice", new TByte(1));
            gff.Add(combatInfo, "DamageDie", new TByte(6));

            #region DamageList

            var damageList = new TList();

            gff.Add(combatInfo, "DamageList", damageList);

            #endregion

            gff.Add(combatInfo, "eatureDice", new TByte(0));
            gff.Add(combatInfo, "eatureDie", new TByte(0));
            gff.Add(combatInfo, "LeftEquip", new TDword(Gff.OBJECT_INVALID));
            gff.Add(combatInfo, "LeftString", new TCExoString(string.Empty));
            gff.Add(combatInfo, "NumAttacks", new TByte(1));
            gff.Add(combatInfo, "OffHandAttackMod", new TChar('\0'));
            gff.Add(combatInfo, "OffHandCritMult", new TByte(2));
            gff.Add(combatInfo, "OffHandCritRng", new TByte(1));
            gff.Add(combatInfo, "OffHandDamageMod", new TChar('\0'));
            gff.Add(combatInfo, "OffHandWeaponEq", new TByte(0));
            gff.Add(combatInfo, "OnHandAttackMod", new TChar('\0'));
            gff.Add(combatInfo, "OnHandCritMult", new TByte(2));
            gff.Add(combatInfo, "OnHandCritRng", new TByte(1));
            gff.Add(combatInfo, "OnHandDamageMod", new TChar('\0'));
            gff.Add(combatInfo, "reatureDice", new TByte(0));
            gff.Add(combatInfo, "reatureDie", new TByte(0));
            gff.Add(combatInfo, "RightEquip", new TDword(Gff.OBJECT_INVALID));
            gff.Add(combatInfo, "RightString", new TCExoString(string.Empty));
            gff.Add(combatInfo, "SpellResistance", new TByte(0));
            gff.Add(combatInfo, "UnarmedDamDice", new TByte(1));
            gff.Add(combatInfo, "UnarmedDamDie", new TByte(3));

            #endregion

            #region CombatRoundData

            var combatRoundData = new TStruct(0xCADA);

            gff.Add("CombatRoundData", combatRoundData);

            #endregion

            gff.Add("CreatnScrptFird", new TByte(1));
            gff.Add("CreatureSize", new TInt(3));
            gff.Add("CurrentHitPoints", new TShort(MaxHitPoints));
            gff.Add("DeadSelectable", new TByte(1));
            gff.Add("DecayTime", new TDword(5000));
            gff.Add("Deity", new TCExoString(this.Deity));
            gff.Add("Description", new TCExoLocString(this.Description));
            gff.Add("DescriptionOverr", new TCExoString(string.Empty));
            gff.Add("DetectMode", new TByte(0));
            gff.Add("Dex", new TByte(10));
            gff.Add("Disarmable", new TByte(1));
            gff.Add("EncounterObject", new TDword(Gff.OBJECT_INVALID));

            #region EquipItemList

            var equipItemList = new TList();

            gff.Add("EquipItemList", equipItemList);

            #endregion

            gff.Add("Experience", new TDword(0));
            gff.Add("FactionID", new TWord(80));

            #region FeatList

            var featList = new TList();
            byte typeStructFeat = 0x1;

            foreach (var feat in this.Feats)
                featList.Add(new TStruct(typeStructFeat));

            gff.Add("FeatList", featList);

            for (int i = 0; i < Feats.Count; i++)
                gff.Add(featList.Structs[i], "Feat", new TWord((ushort)Feats[i]));

            #endregion

            string[] splitName = PrivateName.Split(new[] { ' ' });
            string firstName = splitName[0];
            string lastName = splitName.Length > 1 ? PrivateName.Substring(PrivateName.IndexOf(' ') + 1) : string.Empty;
            gff.Add("FirstName", new TCExoLocString(firstName));
            gff.Add("FootstepType", new TInt(-1));
            gff.Add("fortbonus", new TShort(0));
            gff.Add("FortSaveThrow", new TChar((char)2));
            gff.Add("Gender", new TByte((byte)Gender));
            gff.Add("Gold", new TDword(0));
            gff.Add("GoodEvil", new TByte(50));
            gff.Add("HitPoints", new TShort(MaxHitPoints));
            gff.Add("Int", new TByte(10));
            gff.Add("Interruptable", new TByte(1));
            gff.Add("IsCommandable", new TByte(1));
            gff.Add("IsDestroyable", new TByte(1));
            gff.Add("IsDM", new TByte(0));
            gff.Add("IsImmortal", new TByte(0));
            gff.Add("IsPC", new TByte(1));
            gff.Add("IsRaiseable", new TByte(1));

            #region ItemList

            var itemList = new TList();

            gff.Add("ItemList", itemList);

            #endregion

            gff.Add("LastName", new TCExoLocString(lastName));
            gff.Add("LawfulChaotic", new TByte(50));
            gff.Add("Listening", new TByte(0));
            gff.Add("Lootable", new TByte(0));

            #region LvlStatList

            var lvlStatList = new TList();
            var hpleft = MaxHitPoints;
            var levels = 10;

            for (int i = 0; i < levels; i++)
                lvlStatList.Add(new TStruct());

            gff.Add("LvlStatList", lvlStatList);

            for (int i = 0; i < levels; i++)
            {
                var level = lvlStatList.Structs[i];

                #region Hitpoints

                var hp = (byte)(hpleft > byte.MaxValue ? byte.MaxValue : hpleft);

                hpleft -= hp;

                if (hpleft <= levels - i)
                {
                    //hpleft += (short)(hp - (levels - i + 1));
                    hpleft = (byte)(levels - i - 1);
                    hp -= (byte)(hpleft);
                }

                #endregion

                gff.Add(level, "EpicLevel", new TByte(0));

                #region FeatList

                var lvlFeatList = new TList();

                if (i == 0)
                    for (int f = 0; f < Feats.Count; f++)
                        lvlFeatList.Add(new TStruct());

                gff.Add(level, "FeatList", lvlFeatList);

                // grant all feats on level 1
                if (i == 0)
                    for (int f = 0; f < Feats.Count; f++)
                        gff.Add(lvlFeatList.Structs[f], "Feat", new TWord((ushort)Feats[f]));

                #endregion

                gff.Add(level, "LvlStatClass", new TByte(4));
                gff.Add(level, "LvlStatHitDie", new TByte(hp));

                #region SkillList

                var skillList = new TList();

                gff.Add(level, "SkillList", skillList);

                #endregion

                gff.Add(level, "SkillPoints", new TWord(0));
            }

            #endregion

            gff.Add("MasterID", new TDword(Gff.OBJECT_INVALID));
            gff.Add("MaxHitPoints", new TShort(MaxHitPoints));
            gff.Add("MClassLevUpIn", new TByte(0));
            gff.Add("MovementRate", new TByte(0));
            gff.Add("NaturalAC", new TByte(0));
            gff.Add("NoPermDeath", new TByte(0));
            gff.Add("OverrideBAB", new TByte(0));

            #region PerceptionList

            var perceptionList = new TList();

            gff.Add("PerceptionList", perceptionList);

            #endregion

            gff.Add("Phenotype", new TInt(Phenotype));
            gff.Add("Plot", new TByte(0));
            gff.Add("PM_IsPolymorphed", new TByte(0));
            gff.Add("Portrait", new TCResRef(string.Empty));
            gff.Add("PregameCurrent", new TShort(100));

            #region QBList

            var qbList = new TList();
            var qb_XF1 = new TStruct();
            var qb_XF2 = new TStruct();
            var qb_XF3 = new TStruct();
            var qb_XF4 = new TStruct();
            var qb_XF5 = new TStruct();
            var qb_XF6 = new TStruct();
            var qb_XF7 = new TStruct();
            var qb_XF8 = new TStruct();
            var qb_XF9 = new TStruct();
            var qb_XF10 = new TStruct();
            var qb_XF11 = new TStruct();
            var qb_XF12 = new TStruct();

            var qb_CF1 = new TStruct();
            var qb_CF2 = new TStruct();
            var qb_CF3 = new TStruct();
            var qb_CF4 = new TStruct();
            var qb_CF5 = new TStruct();
            var qb_CF6 = new TStruct();
            var qb_CF7 = new TStruct();
            var qb_CF8 = new TStruct();
            var qb_CF9 = new TStruct();
            var qb_CF10 = new TStruct();
            var qb_CF11 = new TStruct();
            var qb_CF12 = new TStruct();

            var qb_SF1 = new TStruct();
            var qb_SF2 = new TStruct();
            var qb_SF3 = new TStruct();
            var qb_SF4 = new TStruct();
            var qb_SF5 = new TStruct();
            var qb_SF6 = new TStruct();
            var qb_SF7 = new TStruct();
            var qb_SF8 = new TStruct();
            var qb_SF9 = new TStruct();
            var qb_SF10 = new TStruct();
            var qb_SF11 = new TStruct();
            var qb_SF12 = new TStruct();

            qbList.Add(qb_XF1);
            qbList.Add(qb_XF2);
            qbList.Add(qb_XF3);
            qbList.Add(qb_XF4);
            qbList.Add(qb_XF5);
            qbList.Add(qb_XF6);
            qbList.Add(qb_XF7);
            qbList.Add(qb_XF8);
            qbList.Add(qb_XF9);
            qbList.Add(qb_XF10);
            qbList.Add(qb_XF11);
            qbList.Add(qb_XF12);

            qbList.Add(qb_CF1);
            qbList.Add(qb_CF2);
            qbList.Add(qb_CF3);
            qbList.Add(qb_CF4);
            qbList.Add(qb_CF5);
            qbList.Add(qb_CF6);
            qbList.Add(qb_CF7);
            qbList.Add(qb_CF8);
            qbList.Add(qb_CF9);
            qbList.Add(qb_CF10);
            qbList.Add(qb_CF11);
            qbList.Add(qb_CF12);

            qbList.Add(qb_SF1);
            qbList.Add(qb_SF2);
            qbList.Add(qb_SF3);
            qbList.Add(qb_SF4);
            qbList.Add(qb_SF5);
            qbList.Add(qb_SF6);
            qbList.Add(qb_SF7);
            qbList.Add(qb_SF8);
            qbList.Add(qb_SF9);
            qbList.Add(qb_SF10);
            qbList.Add(qb_SF11);
            qbList.Add(qb_SF12);

            gff.Add("QBList", qbList);

            int q = 0;

            for (int i = 0; i < qbList.Structs.Count; i++)
            {
                if (q < QuickSlots.Count && i == QuickSlots[q].Position)
                {
                    gff.Add(qbList.Structs[i], "QBINTParam1", new TInt(QuickSlots[q].Id));
                    gff.Add(qbList.Structs[i], "QBObjectType", new TByte(4));

                    q++;
                }
                else
                    gff.Add(qbList.Structs[i], "QBObjectType", new TByte(0));
            }

            #endregion

            gff.Add("Race", new TByte((byte)Race));
            gff.Add("refbonus", new TShort(0));
            gff.Add("RefSaveThrow", new TChar((char)0));

            #region Scripts

            gff.Add("ScriptAttacked", new TCResRef("default"));
            gff.Add("ScriptDamaged", new TCResRef("default"));
            gff.Add("ScriptDeath", new TCResRef("default"));
            gff.Add("ScriptDialogue", new TCResRef("default"));
            gff.Add("ScriptDisturbed", new TCResRef("default"));
            gff.Add("ScriptEndRound", new TCResRef("default"));
            gff.Add("ScriptHeartbeat", new TCResRef("default"));
            gff.Add("ScriptOnBlocked", new TCResRef("default"));
            gff.Add("ScriptOnNotice", new TCResRef("default"));
            gff.Add("ScriptRested", new TCResRef("default"));
            gff.Add("ScriptSpawn", new TCResRef("default"));
            gff.Add("ScriptSpellAt", new TCResRef("default"));
            gff.Add("ScriptUserDefine", new TCResRef("default"));

            #endregion

            gff.Add("SitObject", new TDword(Gff.OBJECT_INVALID));

            #region SkillList

            var skillList1 = new TList();

            gff.Add("SkillList", skillList1);

            #endregion

            gff.Add("SkillPoints", new TWord(0));
            gff.Add("SoundSetFile", new TWord(SoundSetType));
            gff.Add("StartingPackage", new TByte(0));
            gff.Add("StealthMode", new TByte(0));
            gff.Add("Str", new TByte(10));
            gff.Add("Subrace", new TCExoString(string.Empty));
            gff.Add("Tag", new TCExoString(Tag));
            gff.Add("Tail_New", new TDword(0));
            gff.Add("TemplateResRef", new TCResRef(ResRef));
            gff.Add("willbonus", new TShort(0));
            gff.Add("WillSaveThrow", new TChar((char)0));
            gff.Add("Wings_New", new TDword(0));
            gff.Add("Wis", new TByte(10));
            gff.Add("XOrientation", new TFloat());
            gff.Add("XPosition", new TFloat());
            gff.Add("YOrientation", new TFloat());
            gff.Add("YPosition", new TFloat());
            gff.Add("ZOrientation", new TFloat());
            gff.Add("ZPosition", new TFloat());

            return gff;
        }

        #endregion

        #region Methods.Overrides

        /// <summary>
        /// Gets the string representation of the Character model
        /// </summary>
        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(this.PrivateName) ? "NewCharacter" : this.PrivateName;
        }

        #endregion

    }

}
