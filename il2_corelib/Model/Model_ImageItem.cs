﻿
namespace il2_clienttools.Model
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.Windows.Media;
using System.Windows.Media.Imaging;
    
    #endregion

    /// <summary>
    /// Model that binds Uri, Color and Label
    /// Used in horizontal image list
    /// </summary>
    public class Model_ImageItem : NotifyBase
    {
        #region Fields

        private string _path;
        private string _displayName;
        private Brush _brush;
        private BitmapImage _image;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Image property
        /// </summary>
        public BitmapImage Source
        {
            get 
            {
                if (string.IsNullOrWhiteSpace(Path)) 
                    return null;

                if (_image == null)
                {
                    _image = new BitmapImage();
                    _image.BeginInit();
                    _image.CacheOption = BitmapCacheOption.OnLoad;
                    _image.UriSource = new Uri(Path);
                    _image.EndInit();
                    _image.Freeze();
                }

                return _image; 
            }
        }

        /// <summary>
        /// Gets or sets the Path
        /// </summary>
        public string Path
        {
            get { return _path; }
            set 
            {
                if (_path == value) return;

                _path = value;
                _image = null;

                base.OnPropertyChanged("Path");
                base.OnPropertyChanged("Image");
            }
        }

        /// <summary>
        /// Gets or sets the DisplayName
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set 
            {
                if (_displayName == value) return;

                _displayName = value;

                base.OnPropertyChanged("DisplayName");
            }
        }

        /// <summary>
        /// Gets or sets the Brush
        /// </summary>
        public Brush Brush
        {
            get { return _brush; }
            set 
            {
                if (_brush == value) return;

                _brush = value;

                base.OnPropertyChanged("Brush");
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an empty ImageItem
        /// </summary>
        public Model_ImageItem()
            : this(null, null, null)
        {

        }

        /// <summary>
        /// Constructs an ImageItem with the given path
        /// </summary>
        /// <param name="path"></param>
        public Model_ImageItem(string path) 
            : this(path, string.Empty, null)
        {
        }

        /// <summary>
        /// Constructs an ImageItem with the given brush
        /// </summary>
        /// <param name="brush"></param>
        public Model_ImageItem(Brush brush)
            : this(null, null, brush)
        {

        }

        public Model_ImageItem(Color color)
            : this(null, null, new RadialGradientBrush(Color.Add(Color.FromRgb(128, 128, 128), color), color))
        { 
        
        }

        /// <summary>
        /// Constructs an ImageItem with the given path, name and brush
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <param name="brush"></param>
        public Model_ImageItem(string path, string name, Brush brush)
        {
            _path = path;
            _displayName = name;
            _brush = brush;
        }

        #endregion

        #region Methods.Overrides

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}", DisplayName, Brush, Path);
        }

        #endregion
    }
}
