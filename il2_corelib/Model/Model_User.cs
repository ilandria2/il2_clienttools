﻿namespace il2_clienttools.Model
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;

    #endregion

    public class Model_User : NotifyBase
    {
        #region Constructors
        
        /// <summary>
        /// Constructs the User model
        /// </summary>
        public Model_User(string user, string email)
        {
            this.UserName = user;
            this.Email = email;
        }

        #endregion

        #region Fields

        private string _userName;
        private string _email;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the user name of the User
        /// </summary>
        public string UserName
        {
            get { return this._userName; }
            private set { this._userName = value; }
        }

        /// <summary>
        /// Gets the Email of this User
        /// </summary>
        public string Email
        {
            get { return this._email; }
            private set { this._email = value; }
        }

        #endregion

        #region Methods.Overrides

        /// <summary>
        /// Gets the string representation of the User model
        /// </summary>
        public override string ToString()
        {
            return string.Format("{0} [{1}]", this.UserName, this.Email);
        }

        #endregion
    }
}
