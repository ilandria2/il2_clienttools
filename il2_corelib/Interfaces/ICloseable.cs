﻿namespace il2_corelib.Interfaces
{
    public interface ICloseable
    {
        void Close();
    }
}
