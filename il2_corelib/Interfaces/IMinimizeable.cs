﻿namespace il2_corelib.Interfaces
{
    public interface IMinimizeable : ICloseable
    {
        void Minimize();
    }
}
