﻿
namespace il2_clienttools.Converters
{
    #region Namespaces

    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    #endregion

    /// <summary>
    /// Visibility to Boolean value converter
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class Converter_VisibilityToBoolean : IValueConverter
    {
        #region IValueConverter.Methods

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return null;

            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Visibility)) return null;

            return (Visibility)value == Visibility.Visible ? true : false;
        }

        #endregion
    }

    /// <summary>
    /// Visibility to Boolean multiple values converter
    /// </summary>
    //[ValueConversion(typeof(bool[]), typeof(Visibility))]
    public sealed class MultiConverter_VisibilityToBoolean : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string param = parameter != null ? parameter.ToString().ToUpper() : "AND";
            Visibility result = Visibility.Visible;

            foreach (var value in values)
            {
                if (value is bool)
                {
                    // OR
                    if (param == "OR")
                    {
                        if ((bool)value)
                            return Visibility.Visible;
                        result = Visibility.Collapsed;
                    }

                    // AND
                    else if (param == "AND")
                    {
                        if (!(bool)value)
                            return Visibility.Collapsed;
                        result = Visibility.Visible;
                    }
                }
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
