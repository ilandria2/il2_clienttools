﻿
namespace il2_clienttools.Data
{
    #region Namespaces

    using System.ComponentModel;

    #endregion



    /// <summary>
    /// Implemented hasher/checksum methods
    /// </summary>
    public enum ChecksumMethod
    {
        [Description("MD5")]
        MD5,
    }

    /// <summary>
    /// The hash/checksum value computed by a specific hasher type
    /// </summary>
    public struct Data_Checksum
    {
        #region Fields

        public string Value;
        public ChecksumMethod Method;

        #endregion

        #region Methods

        #region Methods.Overrides

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Data_Checksum))
                return false;

            var data = (Data_Checksum)obj;

            return
                this.Value == data.Value &&
                this.Method == data.Method;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 1000 + (this.Value ?? string.Empty).GetHashCode() + this.Method.GetHashCode();
            }
        }

        public override string ToString()
        {
            return this.Value;
        }

        #endregion //Methods.Overrides

        #endregion //Methods
    }
}
