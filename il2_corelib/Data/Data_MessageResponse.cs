﻿
namespace il2_clienttools.Data
{
    #region Namespaces
    
    using System;
    
    #endregion

    /// <summary>
    /// Holds the WebClient message response data
    /// </summary>
    public struct Data_MessageResponse
    {
        #region Constants

        public static readonly int MESSAGE_COLUMN_COUNT = 10;

        #endregion

        #region Fields

        private string _requestor;
        private int _requestorId;
        private string _clientType;
        private bool _isTestEnvironment;
        private int _type;
        private int _subType;
        private int _rawDataId;
        private string _parameter;
        private string _data;
        private Exception _exceptionObject;

        #endregion

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="Data_MessageResponse"/> structure
        /// </summary>
        /// <param name="requestor">Username</param>
        /// <param name="requestorId">User id</param>
        /// <param name="clientType">SMF | GAME</param>
        /// <param name="isTest">bool</param>
        /// <param name="type">Message Type</param>
        /// <param name="subType">Message SubType</param>
        /// <param name="rawDataId">RawData id</param>
        /// <param name="param">Message Parameter</param>
        /// <param name="data">Response data</param>
        /// <param name="exception">Error object</param>
        public Data_MessageResponse(string requestor, int requestorId, string clientType, bool isTest, int type, int subType, int rawDataId, string param, string data, Exception exception)
        {
            #region Initialize default data structure

            _requestor = string.Empty;
            _requestorId = -1;
            _clientType = string.Empty;
            _isTestEnvironment = false;
            _type = 0;
            _subType = 0;
            _rawDataId = -1;
            _parameter = string.Empty;
            _data = string.Empty;
            _exceptionObject = null;

            #endregion

            Requestor = requestor;
            RequestorId = requestorId;
            ClientType = clientType;
            IsTestEnvironment = isTest;
            Type = type;
            SubType = subType;
            RawDataId = rawDataId;
            Parameter = param;
            Data = data;
            ExceptionObject = exception;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Data_MessageResponse"/> structure using only the ExceptionObject as parameter
        /// </summary>
        /// <param name="exception"></param>
        public Data_MessageResponse(Exception exception)
        {
            #region Initialize default data structure


            _requestor = string.Empty;
            _requestorId = -1;
            _clientType = string.Empty;
            _isTestEnvironment = false;
            _type = 0;
            _subType = 0;
            _rawDataId = -1;
            _parameter = string.Empty;
            _data = string.Empty;
            _exceptionObject = null;

            #endregion

            ExceptionObject = exception;
        }

        #endregion

        #region Properties

        public string ClientType 
        {
            get { return _clientType; }
            private set { _clientType = value; } 
        }

        public bool IsTestEnvironment 
        {
            get { return _isTestEnvironment; }
            private set { _isTestEnvironment = value; } 
        }

        public int Type 
        {
            get { return _type; }
            private set { _type = value; } 
        }

        public int SubType 
        {
            get { return _subType; }
            private set { _subType = value; }
        }

        public string Parameter 
        {
            get { return _parameter; }
            private set { _parameter = value; }
        }

        public string Data 
        {
            get { return _data; }
            private set { _data = value; }
        }

        public Exception ExceptionObject 
        {
            get { return _exceptionObject; }
            private set { _exceptionObject = value; } 
        }

        public int RawDataId 
        {
            get { return _rawDataId; }
            private set { _rawDataId = value; }
        }

        public int RequestorId 
        {
            get { return _requestorId; }
            private set { _requestorId = value; }
        }

        public string Requestor 
        {
            get { return _requestor; }
            private set { _requestor = value; }
        }

        #endregion
    }
}
