using System.Reflection;
using System.Runtime.InteropServices;



/************ Local assembly info *************/

[assembly: AssemblyTitle("CoreLib")]
[assembly: AssemblyDescription("Assembly core for Ilandria 2 client tools.")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("E640E428-A2FF-4D4F-8D95-14E0D1F16679")]

//VERSION=0.6.111
[assembly: AssemblyInformationalVersion("0.6.111")]
[assembly: AssemblyFileVersion("0.6.111")]
[assembly: AssemblyVersion("0.6.111")]
