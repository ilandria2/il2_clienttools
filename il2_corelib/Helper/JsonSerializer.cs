﻿
namespace il2_clienttools.Helper
{
    #region Namespaces

    using System;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;

    #endregion

    public static class Json
    {
        /// <summary>
        /// Deserializes a Json string and returns an instance of an object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        public static T Deserialize<T>(string jsonString) 
        {
            var serializer = new DataContractJsonSerializer(typeof(T));

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
                return (T)serializer.ReadObject(stream);
        }
    }
}
