﻿namespace il2_clienttools.Helper
{
    #region Namespaces

    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using System.Windows;

    #endregion

    public static class ApplicationExtension
    {
        private static Mutex mutex;

        public static bool IsOneTimeLaunch(this Application application, string uniqueName = null)
        {
            uniqueName = uniqueName ?? string.Format("{0}_{1}_{2}",
                Environment.MachineName,
                Environment.UserName,
                Path.GetFileName(Assembly.GetEntryAssembly().GetName().Name));

            application.Exit += (sender, e) => mutex.ReleaseMutex();

            bool isOneTimeLaunch;

            mutex = new Mutex(true, uniqueName, out isOneTimeLaunch);
            
            return isOneTimeLaunch;
        }
    }
}
