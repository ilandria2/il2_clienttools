﻿namespace il2_clienttools.Helper
{
    #region Namespaces

    using System;
    using System.ComponentModel;

    #endregion

    public class NotifyBase : INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">A name of the property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            var handler = this.PropertyChanged;

            if (handler != null)
            {
                var eventArgs = new PropertyChangedEventArgs(propertyName);

                handler(this, eventArgs);
            }
        }

        /// <summary>
        /// Warns the developer if this object does not have a public property
        /// with the specified name. This method does not exist in a Release build
        /// </summary>
        /// <param name="propertyName"></param>
        [System.Diagnostics.Conditional("DEBUG")]
        [System.Diagnostics.DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            //  Verify that the property matches a real public instance
            //  property on this object
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string message = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(message);
                else
                    System.Diagnostics.Debug.Fail(message);
            }

        }

        #endregion

        #region INotifyPropertyChanged.Events

        /// <summary>
        /// Raised when a property on this object gets a new value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
