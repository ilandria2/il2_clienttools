﻿namespace il2_clienttools.Helper
{
    #region Namespaces

    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.InteropServices;

    #endregion

    public static class AssemblyInfo
    {
        /// <summary>
        /// Gets the formatted Title + FileVersionInfo of the Calling assembly
        /// </summary>
        public static string GetVersionCalling()
        {
            var asm = Assembly.GetCallingAssembly();
            var ver = FileVersionInfo.GetVersionInfo(asm.Location);
            var title = GetTitle(asm);

            return string.Format("{0} v{1}.{2}.{3}", title, ver.FileMajorPart, ver.FileMinorPart, ver.FileBuildPart);
        }

        /// <summary>
        /// Gets the formatted Title + FileVersionInfo of the Executing assembly
        /// </summary>
        public static string GetVersionExecuting()
        {
            var asm = Assembly.GetExecutingAssembly();
            var ver = FileVersionInfo.GetVersionInfo(asm.Location);
            var title = GetTitle(asm);

            return string.Format("{0} v{1}.{2}.{3}", title, ver.FileMajorPart, ver.FileMinorPart, ver.FileBuildPart);
        }

        /// <summary>
        /// Gets the Product of the assembly
        /// </summary>
        public static string GetProduct()
        {
            var asmProduct = Assembly.GetCallingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false)[0] as AssemblyProductAttribute;
            return asmProduct.Product;
        }

        /// <summary>
        /// Gets the Title of the assembly
        /// </summary>
        /// <param name="asm"></param>
        public static string GetTitle(Assembly asm)
        {
            var asmTitle = asm.GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0] as AssemblyTitleAttribute;
            return asmTitle.Title;
        }

        /// <summary>
        /// Gets the GUID of the assembly
        /// </summary>
        public static string GetGuid()
        {
            var attribute = (GuidAttribute)Assembly.GetCallingAssembly().GetCustomAttributes(typeof(GuidAttribute), true)[0];
            return attribute.Value;
        }
    }
}
