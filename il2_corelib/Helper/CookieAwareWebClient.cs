﻿
namespace il2_corelib.Helper
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.Collections.Specialized;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.IO;
    using System.ComponentModel;
using il2_clienttools.EventHandlers;

    #endregion

    public class CookieAwareWebClient : WebClient
    {
        #region Fields
        
        private Uri _clientUri;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the WebClient's associated CookieContainer object instance
        /// </summary>
        public CookieContainer CookieContainer { get; set; }

        /// <summary>
        /// Gets the Uri associated to this WebClient-based object instance
        /// </summary>
        public Uri ClientUri
        {
            get { return this._clientUri; }
            private set { _clientUri = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new WebClient instance with a new CookieContainer instance
        /// </summary>
        public CookieAwareWebClient(Uri uri)
            : this(new CookieContainer(), uri, string.Empty)
        { }

        /// <summary>
        /// Initializes a new WebClient instance with a new CookieContainer instance using a specific content type header
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="contentType"></param>
        public CookieAwareWebClient(Uri uri, string contentType)
            : this(new CookieContainer(), uri, contentType)
        { }

        /// <summary>
        /// Initializes a new WebClient instance with an existing instance of the CookieContainer object
        /// </summary>
        /// <param name="cookieContainer"></param>
        public CookieAwareWebClient(CookieContainer cookieContainer, Uri uri, string contentType)
        {
            string type = "application/x-www-form-urlencoded";

            if (!string.IsNullOrWhiteSpace(contentType))
                type = contentType;

            CookieContainer = cookieContainer;
            ClientUri = uri;
            Headers.Add("Content-Type", type);
            Proxy = null;
            ServicePointManager.DefaultConnectionLimit = 10;
        }

        #endregion

        #region Methods.Overrides

        /// <summary>
        /// Overrides a base GetWebRequest method to also store the CookieContainer
        /// </summary>
        /// <param name="address"></param>
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).KeepAlive = false;
                (request as HttpWebRequest).CookieContainer = CookieContainer;
            }
            return request;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes UploadValuesAsync(ClientUri, "POST", data);
        /// </summary>
        /// <param name="data"></param>
        public bool PostMessageAsync(NameValueCollection data, object userState = null)
        {
            try
            {
                UploadValuesAsync(ClientUri, "POST", data, userState);
            }
            catch (WebException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes UploadDataAsync(ClientUri, "POST", <combined_message_with_rawData>);
        /// </summary>
        /// <param name="data"></param>
        public bool PostMessageAsync(NameValueCollection message, byte[] rawData, object userState = null)
        {
            try
            {
                var combined = Common.GetCombinedClientMessage(message, rawData);
                UploadDataAsync(ClientUri, "POST", combined, userState);
            }
            catch (WebException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Starts asynchronous file download from il2 ftp page and passes a new FileInfo instance as UserToken as identifier
        /// </summary>
        /// <param name="localFileName"></param>
        public bool DownloadFileStart(string localFileName, bool? isTest, object userState = null)
        {
            try
            {
                string M = string.Format("{0}.{1}", GetType().Name, "DownloadFileStart");
                Common.LogEntry(M, "Started");

                var localFile = new FileInfo(localFileName);
                Common.LogEntry(M, "Local file: {0}", localFile.FullName);

                var zipVersion = Common.ZipNeededForFile(localFile);
                Common.LogEntry(M, "ZipNeeded: {0}", zipVersion);

                var localFileZip = !zipVersion ? localFile : new FileInfo(localFile.FullName.Replace(localFile.Extension, ".zip"));
                Common.LogEntry(M, "localFileZip: {0}", localFileZip.FullName);

                Uri remoteFile = new Uri(Common.GetFileLink(localFile, isTest));
                Common.LogEntry(M, "remoteFileUri: {0}", remoteFile);

                if (localFileZip.Exists)
                {
                    Common.LogEntry(M, "Deleting existing file...");
                    localFileZip.Delete();
                    Common.LogEntry(M, "Deleted existing file...");
                }

                Common.LogEntry(M, "Calling DownloadFileAsync({0}, {1}, {2})", remoteFile, localFileZip.FullName, localFileZip);
                DownloadFileAsync(remoteFile, localFileZip.FullName, localFileZip);

                Common.LogEntry(M, "Finished");
            }
            catch (Exception ex)
            {
                if (ex is NotSupportedException)
                    throw;
                return false;
            }

            return true;
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the WebClient receives a response from the web request
        /// </summary>
        public event MessageResponseEventHandler MessageResponseReceived;

        /// <summary>
        /// Fires the MessageResponseReceived event on demand
        /// </summary>
        public void OnMessageResponseReceived(object sender, MessageResponseEventArgs eventArgs)
        {
            if (MessageResponseReceived != null)
                MessageResponseReceived(sender, eventArgs);
        }

        #endregion
    }
}
