﻿namespace il2_clienttools.Helper
{
    #region Namespaces

    using System;
    using System.Windows.Input;

    #endregion



    /// <summary>
    /// An ICommand-based class with delegates refered by other object instances
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields
        
        private Predicate<object> _canExecute;
        private Action<object> _execute;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new command that can execute always.
        /// </summary>
        /// <param name="execute">The execution logic (can not be null)</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }



        /// <summary>
        /// Creates a new command that can execute if canExecute returns true
        /// </summary>
        /// <param name="execute">The execution logic (can not be null)</param>
        /// <param name="canExecute">The execution prevention logic (null = always can execute)</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            this._canExecute = canExecute;
            this._execute = execute;
        }

        #endregion

        #region ICommand.Methods

        /// <summary>
        /// Allows/denies the execution of this command.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns>true when the command can execute, otherwise false.</returns>
        [System.Diagnostics.DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return this._canExecute == null ? true : this._canExecute(parameter);
        }



        /// <summary>
        /// Performs the action this command represents.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            this._execute(parameter);
        }

        #endregion

        #region ICommand.Events

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion
    }
}
