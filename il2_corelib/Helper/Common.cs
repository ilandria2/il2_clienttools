﻿namespace il2_clienttools.Helper
{
    #region Namespaces

    using il2_clienttools.Data;
    using il2_clienttools.EventHandlers;
    using il2_clienttools.Model;
    using il2_clienttools.ViewModel;
    using il2_corelib.Controls;
    using il2_corelib.Helper;
    using il2_corelib.Interfaces;
    using Microsoft.Win32;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Xml;

    #endregion

    /// <summary>
    /// Static common class
    /// </summary>
    public static class Common
    {
        #region Constructors

        /// <summary>
        /// Constructs the (static) Common object
        /// </summary>
        static Common()
        {
            ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

            AppArgs = Environment.GetCommandLineArgs();
            _iniFiles = new Dictionary<FileInfo,string[]>();
        }

        #endregion

        #region Constants

        public const string URL_GAME_CLIENT = @"https://client.ilandria.com/GameClient.php";
        public const string URL_SMF_CLIENT = @"https://forum.ilandria.com/SmfClient.php";
        public const string URL_SMF_RESET = @"http://forum.ilandria.com/index.php?action=reminder";
        public const string URL_SMF_REGISTER = @"http://forum.ilandria.com/index.php?action=register";
        public const string URL_FTP_FILES_ROOT = @"http://files.ilandria.com/content";
        public const string URL_FTP_FILES_TEST = @"http://files.ilandria.com/content/TEST";
        public const string URL_FTP_FILES_PUBLIC = @"http://files.ilandria.com/content/PUBLIC";
        
        public const string HOSTING_PUBLIC_KEY = "3082010A0282010100AD079C6970B8FD1FD3E23B317D1B7F464EEF49743770DCB44B3563A7CA6B6591A6589C131050922B4351884C55BE1424642F23F336D52E39825F70257DDA220D8CEBBA227946FE9477F8D6662945D6DEED031C0CC5099C00CE12FF7B864A876C6D486C69455AE74C5D97C407F9A6200C99EF553910035D800225264EC0643FEFA884E84072A98F6518270E7A7483F72C817DAA740B46BBDD641E9262719AB0827735E79446740D7AA23998128C5CBCFC0DFDE1D5127ECECC35F7C1631A8F38B5751EE6CB806E4DC9EF923BE3B3920831EDFCEA476FF5D922C061B4CA542EF2DF3D883D1968E7A7FCEF18AF054F821BB3519FBC8F210BBA3B03F7F29DA7DB21D90203010001";

        public const string ERROR_INVALID_BASEDIR = "Unable to determine location of the Neverwinter Nights game!";
        public const string ERROR_UNABLE_TO_SAVE_INI_VALUE = "Unable to save INI value!";
        public const string MESSAGE_NETWORK_ERROR = "Network-related error occured";

        public static readonly string CLIENT_TYPE_SMF = "SMF";
        public static readonly string CLIENT_TYPE_GAME = "GAME";

        public static readonly int MESSAGE_TYPE_CHECK_ACCOUNT = 0;
        public static readonly int MESSAGE_SUBTYPE_CHECK_ACCOUNT_NORMAL = 0;
        public static readonly int RESPONSE_TYPE_CHECK_ACCOUNT_ALREADY_EXISTS = 0;
        public static readonly int RESPONSE_TYPE_CHECK_ACCOUNT_CREATION_REQUEST_EXISTING = 1;
        public static readonly int RESPONSE_TYPE_CHECK_ACCOUNT_CREATION_REQUEST_NEW = 2;
        public static readonly int RESPONSE_TYPE_CHECK_ACCOUNT_ERROR = 9;
                               
        public static readonly int MESSAGE_TYPE_CREATE_CHARACTER = 1;
        public static readonly int RESPONSE_TYPE_CREATE_CHARACTER_SUCCESS = 0;
        public static readonly int RESPONSE_TYPE_CREATE_CHARACTER_ERROR = 9;

        public static readonly int PLAYER_ID_BASE = 100000;
        public static readonly int PLAYER_ID_NULL = -1;
        public static readonly int MESSAGE_TYPE_NEW_PLAYER_ID = 2;
        public static readonly int RESPONSE_TYPE_NEW_PLAYER_ID_ERROR = 9;
                               
        public static readonly int MESSAGE_TYPE_LOAD_CHARACTERS = 3;
        public static readonly int RESPONSE_TYPE_LOAD_CHARACTERS_ERROR = 9;
        public static readonly int RESPONSE_TYPE_LOAD_CHARACTERS_COLUMN_COUNT = 9;

        public static readonly int MESSAGE_TYPE_CHECK_MESSAGE = 4;
        public static readonly int RESPONSE_TYPE_CHECK_MESSAGE_ERROR = 9;
        public static readonly int MESSAGE_TYPE_CHECK_MESSAGE_ATTEMPTS = 4;
        public static readonly int MESSAGE_TYPE_CHECK_MESSAGE_SLEEP_SECONDS = 5;

        public static readonly int MESSAGE_TYPE_SAVE_MESSAGE = 5;
        public static readonly int RESPONSE_TYPE_SAVE_MESSAGE_ERROR = 9;

        public static readonly int MESSAGE_TYPE_PLAY_CHARACTER = 6;
        public static readonly int RESPONSE_TYPE_PLAY_MESSAGE_ERROR = 9;

        public static readonly bool ENVIRONMENT_TEST = true;
        public static readonly bool ENVIRONMENT_LIVE = false;

        public static readonly int FEAT_TARGET = 1600;
        public static readonly int FEAT_INTERACT = 1601;
        public static readonly int FEAT_RUN_MODE = 1625;

        #endregion

        #region Enums

        /// <summary>
        /// Common XML constants
        /// </summary>
        public enum xmlConst : int
        {
            Document_Standalone,
            Settings_IndentChars,
            Settings_Indent,
            Element_Root,
            Element_Header,
            Element_Header_Version,
            Element_Header_Date,
            Element_Header_Time,
            Element_Header_TimeZone,
            Element_FileItems,
            Element_FileItem,
            Element_FileItem_Path,
            Element_FileItem_AbsoluteAttribute,
            Element_FileItem_Checksum,
            Element_FileItem_MethodAttribute,
            Element_FileItem_IsSensitive,
            Element_FileItem_Size,
            Format_Date,
            Format_Time,
            Format_TimeZone,
            FileItemsVersion,
        }

        /// <summary>
        /// Registry base directory type (eg. Neverwitner Nights install folder)
        /// </summary>
        public enum RegistryBaseDirType : int
        {
            /// <summary>
            /// The "Neverwinter Nights" game base directory
            /// (default "C:/Neverwinter/NWN")
            /// </summary>
            BaseDir_NeverwinterNights,

            /// <summary>
            /// Unidentified directory
            /// </summary>
            BaseDir_Unknown,
        }

        #endregion

        #region Fields

        public static readonly string ABSOLUTE_ASSEMBLY_URI = "pack://application:,,,/{0};component/{1}{2}"; // {0} - il2_corelib, {1} Images/, {2} ilandria2.ico
        private static Dictionary<FileInfo, string[]> _iniFiles;
        private static string[] _appArgs;
        private static DirectoryInfo _baseDir_NWN;
        private static ViewModel_Window _mainWindow;
        private static object _logLock = new object();
        private static bool _debugLoggingEnabled;
        private static bool? _batchMode;
        private static bool _environmentType;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets whether the environment is TEST (true) or LIVE (false)
        /// </summary>
        public static bool EnvironmentType
        {
            get { return _environmentType; }
            set 
            {
                if (_environmentType == value)
                    return;

                _environmentType = value;
            }
        }

        /// <summary>
        /// Indicates whether the application is running in a batch mode (-b AppArgs switch used)
        /// </summary>
        public static bool BatchMode 
        {
            get
            {
                if (_batchMode == null)
                {
                    if (AppArgs.Contains("-b"))
                        _batchMode = true;
                    else
                        _batchMode = false;
                }

                return _batchMode.Value;
            }
        }

        /// <summary>
        /// Gets or sets whether the debug logging will be enabled
        /// </summary>
        public static bool DebugLoggingEnabled
        {
            get { return _debugLoggingEnabled; }
            set
            {
                if (value)
                {
                    if (Common.BaseDir_NWN == null)
                        throw new ApplicationException("BaseDir not initialized");

                    var logFilePath = string.Format(@"{0}\Debug.log", Common.BaseDir_NWN);
                    if (!Common.IsFileWriteable(logFilePath))
                        throw new ApplicationException(string.Format("Unable to obtain write access to log file path: {0}", logFilePath));

                    File.Delete(logFilePath);
                }

                _debugLoggingEnabled = value;
            }
        }

        /// <summary>
        /// Gets the base directory path for Neverwinter Nights
        /// Returns path from registry keys or working directory path if keys are not installed
        /// </summary>
        public static DirectoryInfo BaseDir_NWN
        {
            get
            {
                if (_baseDir_NWN == null)
                    _baseDir_NWN = Common.GetBaseDir(RegistryBaseDirType.BaseDir_NeverwinterNights);

                return _baseDir_NWN;
            }
        }

        /// <summary>
        /// Gets a list of INI files stored in memory
        /// </summary>
        public static Dictionary<FileInfo, string[]> IniFiles
        {
            get { return _iniFiles; }
        }

        /// <summary>
        /// Gets the application's command-line arguments
        /// </summary>
        public static string[] AppArgs
        {
            get { return _appArgs; }
            private set { _appArgs = value; }
        }

        /// <summary>
        /// Returns the value of the -f app arg switch
        /// </summary>
        public static string AppArgFile
        {
            get 
            {
                if (AppArgs.Length == 1)
                    return null;

                int indexArgFile = AppArgs.ToList().IndexOf("-f");
                if (indexArgFile == -1)
                    return null;

                // the "-f" switch was used but next arg missing
                if (AppArgs.Length == indexArgFile)
                    return null;

                return AppArgs[indexArgFile + 1];
            }
        }

        /// <summary>
        /// Represents the user that is currently logged in
        /// </summary>
        public static Model_User User 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets or sets the main window view model
        /// </summary>
        public static ViewModel_Window MainViewModel
        {
            get { return _mainWindow; }
            set { _mainWindow = value; }
        }
        
        #endregion

        #region Event handlers

        /// <summary>
        /// Validates certificate from the host
        /// </summary>
        private static bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None) return true;
            if (certificate.GetPublicKeyString() == HOSTING_PUBLIC_KEY) return true;

            return false;
        }

        #endregion

        #region Methods

        #region Methods.Assembly

        /// <summary>
        /// Gets the assembly directory
        /// </summary>
        /// <param name="asm"></param>
        public static string GetAssemblyDirectory(Assembly asm)
        {
            var uri = new UriBuilder(asm.CodeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var dir = Path.GetDirectoryName(path);

            return dir;
        }

        #endregion

        #region Methods.Website

        /// <summary>
        /// Opens url with user's default web browser
        /// </summary>
        /// <param name="url"></param>
        public static void OpenUrl(string url)
        {
            string browserPath = GetBrowserPath();
            
            if (browserPath == string.Empty)
                browserPath = "iexplore";

            Process process = new Process();
            process.StartInfo = new ProcessStartInfo(browserPath);
            process.StartInfo.Arguments = "\"" + url + "\"";
            process.Start();
        }

        /// <summary>
        /// Gets the registry path to the user's default web browser application.
        /// Default = Internet Explorer
        /// </summary>
        public static string GetBrowserPath()
        {
            string browser = string.Empty;
            RegistryKey key = null;

            try
            {
                // try location of default browser path in XP
                key = Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                // try location of default browser path in Vista
                if (key == null)
                {
                    key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http", false); ;
                }

                if (key != null)
                {
                    //trim off quotes
                    browser = key.GetValue(null).ToString().ToLower().Replace("\"", "");
                    if (!browser.EndsWith("exe"))
                    {
                        //get rid of everything after the ".exe"
                        browser = browser.Substring(0, browser.LastIndexOf(".exe") + 4);
                    }

                    key.Close();
                }
            }
            catch
            {
                return string.Empty;
            }

            return browser;
        }

        #endregion

        #region Methods.Client

        /// <summary>
        /// Creates a new NameValueCollection which consists of the user
        /// </summary>
        /// <param name="user"></param>
        public static NameValueCollection MessageSmfLogout(string user)
        {
            var credColl = new NameValueCollection();

            credColl.Add("user", user);

            return credColl;
        }

        /// <summary>
        /// Creates a new NameValueCollection which consists of user and password data
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pbox"></param>
        public static NameValueCollection MessageSmfLogin(string user, System.Windows.Controls.PasswordBox pbox)
        {
            var credColl = new NameValueCollection();

            credColl.Add("user", user);
            credColl.Add("password", pbox.Password);

            return credColl;
        }

        /// <summary>
        /// Creates a new NameValueCollection which consists of ClientMessage parameters
        /// </summary>
        /// <param name="user"></param>
        public static NameValueCollection MessageClient(string requestor, int requestorId, bool testEnv, int msgType, int msgSubType, int msgRawDataId, string msgParam)
        {
            string M = string.Format("{0}.{1}", typeof(Common).Name, "MessageClient");
            Common.LogEntry(M, "Start with requestor={0}, requestorId={1}, testEnv={2}, msgType={3}, msgSubType={4}, msgRawDataId={5}, msgParam={6}", requestor, requestorId, testEnv, msgType, msgSubType, msgRawDataId, msgParam);

            var dataColl = new NameValueCollection();

            dataColl.Add("msgRequestor", requestor);
            dataColl.Add("msgRequestorId", requestorId.ToString());
            dataColl.Add("msgEnv", testEnv.ToString());
            dataColl.Add("msgType", msgType.ToString());
            dataColl.Add("msgSubType", msgSubType.ToString());
            dataColl.Add("msgRawDataId", msgRawDataId.ToString());
            dataColl.Add("msgParameter", msgParam);

            Common.LogEntry(M, "Returning NameValueCollection({0})", dataColl);
            return dataColl;
        }

        /// <summary>
        /// Validates message and raw data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="rawData"></param>
        public static void ValidateClientMessageAndRawData(NameValueCollection message, byte[] rawData)
        {
            if (message == null)
                throw new ArgumentNullException("data");

            if (rawData == null)
                throw new ArgumentNullException("rawData");

            if (message.Count == 0)
                throw new InvalidOperationException("Empty message data");

            if (rawData.Length == 0)
                throw new InvalidOperationException("Empty raw data");
        }

        /// <summary>
        /// Constructs binary data using message and addittional raw data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="rawData"></param>
        /// <returns>combined client message with raw data</returns>
        public static byte[] GetCombinedClientMessage(NameValueCollection message, byte[] rawData)
        {
            ValidateClientMessageAndRawData(message, rawData);

            byte[] result = null;

            #region (combined message binary format)
            /* BINARY FORMAT:
             * <HEADER><MESSAGE><RAWDATA>
             * 
             * HEADER section:
             * 4Bytes - count of message keys
             * 4Bytes - start of Data section
             * 4Bytes - start of RawData section
             *
             * then for each key:
             * 
             * 4Bytes - count of key[n] bytes
             * 4Bytes - count of value[n] bytes
             */
            #endregion

            int sizeData = GetClientMessageSize(message);
            int startData = 3 * sizeof(int) + message.Count * 2 * sizeof(int);
            int startRawData = startData + sizeData;
            var sizes = new byte[message.Count * 2 * sizeof(int)];
            var data = new byte[sizeData];
            var sizeTemp = 0;

            for (int i = 0; i < message.Count; i++)
            {
                var key = message.GetKey(i);
                var value = message[i];

                BitConverter.GetBytes(key.Length).CopyTo(sizes, 2 * i * sizeof(int));
                BitConverter.GetBytes(value.Length).CopyTo(sizes, 2 * i * sizeof(int) + sizeof(int));

                var keyData = Encoding.UTF8.GetBytes(key);
                var valueData = Encoding.UTF8.GetBytes(value);

                keyData.CopyTo(data, sizeTemp);
                sizeTemp += keyData.Length;

                valueData.CopyTo(data, sizeTemp);
                sizeTemp += valueData.Length;
            }

            result = new byte[3 * sizeof(int) + sizes.Length + data.Length + rawData.Length];

            BitConverter.GetBytes(message.Count).CopyTo(result, 0 * sizeof(int));
            BitConverter.GetBytes(startData).CopyTo(result, 1 * sizeof(int));
            BitConverter.GetBytes(startRawData).CopyTo(result, 2 * sizeof(int));

            sizes.CopyTo(result, 3 * sizeof(int));
            data.CopyTo(result, startData);
            rawData.CopyTo(result, startRawData);

            return result;
        }

        /// <summary>
        /// Parses message and rawData out of an existing combined message
        /// </summary>
        /// <param name="combinedMessage"></param>
        /// <param name="message"></param>
        /// <param name="rawData"></param>
        public static bool ParseClientMessageResults(byte[] combinedMessage, out NameValueCollection message, out byte[] rawData)
        {
            message = null;
            rawData = null;

            try
            {
                var count = BitConverter.ToInt32(combinedMessage, 0 * sizeof(int));
                var startData = BitConverter.ToInt32(combinedMessage, 1 * sizeof(int));
                var startRawData = BitConverter.ToInt32(combinedMessage, 2 * sizeof(int));
                var sizeTemp = 0;

                rawData = new byte[combinedMessage.Length - startRawData];
                message = new NameValueCollection(count);

                Array.Copy(combinedMessage, startRawData, rawData, 0, combinedMessage.Length - startRawData);

                for (int i = 0; i < count; i++)
                {
                    var sizeK = BitConverter.ToInt32(combinedMessage, 3 * sizeof(int) + 2 * i * sizeof(int));
                    var sizeV = BitConverter.ToInt32(combinedMessage, 3 * sizeof(int) + 2 * i * sizeof(int) + sizeof(int));
                    var key = Encoding.ASCII.GetString(combinedMessage, sizeTemp + startData, sizeK);
                    var value = Encoding.ASCII.GetString(combinedMessage, sizeTemp + startData + sizeK, sizeV);

                    message.Add(key, value);
                    sizeTemp += sizeK + sizeV;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Counts amount of bytes needed to convert message data into binary format
        /// </summary>
        /// <param name="data"></param>
        public static int GetClientMessageSize(NameValueCollection data)
        {
            int size = 0;

            for (int i = 0; i < data.Count; i++)
                size += data.GetKey(i).Length + data[i].Length;

            return size;
        }

        /// <summary>
        /// Parses the results of the SmfApiMessageAsync procedure after it completed uploading values
        /// (when the OnUploadValuesCompleted event is handled)
        /// </summary>
        /// <remarks>
        /// Relies on the response (data parameter) being formatted as "..RESULT=xxx~yyy~zzz.."
        /// </remarks>
        /// <param name="data">UploadValuesCompletedEventArgs e.Result</param>
        /// <returns>array of string values - the "xxx", "yyy", "zzz" from remarks' example</returns>
        public static string[] ParseClientMessageResults(byte[] data)
        {
            string[] results = null;
            string dataText = string.Empty;
            try
            {
                dataText = Encoding.UTF8.GetString(data).Replace("<br />", "<br/>");
                var resultRow = dataText.Split(new[] { "<br/>" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(r => r.StartsWith("RESULT="));
                var resultData = resultRow.Split(new[] { "RESULT=" }, StringSplitOptions.RemoveEmptyEntries)[0];

                if (dataText.ToUpper().Contains("ERROR") || dataText.ToUpper().Contains("FAIL") || dataText.Contains("<b>Notice</b>:"))
                    throw new ApplicationException();

                results = resultData.Split(new[] { '~' }).ToArray();
            }
            catch 
            {
                if (Debugger.IsAttached)
                {
                    // review dataText variable above
                    Debugger.Break();
                }

                throw new ApplicationException("Error: Unable to parse game client data.");
            }

            return results;
        }

        /// <summary>
        /// Handles the asynchronous WebClient.xxCompletedEvents
        /// </summary>
        /// <param name="newClient"></param>
        /// <param name="sender"></param>
        /// <param name="cancelled"></param>
        /// <param name="result"></param>
        /// <param name="exception"></param>
        private static void HandleMessageResponseEvent(CookieAwareWebClient newClient, object sender, bool cancelled, byte[] result, Exception exception, object userState = null)
        {
            #region RESULTS ARRAY FORMAT
            /*
                        * [0]="GAME"   - client type (game, smf)
                        * [1]="VirgiL" - requestor
                        * [2]=100034   - requestor id
                        * [3]="1"      - testing environment? 1/0
                        * [4]="1"      - msgType
                        * [5]="0"      - msgSubType
                        * [6]="74"     - msgRawDataId
                        * [7]="Test"   - msgParameter
                        * [8]="1"      - message specific response
                        * [9]="ERROR TEXT" - error text
            */
            #endregion

            Exception ex = cancelled ? new OperationCanceledException() : exception;
            MessageResponseEventArgs eventArgs = null;

            if (ex == null)
            {
                string[] results = null;
                if (result != null)
                {
                    results = ParseClientMessageResults(result);

                    if (results != null)
                    {
                        if (results.Length != Data_MessageResponse.MESSAGE_COLUMN_COUNT)
                            throw new ApplicationException("Unexpected count of response fields (" + results.Length + ")");

                        string client = results[0];
                        string requestor = results[1];
                        int requestorId = int.Parse(results[2]);
                        bool isTest = results[3] == "1" ? true : false;
                        int msgType = int.Parse(results[4]);
                        int msgSubType = int.Parse(results[5]);
                        int msgRawDataId = int.Parse(results[6]);
                        string param = results[7];
                        string response = results[8];
                        string error = results[9];

                        if (!string.IsNullOrEmpty(error))
                            ex = new ApplicationException(error);

                        eventArgs = new MessageResponseEventArgs(requestor, requestorId, client, isTest, msgType, msgSubType, msgRawDataId, param, response, ex, userState);
                    }
                }
                else
                    eventArgs = new MessageResponseEventArgs(exception: null, userState: userState);
            }
            else
                eventArgs = new MessageResponseEventArgs(ex, userState);

            newClient.OnMessageResponseReceived(sender, eventArgs);
        }

        /// <summary>
        /// Executes UploadValuesAsync(ClientUri, "POST", data); on a new instance of the web client
        /// </summary>
        /// <param name="address"></param>
        /// <param name="data"></param>
        /// <param name="handlerCompleted">optional event handler</param>
        /// <param name="handlerProgress">optional event handler</param>
        public static bool PostMessageAsync(string address, NameValueCollection data, MessageResponseEventHandler handlerCompleted = null, UploadProgressChangedEventHandler handlerProgress = null, object userState = null)
        {
            string M = string.Format("{0}.{1}", typeof(Common).Name, "PostMessageAsync");
            Common.LogEntry(M, "Start with address={0}, data={1}, handlerCompleted={2}, handlerProgress={3}, userState={4}", address, data, handlerCompleted, handlerProgress, userState);

            var newClient = new CookieAwareWebClient(new Uri(address));

            if (handlerCompleted != null)
            {
                Common.LogEntry(M, "Registering handler completed");
                newClient.MessageResponseReceived += handlerCompleted;
                newClient.UploadValuesCompleted += (s, e) => { HandleMessageResponseEvent(newClient, s, e.Cancelled, e.Error != null ? null : e.Result, e.Error, userState); };
            }

            if (handlerProgress != null)
            {
                Common.LogEntry(M, "Registering handler progress");
                newClient.UploadProgressChanged += handlerProgress;
            }

            Common.LogEntry(M, "Registering UploadValuesCompleted to dispose internal web client instance");
            newClient.UploadValuesCompleted += (o, ea) =>
            {
                if (newClient != null)
                {
                    Common.LogEntry(M, "Disposing webclient instance");
                    newClient.Dispose();
                    newClient = null;
                }
            };

            Common.LogEntry(M, "Posting message async...");
            return newClient.PostMessageAsync(data, userState);
        }

        /// <summary>
        /// Executes UploadDataAsync(ClientUri, "POST", <combined_message_with_rawData>);
        /// </summary>
        /// <param name="address"></param>
        /// <param name="message"></param>
        /// <param name="rawData"></param>
        /// <param name="handlerCompleted">optional event handler</param>
        /// <param name="handlerProgress">optional event handler</param>
        public static bool PostMessageAsync(string address, NameValueCollection message, byte[] rawData, MessageResponseEventHandler handlerCompleted = null, UploadProgressChangedEventHandler handlerProgress = null, object userState = null)
        {
            string M = string.Format("{0}.{1}", typeof(Common).Name, "PostMessageAsync_withRawData");
            Common.LogEntry(M, "Start with address={0}, message={1}, rawData={2}, handlerCompleted={3}, handlerProgress={4}, userState={5}", address, message, rawData, handlerCompleted, handlerProgress, userState);

            var newClient = new CookieAwareWebClient(new Uri(address));

            if (handlerCompleted != null)
            {
                Common.LogEntry(M, "Registering handler completed");
                newClient.MessageResponseReceived += handlerCompleted;
                newClient.UploadDataCompleted += (s, e) => { HandleMessageResponseEvent(newClient, s, e.Cancelled, e.Result, e.Error, e.UserState); };
            }

            if (handlerProgress != null)
            {
                Common.LogEntry(M, "Registering handler progress");
                newClient.UploadProgressChanged += handlerProgress;
            }

            Common.LogEntry(M, "Registering UploadDataCompleted to dispose internal web client instance");
            newClient.UploadDataCompleted += (o, ea) =>
            {
                if (newClient != null)
                {
                    Common.LogEntry(M, "Disposing webclient instance");
                    newClient.Dispose();
                    newClient = null;
                }
            };

            Common.LogEntry(M, "Posting message with raw data async...");
            return newClient.PostMessageAsync(message, rawData, userState);
        }

        /// <summary>
        /// Starts asynchronous file download from il2 ftp page and passes a new FileInfo instance as UserToken as identifier using a new WebClient instance
        /// </summary>
        /// <param name="localFileName"></param>
        /// <param name="handlerCompleted">optional handler</param>
        /// <param name="hnadlerProgress">optional handler</param>
        public static bool DownloadFileAsync(string localFileName, bool? isTest, MessageResponseEventHandler handlerCompleted = null, DownloadProgressChangedEventHandler handlerProgress = null, object userState = null)
        {
            var newClient = new CookieAwareWebClient(null);

            if (handlerCompleted != null)
            {
                newClient.MessageResponseReceived += handlerCompleted;
                newClient.DownloadFileCompleted += (s, e) => { HandleMessageResponseEvent(newClient, s, e.Cancelled, null, e.Error, userState); };
            }

            if (handlerProgress != null)
                newClient.DownloadProgressChanged += handlerProgress;

            newClient.DownloadFileCompleted += (o, ea) =>
            {
                if (newClient != null)
                {
                    newClient.Dispose();
                    newClient = null;
                }
            };

            return newClient.DownloadFileStart(localFileName, isTest, userState);
        }

        #endregion

        #region Methods.Windows

        /// <summary>
        /// Initializes a View and binds it to a ViewModel.
        /// </summary>
        /// <param name="typeView"></param>
        /// <param name="typeViewModel"></param>
        /// <param name="minimizeLevel"></param>
        /// <param name="switchAppMainWindow"></param>
        /// <param name="showView"></param>
        /// <param name="args"></param>
        public static void InitView(Type typeView, Type typeViewModel, 
            int minimizeLevel, 
            bool switchAppMainWindow, 
            bool showView, 
            params object[] args)
        {
            //  every valid view must inherit from FrameworkElement to have DataContext property
            if (!typeView.IsSubclassOf(typeof(FrameworkElement))) return;

            //  temporarily set application Shutdown mode to explicit due to possible windows shown before the main window
            if (switchAppMainWindow && typeView.IsSubclassOf(typeof(Window)))
                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            //  create View
            object view = Activator.CreateInstance(typeView);
            
            //  create ViewModel (based on amount of parameters passed)
            object viewModel = null;

            #region Create instance based on amount of parameters passed

            if (minimizeLevel == 0)
            {
                switch (args.Length)
                {
                    case 0:
                    viewModel = Activator.CreateInstance(typeViewModel);
                        break;

                    case 1:
                    viewModel = Activator.CreateInstance(typeViewModel, args[0]);
                        break;

                    case 2:
                    viewModel = Activator.CreateInstance(typeViewModel, args[0], args[1]);
                        break;

                    default:
                    viewModel = Activator.CreateInstance(typeViewModel, args[0], args[1], args[2]);
                        break;
                }
            }
            else
            {
                switch (args.Length)
                {
                    case 0:
                        viewModel = Activator.CreateInstance(typeViewModel, minimizeLevel == 1 ? (ICloseable)view : (IMinimizeable)view);
                        break;

                    case 1:
                        viewModel = Activator.CreateInstance(typeViewModel, minimizeLevel == 1 ? (ICloseable)view : (IMinimizeable)view, args[0]);
                        break;

                    case 2:
                        viewModel = Activator.CreateInstance(typeViewModel, minimizeLevel == 1 ? (ICloseable)view : (IMinimizeable)view, args[0], args[1]);
                        break;                                            
                                                                          
                    default:                                              
                        viewModel = Activator.CreateInstance(typeViewModel, minimizeLevel == 1 ? (ICloseable)view : (IMinimizeable)view, args[0], args[1], args[2]);
                        break;
                }
            }

            #endregion

            //  bind DataContext
            ((FrameworkElement)view).DataContext = viewModel;

            //  set application Shutdown mode back
            if (switchAppMainWindow && view is Window)
            {
                Application.Current.MainWindow = (Window)view;
                Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
                Common.MainViewModel = viewModel as ViewModel_Window;
            }

            //  show view
            if (showView && view is Window)
                ((Window)view).Show();
        }

        /// <summary>
        /// Performs the <paramref name="action"/> on the UI thread if not currently in it.
        /// </summary>
        /// <param name="action"/>
        public static void DoActionOnUi(Action action)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(action);
                return;
            }

            action();
        }

        #endregion

        #region Methods.Registry

        /// <summary>
        /// Sets registry key value (creates subkeys if not exists)
        /// </summary>
        /// <param name="regHive"></param>
        /// <param name="sSubKey"></param>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public static void SetRegistryKey(RegistryHive regHive, string sSubKey, string sKey, string sValue)
        {
            RegistryKey rkBaseKey = RegistryKey.OpenBaseKey(regHive, RegistryView.Default);
            RegistryKey rkTemp = null;

            sSubKey = sSubKey.Replace('/', '\\');

            var subkeys = sSubKey.Split(new char[] { '\\' });

            rkTemp = rkBaseKey;

            foreach (var item in subkeys)
                rkTemp =
                    rkTemp.OpenSubKey(item, true) ??
                    rkTemp.CreateSubKey(item, RegistryKeyPermissionCheck.ReadWriteSubTree);

            rkTemp.SetValue(sKey, sValue, RegistryValueKind.String);
        }

        /// <summary>
        /// Reads registry key value
        /// </summary>
        /// <param name="regHive"></param>
        /// <param name="sSubKey"></param>
        /// <param name="sKey"></param>
        public static string ReadRegistryKey(RegistryHive regHive, string sSubKey, string sKey)
        {
            sSubKey = sSubKey.Replace('/', '\\');

            RegistryKey rkBaseKey = RegistryKey.OpenBaseKey(regHive, RegistryView.Default);
            RegistryKey rkSubKey = rkBaseKey.OpenSubKey(sSubKey);

            if (rkSubKey == null)
                return null;

            try
            { return (string)rkSubKey.GetValue(sKey); }
            catch
            { }

            return null;
        }

        #endregion

        #region Methods.FileSystem

        /// <summary>
        /// Determines whether a filePath is writeable
        /// </summary>
        /// <param name="filePath"></param>
        public static bool IsFileWriteable(string filePath)
        {
            try
            {
                using (var fs = File.OpenWrite(filePath))
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Reads all bytes of a binary reader
        /// </summary>
        /// <param name="reader"></param>
        public static byte[] ReadAllBytes(this BinaryReader reader)
        {
            const int bufferSize = 4096;
            using (var ms = new MemoryStream())
            {
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                    ms.Write(buffer, 0, count);
                return ms.ToArray();
            }

        }

        /// <summary>
        /// Gets the working directory path (string value) of the calling assembly
        /// </summary>
        public static string GetWorkingDirectory()
        {
            return GetAssemblyDirectory(Assembly.GetCallingAssembly());
        }

        /// <summary>
        /// Compares directory names (case-insensitive) and returns true if they equal
        /// </summary>
        /// <param name="dir1"></param>
        /// <param name="dir2"></param>
        public static bool CompareDirectoryName(string dir1, string dir2)
        {
            return dir1.ToUpper().TrimEnd('/', '\\') == dir2.ToUpper().TrimEnd('/', '\\');
        }

        /// <summary>
        /// Compares two file case-insensitive names and returns true if they equal
        /// </summary>
        /// <param name="file1"></param>
        /// <param name="file2"></param>
        public static bool CompareFileName(string file1, string file2)
        {
            return file1.ToUpper() == file2.ToUpper();
        }

        /// <summary>
        /// Formats the amount of bytes (lSize) to a formatted text equivalent
        /// </summary>
        public static string GetFileSizeFormat(long lSize)
        {
            return GetFileSizeFormat(lSize, false, false);
        }

        /// <summary>
        /// Formats the amount of bytes (lSize) to a formatted text equivalent
        /// </summary>
        public static string GetFileSizeFormat(long lSize, bool perSecond, bool unitAfter)
        {
            double size = lSize;
            int index = 0;
            for (; size > 1024; index++)
                size /= 1024;

            var unit = new[] { "B ", "KB", "MB", "GB" }[index > 3 ? 3 : index] + (perSecond ? "/s" : string.Empty);

            if (!unitAfter)
                return size.ToString(unit + " 0.0");
            else
                return size.ToString("0.0 " + unit);
        }

        /// <summary>
        /// Formats the amount of seconds to a user friendly version
        /// </summary>
        /// <param name="seconds"></param>
        public static string GetTimeFormat(long seconds)
        {
            var sb = new StringBuilder();
            var secs = seconds;
            var hours = secs / 3600;
            secs -= hours * 3600;

            var minutes = secs / 60;
            secs -= minutes * 60;

            if (hours > 0)
                sb.AppendFormat("{0}h", hours);

            if (minutes > 0)
                sb.AppendFormat(" {0}m", minutes);

            sb.AppendFormat(" {0}s", secs);

            return sb.ToString();
        }

        /// <summary>
        /// Each baseDirType refers to one registry key value which holds the path of the directory
        /// </summary>
        /// <param name="baseDirType"></param>
        /// <returns>DirectoryInfo object refering to the base directory of the baseDirType specified</returns>
        public static DirectoryInfo GetBaseDir(RegistryBaseDirType baseDirType)
        {
            string sSubKey;
            string sKey;
            string sBasePath;

            //  determine the registry subkey path and key name based on the baseDirType specified
            switch (baseDirType)
            {
                case RegistryBaseDirType.BaseDir_NeverwinterNights:

                    if (Environment.Is64BitOperatingSystem)
                        sSubKey = @"Software\Wow6432Node\BioWare\NWN\Neverwinter";
                    else
                        sSubKey = @"Software\BioWare\NWN\Neverwinter";

                    sKey = "Location";
                    break;

                case RegistryBaseDirType.BaseDir_Unknown:
                    sSubKey = @"";
                    sKey = @"";
                    break;

                default:
                    sSubKey = @"";
                    sKey = @"";
                    break;
            }

            sBasePath = ReadRegistryKey(RegistryHive.LocalMachine, sSubKey, sKey);

            //  registry key is not installed
            if (sBasePath == null)
            {
                // for NWN try to search for game client in the working directory
                if (baseDirType != RegistryBaseDirType.BaseDir_NeverwinterNights)
                {
                    //  check working directory
                    if (File.Exists(GetWorkingDirectory() + @"\nwmain.exe"))
                        return new DirectoryInfo(GetWorkingDirectory());
                }

                // registry key not found
                else
                    return null;
            }

            return new DirectoryInfo(sBasePath);
        }

        /// <summary>
        /// Determines whether the fileInfo is location somewhere within the baseDir
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="baseDir"></param>
        /// <returns>true or false</returns>
        public static bool IsPathRelative(FileInfo fileInfo, string baseDir)
        {
            return baseDir == null ? false : Path.GetDirectoryName(fileInfo.FullName).StartsWith(baseDir);
        }

        /// <summary>
        /// Determines whether the fileInfo is location somewhere within the baseDir
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="baseDirType"></param>
        /// <returns>true or false</returns>
        public static bool IsPathRelative(FileInfo fileInfo, RegistryBaseDirType baseDirType)
        {
            var baseDir = Common.GetBaseDir(baseDirType);

            if (baseDir == null)
                return false;
            return IsPathRelative(fileInfo, baseDir.FullName);
        }

        /// <summary>
        /// Gets the baseDirType relative (or base) path of the fileInfo object
        /// </summary>
        public static string GetRelativePath(FileInfo fileInfo, string baseDir)
        { 
            return (!Common.IsPathRelative(fileInfo, baseDir) ?
                fileInfo.FullName: @"{NWN_DIR}\" +
                fileInfo.FullName.Substring(baseDir.Length))
                .Replace("\\\\", "\\");
        }

        /// <summary>
        /// Gets the baseDirType relative (or base) path of the fileInfo object
        /// </summary>
        public static string GetRelativePath(FileInfo fileInfo, RegistryBaseDirType baseDirType)
        {
            var baseDir = Common.GetBaseDir(baseDirType);

            if (baseDir == null)
                return null;
            return GetRelativePath(fileInfo, baseDir.FullName);
        }

        /// <summary>
        /// Resets file size on list of file items
        /// </summary>
        /// <param name="fileItems"></param>
        public static void RefreshSizesForFileItems(ObservableCollection<Model_FileItem> fileItems)
        {
            foreach (var fileItem in fileItems)
            {
                fileItem.FileSize = 0;
                fileItem.FileSize = fileItem.FileSize;
            }
        }

        /// <summary>
        /// ~static method
        /// Calculates MD5 checksum using buffered stream reading and cancellation token
        /// </summary>
        /// <param name="fileItem"></param>
        /// <param name="cancelToken"></param>
        public static void ComputeChecksumWithUpdate(Model_FileItem fileItem, CancellationToken cancelToken)
        {
            if (cancelToken.IsCancellationRequested) return;
            string M = string.Format("{0}.{1}", typeof(Common).Name, "ComputeChecksumWithUpdate()");
            Common.LogEntry(M, "Started with parameters {0}, {1}", fileItem, cancelToken);

            //  size of the buffer
            const int _nBufferSize = 4096;

            int nBytesRead = 0;
            decimal mStatus = 0M;
            int nChunk = 0, nChunks = 0;
            byte[] bBuffer = new byte[_nBufferSize];
            byte[] bHash = null;

            using (FileStream fs = new FileStream(fileItem.FileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                nChunks = (int)(fs.Length / _nBufferSize);
                nChunks += (fs.Length % _nBufferSize > 0) ? 1 : 0;

                Common.LogEntry(M, "Starting to compute checsum of {0} file chunks", nChunks);

                //  create MD5 hasher instance
                using (HashAlgorithm objHasher = new MD5CryptoServiceProvider())
                {
                    //  loop until EOF
                    while ((nBytesRead = fs.Read(bBuffer, 0, _nBufferSize)) > 0)
                    {
                        //  calculate MD5 hash of the buffer
                        objHasher.TransformBlock(bBuffer, 0, nBytesRead, bBuffer, 0);

                        //  task cancelled
                        if (cancelToken.IsCancellationRequested)
                        {
                            Common.LogEntry(M, "Cancellation requested - return");
                            return;
                        }

                        nChunk += 1;

                        //  update the FileItem object's Status property
                        mStatus = (decimal)(((float)nChunk) / nChunks * 100);
                        mStatus = mStatus > 100 ? 100 : mStatus;

                        fileItem.StatusSizeVerified += nBytesRead;
                        fileItem.Status = mStatus;
                        //Common.LogEntry(M, "Item {0} Chunk {1} Status: {2}, item verified: {3}", fileItem.FileInfo.Name, nChunk, mStatus, fileItem.StatusSizeVerified);
                    }

                    //  build new byte[] array representing the hash bytes of the file
                    Common.LogEntry(M, "Transform final block...");
                    objHasher.TransformFinalBlock(new byte[0], 0, 0);
                    bHash = objHasher.Hash;
                    Common.LogEntry(M, "Hash: {0}", bHash);
                }
            }

            //  convert the bytes[] array into string
            StringBuilder sbHash = new StringBuilder(16);

            foreach (byte byt in bHash)
                sbHash.Append(byt.ToString("X2"));

            //  update the FileItem object
            Data_Checksum Checksum = new Data_Checksum();

            Checksum.Method = ChecksumMethod.MD5;
            Checksum.Value = sbHash.ToString();

            fileItem.Checksum = Checksum;
            fileItem.Status = 100.0M;
            Common.LogEntry(M, "Item {0} hash {1} status 100%", fileItem, Checksum.Value);
        }

        /// <summary>
        /// Decompresses a file
        /// </summary>
        /// <param name="fileInfo"/>
        /// <param name="expectedFileSize"/>
        /// <param name="onSizeDifference"/>
        /// <param name="onChunkRead"/>
        /// <param name="cancelToken"/>
        public static bool DecompressFileWithUpdate(FileInfo fileInfo, long expectedFileSize, Action<object> onSizeDifference, Action<object> onChunkRead, CancellationToken cancelToken)
        {
            string M = string.Format("{0}.{1}", typeof(Common).Name, "DecompressFileWithUpdate");
            Common.LogEntry(M, "Start with file {0}", fileInfo);

            try
            {
                if (fileInfo.Extension.ToUpper() != ".ZIP")
                {
                    fileInfo = new FileInfo(fileInfo.FullName.Replace(fileInfo.Extension, ".zip"));
                    Common.LogEntry(M, "File extension is not zip - converted to {0}", fileInfo);
                }

                if (!fileInfo.Exists)
                {
                    Common.LogEntry(M, "File not found!");
                    throw new ApplicationException(string.Format("File {0} not found!", fileInfo.Name));
                }

                Common.LogEntry(M, "Opening zip file...");
                using (var zipArchive = ZipFile.OpenRead(fileInfo.FullName))
                {
                    if (zipArchive.Entries.Count == 0)
                    {
                        Common.LogEntry(M, "No zip entries found");
                        return false;
                    }

                    Common.LogEntry(M, "Looping through zip archive entries...");
                    foreach (var zipItem in zipArchive.Entries)
                    {
                        // compare received file's decompressed size against expected file size
                        if (onSizeDifference != null)
                            if (zipItem.Length != expectedFileSize)
                                    onSizeDifference(zipItem.Length);

                        var tempFileName = Path.Combine(fileInfo.DirectoryName, zipItem.Name + ".new");
                        var outFileName = Path.Combine(fileInfo.DirectoryName, zipItem.Name);

                        Common.LogEntry(M, "tempFileName: {0}", tempFileName);
                        Common.LogEntry(M, "outFileName: {0}", outFileName);
                        Common.LogEntry(M, "Opening zip item and binary writer...");

                        using (var zipStream = zipItem.Open())
                        using (var binReader = new BinaryReader(zipStream))
                        using (var fileStream = new FileStream(tempFileName, FileMode.Create, FileAccess.Write))
                        using (var binWriter = new BinaryWriter(fileStream))
                        {
                            const int _nBufferSize = 4096;
                            int nBytesRead = 0;
                            int nChunk = 0, nChunks = 0;
                            byte[] bBuffer = new byte[_nBufferSize];

                            nChunks = (int)(zipItem.Length / _nBufferSize);
                            nChunks += (zipItem.Length % _nBufferSize > 0) ? 1 : 0;

                            Common.LogEntry(M, "Chunks to be read: {0}", nChunks);

                            //  loop until EOF
                            while ((nBytesRead = zipStream.Read(bBuffer, 0, bBuffer.Length)) > 0)
                            {
                                //  task cancelled
                                if (cancelToken.IsCancellationRequested)
                                {
                                    Common.LogEntry(M, "cancelled");
                                    return false;
                                }

                                nChunk += 1;
                                if (nChunk == nChunks)
                                {
                                    Common.LogEntry(M, "last chunk was read - resizing buffer to: {0} bytes", nBytesRead);
                                    Array.Resize(ref bBuffer, nBytesRead);
                                }

                                binWriter.Write(bBuffer);

                                //  update the FileItem object's Status property
                                if (onChunkRead != null)
                                    onChunkRead((long)nBytesRead);
                            }

                            Common.LogEntry(M, "end of loop");
                        }

                        if (File.Exists(outFileName))
                        {
                            Common.LogEntry(M, "Deleting file: {0}...", outFileName);
                            File.Delete(outFileName);
                        }

                        Common.LogEntry(M, "Copying {0} to {1}", tempFileName, outFileName);
                        File.Copy(tempFileName, outFileName);

                        Common.LogEntry(M, "Deleting {0}...", tempFileName);
                        File.Delete(tempFileName);
                    }
                }

                Common.LogEntry(M, "Deleting file {0}", fileInfo);
                File.Delete(fileInfo.FullName);
            }
            catch (Exception ex)
            {
                Common.LogEntry(M, "Error: {0}-{1}", ex.GetType().FullName, ex.Message);
                return false;
            }

            Common.LogEntry(M, "Finished");
            return true;
        }

        /// <summary>
        /// Compresses the fileSource at fileDest with overwrite
        /// </summary>
        /// <param name="fileSource"></param>
        /// <param name="fileDest"></param>
        public static void CompressFile(string fileSource, string fileDest)
        {
            using (var stmFile = new FileStream(fileDest, FileMode.Create))
            using (var stmZip = new ZipArchive(stmFile, ZipArchiveMode.Create))
            {
                var zipEntry = stmZip.CreateEntryFromFile(fileSource, Path.GetFileName(fileSource));
            }
        }

        /// <summary>
        /// Compresses dirSource directory to fileDest package
        /// </summary>
        /// <param name="dirSource"></param>
        /// <param name="fileDest"></param>
        public static void CompressDirectory(string dirSource, string fileDest)
        {
            ZipFile.CreateFromDirectory(dirSource, fileDest);
        }

        /// <summary>
        /// Decompresses compressedFile into dirDest
        /// </summary>
        /// <param name="dirDest"></param>
        /// <param name="compressedFile"></param>
        public static void DecompressFile(string dirDest, string compressedFile)
        {
            ZipFile.ExtractToDirectory(compressedFile, dirDest);
        }

        /// <summary>
        /// Determines whether a zip file version of localFile is needed when going to download it
        /// </summary>
        /// <param name="localFile"></param>
        public static bool ZipNeededForFile(FileInfo localFile)
        {
            var localExt = localFile.Extension.ToUpper();

            return !(localExt == ".XML" || localExt == ".ZIP");
        }

        /// <summary>
        /// Generates ZIP version of for direct download link of a local file
        /// </summary>
        /// <param name="localFile"></param>
        public static string GetFileLink(FileInfo localFile, bool? isTest, bool zipVersion = true)
        {
            if (!ZipNeededForFile(localFile))
                return string.Format(GetFileLinkRoot(localFile, isTest) + "/" + localFile.Name);
            else
                return string.Format(GetFileLinkRoot(localFile, isTest) + "/" + localFile.Name.Replace(localFile.Extension, ".zip"));
        }

        /// <summary>
        /// Gets the root url link based on the environment type specified
        /// </summary>
        /// <param name="isTest"></param>
        private static string GetFileLinkRoot(FileInfo localFile, bool? isTest)
        {
            if (isTest == null || localFile.Extension.ToLower() == ".xml")
                return Common.URL_FTP_FILES_ROOT;
            else if (isTest.Value)
                return Common.URL_FTP_FILES_TEST;
            else if (!isTest.Value)
                return Common.URL_FTP_FILES_PUBLIC;

            throw new ApplicationException("Unable to determine file link root location");
        }

        /// <summary>
        /// Gets the file size of a remote file or 0 on error
        /// </summary>
        /// <param name="localFile"></param>
        /// <returns></returns>
        public static long GetRemoteFileSize(FileInfo localFile, bool? isTest)
        {
            var link = GetFileLink(localFile, isTest, zipVersion: true);
            var size = GetRemoteFileSize(link);

            if (size > 0)
                return size;

            else
            {
                link = GetFileLink(localFile, isTest, zipVersion: false);
                size = GetRemoteFileSize(link);

                return size;
            }
        }

        /// <summary>
        /// Gets the file size of a remote file or 0 on error
        /// </summary>
        /// <param name="link"></param>
        public static long GetRemoteFileSize(string link)
        {
          long length = 0;

          try
          {
            WebRequest req = HttpWebRequest.Create(link);
            req.Method = "HEAD";

            using (System.Net.WebResponse resp = req.GetResponse())
              long.TryParse(resp.Headers.Get("Content-Length"), out length);
          }
          catch { }

          return length;
        }

        /// <summary>
        /// Shows the Open file dialog window and returns the chosen path
        /// </summary>
        /// <param name="sTitle"></param>
        /// <param name="BaseDir"></param>
        /// <returns>FileInfo of the File to open</returns>
        public static DirectoryInfo GetOpenDirectoryPath(string sTitle, FileInfo BaseDir)
        {

            System.Windows.Forms.FolderBrowserDialog dlgOpenDir = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult dlgResult;
            BaseDir = BaseDir ?? new FileInfo(Environment.CurrentDirectory);

            dlgOpenDir.Description = sTitle;
            dlgOpenDir.SelectedPath = BaseDir.FullName;

            dlgResult = dlgOpenDir.ShowDialog();

            if (dlgResult == System.Windows.Forms.DialogResult.OK)
                return new DirectoryInfo(dlgOpenDir.SelectedPath);

            return null;
        }

        /// <summary>
        /// Shows the Open file dialog window and returns the chosen path
        /// </summary>
        /// <param name="sTitle"></param>
        /// <param name="BaseDir"></param>
        /// <returns>FileInfo of the File to open</returns>
        public static FileInfo GetOpenFilePath(string sTitle, string sFilter, FileInfo BaseDir)
        {
            OpenFileDialog dlgOpenFile = new OpenFileDialog();
            bool? dlgResult = null;
            BaseDir = BaseDir ?? new FileInfo(Environment.CurrentDirectory);

            dlgOpenFile.Filter = sFilter;
            dlgOpenFile.Title = sTitle;
            dlgOpenFile.InitialDirectory = BaseDir.FullName;
            dlgOpenFile.CheckFileExists = false;
            dlgOpenFile.Multiselect = false;

            dlgResult = dlgOpenFile.ShowDialog();

            if (dlgResult == true)
                return new FileInfo(dlgOpenFile.FileName);

            return null;
        }

        /// <summary>
        /// Shows the Open file dialog window and returns the chosen path
        /// </summary>
        /// <param name="sTitle"></param>
        /// <param name="BaseDir"></param>
        /// <returns>FileInfo of the File to open</returns>
        public static List<FileInfo> GetOpenFilePathMultiple(string sTitle, string sFilter, FileInfo BaseDir)
        {
            OpenFileDialog dlgOpenFile = new OpenFileDialog();
            bool? dlgResult = null;
            BaseDir = BaseDir ?? new FileInfo(Environment.CurrentDirectory);

            dlgOpenFile.Filter = sFilter;
            dlgOpenFile.Title = sTitle;
            dlgOpenFile.InitialDirectory = BaseDir.FullName;
            dlgOpenFile.CheckFileExists = true;
            dlgOpenFile.Multiselect = true;

            dlgResult = dlgOpenFile.ShowDialog();

            if (dlgResult == true)
            {
                string[] sFiles = dlgOpenFile.FileNames;
                List<FileInfo> lstResult = new List<FileInfo>(sFiles.Length);

                foreach (string sFile in sFiles)
                {
                    lstResult.Add(new FileInfo(sFile));
                }

                return lstResult;
            }

            return null;
        }

        /// <summary>
        /// Shows the Save file dialog window and returns the chosen path
        /// </summary>
        /// <param name="sTitle"></param>
        /// <param name="BaseDir"></param>
        /// <returns>FileInfo of the File to save</returns>
        public static FileInfo GetSaveFilePath(string sTitle, string sFilter, FileInfo BaseDir, bool bOverwritePrompt)
        {
            SaveFileDialog dlgSaveFile = new SaveFileDialog();
            bool? dlgResult = null;
            BaseDir = BaseDir ?? new FileInfo(Environment.CurrentDirectory);

            dlgSaveFile.Filter = sFilter;
            dlgSaveFile.Title = sTitle;
            dlgSaveFile.InitialDirectory = BaseDir.FullName;
            dlgSaveFile.OverwritePrompt = bOverwritePrompt;

            dlgResult = dlgSaveFile.ShowDialog();

            if (dlgResult == true)
                return new FileInfo(dlgSaveFile.FileName);

            return null;
        }

        #endregion

        #region Methods.Game

        /// <summary>
        /// Reads a value of a specific key from an ini file
        /// </summary>
        public static string ReadIniValue(FileInfo file, string key)
        {
            if (!CheckIniFile(file)) return null;
            if (string.IsNullOrWhiteSpace(key)) return null;

            string line = IniFiles[file].FirstOrDefault(l => l.StartsWith(string.Format("{0}=", key)));

            if (!string.IsNullOrWhiteSpace(line))
                return line.Split('=')[1];

            return string.Empty;
        }

        /// <summary>
        /// Stores a value of a specific key to an ini file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns>false on error, true on success</returns>
        public static bool StoreIniValue(FileInfo file, string key, string value)
        {
            if (!CheckIniFile(file)) return false;
            if (string.IsNullOrWhiteSpace(key)) return false;

            int pos = Array.FindIndex<string>(IniFiles[file], line => line.StartsWith(string.Format("{0}=", key)));

            if (pos == -1)
                return false;

            IniFiles[file][pos] = string.Format("{0}={1}", key, value);

            File.WriteAllLines(file.FullName, IniFiles[file]);
            return true;
        }

        /// <summary>
        /// Loads all lines of a file and adds it to the IniFiles dictionary
        /// </summary>
        /// <param name="file"></param>
        private static void LoadIniFile(FileInfo file)
        {
            string[] lines = File.ReadAllLines(file.FullName);

            IniFiles.Add(file, lines);
        }

        /// <summary>
        /// Returns true if the file parameter is an .ini file with max size 3mb
        /// </summary>
        /// <param name="file"></param>
        public static bool CheckIniFile(FileInfo file)
        {
            //  file not exists
            if (!file.Exists) return false;

            //  file extension invalid
            if (file.Extension.ToUpper() != ".INI") return false;

            //  file size too big
            if (file.Length > 1024 * 1024 * 3) return false;

            //  file size too small
            if (file.Length == 0) return false;

            if (!IniFiles.ContainsKey(file))
                LoadIniFile(file);

            return true;
        }

        #endregion

        #region Methods.Processes

        /// <summary>
        /// Starts the process (file name from the working directory)
        /// or downloads it if not exists and then shuts down the application
        /// </summary>
        /// <param name="appFileName"></param>
        public static void QuitAndStartProcess(string appFileName)
        {
            if (!File.Exists(appFileName))
                using (var client = new WebClient())
                    client.DownloadFile(GetFileLink(new FileInfo(appFileName), false), appFileName);

            Process.Start(appFileName);

            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            Application.Current.Shutdown(0);
        }

        /// <summary>
        /// Executes the Action method asynchronously but keeps only MaxThreadCount threads running at the same time
        /// Others are waiting in the queue
        /// </summary>
        public static async Task RunTasksAsync(List<Action> actions, int MaxThreadCount, CancellationToken CancelToken)
        {
            int nSkip = 0, nTake = 0;

            var taskSeries = new List<Task>(MaxThreadCount);

            if (MaxThreadCount < 1)
                MaxThreadCount = 1;

            nTake = MaxThreadCount > actions.Count ? actions.Count : MaxThreadCount;

            do
            {
                for (int i = 0; i < nTake; i++)
                {
                    var action = actions[nSkip + i];
                    taskSeries.Add(Task.Run(action));
                    //await taskSeries[taskSeries.Count - 1].ConfigureAwait(false);
                }

                var taskFinishedFirst = await Task.WhenAny(taskSeries).ConfigureAwait(false);

                if (taskFinishedFirst.Status == TaskStatus.RanToCompletion)
                    taskSeries.Remove(taskFinishedFirst);

                await taskFinishedFirst;

                //  cancel breaks the loop
                if (CancelToken.IsCancellationRequested)
                    break;

                nSkip += nTake;
                nTake = nSkip + 1 > actions.Count ? actions.Count - nSkip : 1;
            }
            while (taskSeries.Count > 0 || nTake > 0);
        }

        #endregion

        #region Methods.XML

        /// <summary>
        /// Converts the enum const to it's corresponding text value
        /// </summary>
        /// <param name="xmlConstValue"></param>
        /// <returns>Converted const valu to a text value</returns>
        public static string TranslateEnumConst(xmlConst xmlConstValue)
        {
            string sReturn = string.Empty;

            switch (xmlConstValue)
            {
                case xmlConst.Document_Standalone:
                    sReturn = false.ToString();
                    break;
                case xmlConst.Settings_IndentChars:
                    sReturn = "\t";
                    break;
                case xmlConst.Settings_Indent:
                    sReturn = true.ToString();
                    break;
                case xmlConst.Element_Root:
                    sReturn = "Root";
                    break;
                case xmlConst.Element_Header:
                    sReturn = "Header";
                    break;
                case xmlConst.Element_Header_Version:
                    sReturn = "Version";
                    break;
                case xmlConst.Element_Header_Date:
                    sReturn = "Date";
                    break;
                case xmlConst.Element_Header_Time:
                    sReturn = "Time";
                    break;
                case xmlConst.Element_Header_TimeZone:
                    sReturn = "TimeZone";
                    break;
                case xmlConst.Element_FileItems:
                    sReturn = "FileItems";
                    break;
                case xmlConst.Element_FileItem:
                    sReturn = "FileItem";
                    break;
                case xmlConst.Element_FileItem_Path:
                    sReturn = "Path";
                    break;
                case xmlConst.Element_FileItem_AbsoluteAttribute:
                    sReturn = "Absolute";
                    break;
                case xmlConst.Element_FileItem_Checksum:
                    sReturn = "Checksum";
                    break;
                case xmlConst.Element_FileItem_MethodAttribute:
                    sReturn = "Method";
                    break;
                case xmlConst.Element_FileItem_IsSensitive:
                    sReturn = "IsSensitive";
                    break;
                case xmlConst.Element_FileItem_Size:
                    sReturn = "Size";
                    break;
                case xmlConst.Format_Date:
                    sReturn = "{0:yyyy-MM-dd (dddd, MMMM dd, yyyy)}";
                    break;
                case xmlConst.Format_Time:
                    sReturn = "{0:HH:mm:ss}";
                    break;
                case xmlConst.Format_TimeZone:
                    sReturn = "[UTC]";
                    break;
                case xmlConst.FileItemsVersion:
                    sReturn = "1.0";
                    break;
                default:
                    sReturn = "{Invalid}";
                    break;
            }

            return sReturn;
        }

        /// <summary>
        /// Exports the FileItem objects collection "fileItems" into the xml file "outXmlFile"
        /// </summary>
        /// <param name="fileItems"></param>
        /// <param name="outXmlFile"></param>
        public static void SaveListToXML(Collection<Model_FileItem> fileItems, FileInfo outXmlFile)
        {
            //  delete the file if it exists
            if (outXmlFile.Exists)
                outXmlFile.Delete();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();

            xmlWriterSettings.Indent = bool.Parse(TranslateEnumConst(xmlConst.Settings_Indent));
            xmlWriterSettings.IndentChars = TranslateEnumConst(xmlConst.Settings_IndentChars);

            //  XML Start
            XmlWriter xmlWriter = XmlWriter.Create(outXmlFile.FullName, xmlWriterSettings);
            xmlWriter.WriteStartDocument(bool.Parse(TranslateEnumConst(xmlConst.Document_Standalone)));

            //  Root element
            xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Root));
            {
                //  Header
                xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Header));
                {
                    xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Header_Version));
                    xmlWriter.WriteString(TranslateEnumConst(xmlConst.FileItemsVersion));
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Header_Date));
                    xmlWriter.WriteString(string.Format(TranslateEnumConst(xmlConst.Format_Date), DateTime.UtcNow));
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Header_Time));
                    xmlWriter.WriteString(string.Format(TranslateEnumConst(xmlConst.Format_Time), DateTime.UtcNow));
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_Header_TimeZone));
                    xmlWriter.WriteString(TranslateEnumConst(xmlConst.Format_TimeZone));
                    xmlWriter.WriteEndElement();

                }//header
                xmlWriter.WriteEndElement();

                //  File items collection
                xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItems));

                foreach (Model_FileItem fileItem in fileItems)
                {
                    //  File item object
                    xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItem));
                    {
                        //  relative file path element
                        xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItem_Path));
                        xmlWriter.WriteAttributeString(TranslateEnumConst(xmlConst.Element_FileItem_AbsoluteAttribute), (!Common.IsPathRelative(fileItem.FileInfo, Common.RegistryBaseDirType.BaseDir_NeverwinterNights)).ToString());
                        xmlWriter.WriteString(Common.GetRelativePath(fileItem.FileInfo, Path.GetDirectoryName(fileItem.DefinitionFileName)));
                        xmlWriter.WriteEndElement();

                        //  is sensitive file indicator
                        xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItem_IsSensitive));
                        xmlWriter.WriteValue(fileItem.IsSensitive);
                        xmlWriter.WriteEndElement();

                        //  file size
                        xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItem_Size));
                        xmlWriter.WriteValue(fileItem.FileSize);
                        xmlWriter.WriteEndElement();

                        //  md5 checksum hash value
                        xmlWriter.WriteStartElement(TranslateEnumConst(xmlConst.Element_FileItem_Checksum));
                        xmlWriter.WriteAttributeString(TranslateEnumConst(xmlConst.Element_FileItem_MethodAttribute), fileItem.Checksum.Method.ToString());
                        xmlWriter.WriteString(fileItem.Checksum.Value);
                        xmlWriter.WriteEndElement();

                    }//file item
                    xmlWriter.WriteEndElement();

                }// foreach loop

                xmlWriter.WriteEndElement();

            }//root
            xmlWriter.WriteEndElement();

            //  XML End
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }

        /// <summary>
        /// Imports the FileItem objects collection from the xml file "outXmlFile" into the fileItems collection
        /// </summary>
        /// <param name="fileItems"></param>
        /// <param name="inXmlFile"></param>
        public static bool LoadListFromXML(Collection<Model_FileItem> fileItems, FileInfo inXmlFile, Action<Model_FileItem> OnInit)
        {
            if (!inXmlFile.Exists)
                throw new ArgumentException(string.Format("File {0} does not exist.", inXmlFile.Name));

            if (inXmlFile.Extension.ToLower() != ".xml")
                throw new ArgumentException(string.Format("Unexpected file format extension of file {0}", inXmlFile.Name));

            try
            {
                Model_FileItem fileItem;
                bool bAbsolute;
                bool isSensitive;
                long fileSize;
                string sMethod;
                string sPath;
                string sCheckSum;
                string HeaderVersion;
                string HeaderDate;
                string HeaderTime;
                string HeaderTimeZone;
                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();

                //  XML Start
                XmlReader xmlReader = XmlReader.Create(inXmlFile.FullName, xmlReaderSettings);

                //  <Root> missing - exit
                if (!xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Root)))
                    return false;

                //  <Root>
                else
                {
                    //  <Header>
                    xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Header));
                    {
                        //  <Version>
                        xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Header_Version));
                        HeaderVersion = xmlReader.ReadElementContentAsString();

                        //  <Date>
                        xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Header_Date));
                        HeaderDate = xmlReader.ReadElementContentAsString();

                        //  <Time>
                        xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Header_Time));
                        HeaderTime = xmlReader.ReadElementContentAsString();

                        //  <TimeZone>
                        xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_Header_TimeZone));
                        HeaderTimeZone = xmlReader.ReadElementContentAsString();

                    }//header

                    //  <FileItems>
                    xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItems));

                    if (!xmlReader.IsEmptyElement)
                    {
                        //  <FileItem>
                        while (xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItem)))
                        {
                            //  <Path>
                            xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItem_Path));

                            //  <Path Absolute="False">
                            bAbsolute = bool.Parse(xmlReader.GetAttribute(TranslateEnumConst(xmlConst.Element_FileItem_AbsoluteAttribute)));

                            sPath = xmlReader.ReadElementContentAsString();
                            FileInfo fileInfo = new FileInfo(sPath.Replace("{NWN_DIR}", Common.BaseDir_NWN.FullName));
                            fileItem = new Model_FileItem(fileInfo, inXmlFile.FullName);
                            
                            if (OnInit != null)
                                OnInit(fileItem);

                            //  <IsSensitive>
                            xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItem_IsSensitive));

                            isSensitive = xmlReader.ReadElementContentAsBoolean();
                            fileItem.IsSensitive = isSensitive;

                            //  <Size>
                            xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItem_Size));

                            fileSize = xmlReader.ReadElementContentAsLong();
                            fileItem.FileSize = fileSize;

                            //  <Checksum>
                            xmlReader.ReadToFollowing(TranslateEnumConst(xmlConst.Element_FileItem_Checksum));

                            //  <Checksum Method="MD5">
                            sMethod = xmlReader.GetAttribute(TranslateEnumConst(xmlConst.Element_FileItem_MethodAttribute));
                            sCheckSum = xmlReader.ReadElementContentAsString();

                            Data_Checksum checksum = new Data_Checksum();
                            checksum.Method = (ChecksumMethod)Enum.Parse(typeof(ChecksumMethod), sMethod);
                            checksum.Value = sCheckSum;

                            fileItem.Checksum = checksum;

                            //  Add the file item object
                            fileItems.Add(fileItem);

                        }//FileItem
                    }
                }// </Root>

                //  XML End
                xmlReader.Close();

                //System.Windows.MessageBox.Show(
                //    string.Format(
                //        "Imported FileItems collection:\nVersion:\t\t{0}\nDate:\t\t{1}\nTime:\t\t{2}\nTimeZone:\t{3}",
                //        HeaderVersion, HeaderDate, HeaderTime, HeaderTimeZone),
                //    "Import success",
                //    System.Windows.MessageBoxButton.OK,
                //    System.Windows.MessageBoxImage.Information);
            }

            catch
            {
                //System.Windows.MessageBox.Show("Error importing file...", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        #endregion

        #region Methods.Debug

        /// <summary>
        /// Formats the stack trace string (replaces "   at " with "\n   at ")
        /// </summary>
        /// <param name="stackTrace"></param>
        public static string FormatStackTrace(string stackTrace)
        {
            if (stackTrace == null)
                return null;
            return stackTrace.Replace("   at ", "\n   at ");
        }

        /// <summary>
        /// Tries to convert the parameter to string while considering it as an enumerable instance
        /// </summary>
        /// <param name="objList"></param>
        /// <returns>Debug version of the parameter</returns>
        public static string ObjectsToString(object objList)
        {
            if (objList == null)
                return "<null>";

            if (objList is string)
                return objList.ToString();

            else if (objList is IEnumerable)
            {
                var sb = new StringBuilder();

                foreach (var sub in objList as IEnumerable)
                    sb.AppendFormat("{0};", sub.ToString());

                return sb.ToString().TrimEnd(new[] { ';' });
            }

            else
                return objList.ToString();
        }

        /// <summary>
        /// Formatted logging helper method
        /// </summary>
        /// <param name="method">Name or tag of the caller</param>
        /// <param name="format">Same as format in string.Format</param>
        /// <param name="args">Same as args in string.Format</param>
        public static void LogEntry(string method, string format, params object[] args)
        {
            if (!DebugLoggingEnabled) 
                return;

            if (Common.BaseDir_NWN == null)
                throw new ApplicationException("BaseDir not initialized");

            lock (_logLock)
            {
                string file = string.Format(@"{0}\Debug.log", Common.BaseDir_NWN);
                string[] strings = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                    strings[i] = ObjectsToString(args[i]);

                var sb = new StringBuilder();

                foreach (var item in strings)
                    sb.AppendFormat("{0};", item);

                if (string.IsNullOrWhiteSpace(format))
                    format = sb.ToString();
                else if (sb.Length > 0)
                    format = format + string.Format(" [{0}]", sb.ToString().Replace("{", "{{").Replace("}", "}}"));

                string message = string.Format(format, args);
                Thread thread = Thread.CurrentThread;
                message = string.Format("{0:yyyy-MM-dd HH:mm:ss} - {1} - {2} - {3}", 
                    DateTime.Now, 
                    string.Format("{0}-{1} ({2}) {3} {4}", 
                        Thread.CurrentContext.ContextID,
                        thread.ManagedThreadId, 
                        thread.Name, 
                        thread.IsBackground ? "BG" : "FG", 
                        thread.IsThreadPoolThread ? "TP" : "nTP"), 
                    method, 
                    message);
                message = message.Replace("\r\n", " ");

                using (var fs = new FileStream(file, FileMode.Append))
                using (var sw = new StreamWriter(fs))
                    sw.WriteLine(message);
            }
        }

        #endregion

        #endregion

}
}
