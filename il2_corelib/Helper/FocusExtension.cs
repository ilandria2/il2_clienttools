﻿
namespace il2_corelib.Controls
{
    #region Namespaces

    using System.Windows;
    using System.Windows.Controls;

    #endregion

    /// <summary>
    /// Allows a ViewModel to set Focus to a specific UI element of it's bound View
    /// </summary>
    public static class FocusExtension
    {
        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }

        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
        }

        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached(
             "IsFocused", typeof(bool), typeof(FocusExtension),
             new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        private static void OnIsFocusedPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus(); // Don't care about false values.
            
                if (d is TextBox)
                {
                    (d as TextBox).SelectAll();
                }
                else if (d is PasswordBox)
                {
                    (d as PasswordBox).SelectAll();
                }
                else if (d is RichTextBox)
                {
                    (d as RichTextBox).SelectAll();
                }
            }
        }
    }
}