﻿namespace il2_clienttools
{
    #region Namespaces

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    #endregion

    #region Enums

    /// <summary>
    /// Defines the gff field data type
    /// </summary>
    public enum FieldDataType : uint
    {
        Byte = 0x0,
        Char = 0x1,
        Word = 0x2,
        Short = 0x3,
        Dword = 0x4,
        Int = 0x5,
        Dword64 = 0x6,
        Int64 = 0x7,
        Float = 0x8,
        Double = 0x9,
        CExoString = 0xA,
        ResRef = 0xB,
        CExoLocString = 0xC,
        Void = 0xD,
        Struct = 0xE,
        List = 0xF,
        RawData = 0xF0,
    }

    #endregion

    #region Interfaces

    /// <summary>
    /// Indicats gff structure able to hold raw data
    /// </summary>
    public interface IRawData
    {
        byte[] RawData { get; }
        int RawDataSize { get; }
    }

    /// <summary>
    /// Gff field data type
    /// </summary>
    public interface IFieldType : IRawData
    {
        FieldDataType FieldType { get; }
    }

    #endregion

    #region Classes

    public struct QuickSlotFeat
    {
        public QuickSlotFeat(int position, int id)
        {
            _position = position;
            _id = id;
        }

        private int _position;
        private int _id;

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }

    public class TStruct : IFieldType
    {
        public override string ToString()
        {
            return _structType.Value.ToString() + " - " + _fields.Count + " fields";
        }

        public TStruct()
            : this(structType: 0x0)
        {

        }

        public TStruct(uint structType)
        {
            _structType = new TDword(structType);
            _dataOrDataOffset = new TDword(Gff.OBJECT_LAST);
            _fields = new List<Field>();
        }

        private TDword _structType;
        public TDword StructType
        {
            get { return _structType; }
            set { _structType = value; }
        }

        private TDword _dataOrDataOffset;
        public TDword DataOrDataOffset
        {
            get { return _dataOrDataOffset; }
            set { _dataOrDataOffset = value; }
        }

        public TDword FieldCount
        {
            get { return new TDword((uint)Fields.Count); }
        }
        private List<Field> _fields;
        public List<Field> Fields
        {
            get { return _fields; }
            set { _fields = value; }
        }

        private IList<int> _indices;
        public IList<int> Indices
        {
            get
            {
                if (_indices == null)
                    _indices = new List<int>();

                return _indices;
            }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[12];

                StructType.RawData.CopyTo(data, 0);
                DataOrDataOffset.RawData.CopyTo(data, 4);
                FieldCount.RawData.CopyTo(data, 8);

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 12; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Struct; }
        }
    }

    public class TList : IFieldType
    {
        public TDword Size
        {
            get { return new TDword((uint)Structs.Count); }
        }

        private IList<TStruct> _structs;
        public IList<TStruct> Structs
        {
            get
            {
                if (_structs == null)
                    _structs = new List<TStruct>();

                return _structs;
            }
        }

        private IList<int> _indices;
        public IList<int> Indices
        {
            get
            {
                if (_indices == null)
                    _indices = new List<int>();

                return _indices;
            }
        }

        public void Add(TStruct child)
        {
            Structs.Add(child);
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[RawDataSize];

                Size.RawData.CopyTo(data, 0);

                for (int i = 0; i < Indices.Count; i++)
                {
                    var id = new TDword(Indices[i]);
                    id.RawData.CopyTo(data, 4 + i * 4);
                }

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 4 + Structs.Count * 4; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.List; }
        }
    }

    public class Field : IRawData
    {
        public override string ToString()
        {
            return _fieldType.FieldType.ToString() + " - " + _label.Value;
        }

        public Field(FieldDataType fieldType, string label, IFieldType fieldData)
        {
            _fieldData = fieldData;
            _fieldType = new TDword((uint)fieldType);
            _labelIndex = new TDword(0);
            _label = new Label(label);

            if (fieldType == FieldDataType.Struct)
                _data = BitConverter.GetBytes(Gff.OBJECT_LAST);
            else
                _data = _fieldData.RawData;

            // for complex types the value will be determined once the field is added to the top struct
            if (Gff.IsComplex(fieldType) && fieldType != FieldDataType.List)
                _dataOrDataOffset = new TDword(Gff.OBJECT_LAST);
            else
            {
                var temp = new byte[4];

                temp[0] = _data[0];
                temp[1] = _data.Length < 2 ? (byte)0 : _data[1];
                temp[2] = _data.Length < 3 ? (byte)0 : _data[2];
                temp[3] = _data.Length < 4 ? (byte)0 : _data[3];

                _dataOrDataOffset = new TDword(BitConverter.ToUInt32(temp, 0));
            }
        }

        private IFieldType _fieldData;
        public IFieldType FieldData
        {
            get { return _fieldData; }
            set { _fieldData = value; }
        }


        private TDword _fieldType;
        public TDword FieldType
        {
            get { return _fieldType; }
            set { _fieldType = value; }
        }

        private TDword _labelIndex;
        public TDword LabelIndex
        {
            get { return _labelIndex; }
            set { _labelIndex = value; }
        }

        private Label _label;
        public Label Label
        {
            get { return _label; }
            set { _label = value; }
        }

        private TDword _dataOrDataOffset;
        public TDword DataOrDataOffset
        {
            get { return _dataOrDataOffset; }
            set { _dataOrDataOffset = value; }
        }

        private byte[] _data;
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[12];

                FieldType.RawData.CopyTo(data, 0);
                LabelIndex.RawData.CopyTo(data, 4);
                DataOrDataOffset.RawData.CopyTo(data, 8);

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 12; }
        }
    }

    #endregion

    #region Structs

    public struct Gff : IRawData
    {
        #region Constructors

        public Gff(string fileType)
            : this(fileType, "v3.2")
        {

        }

        public Gff(string fileType, string gffVersion)
        {
            _arrayStruct = new List<TStruct>();
            _arrayField = new List<Field>();
            _arrayLabel = new List<Label>();
            _arrayFieldData = new List<RawDataBlock>();
            _arrayFieldIndices = new List<Index>();
            _arrayListIndices = new List<TList>();
            _fileType = new HeaderFileType(fileType);
            _gffVersion = new HeaderFileVersion(gffVersion);
            _builtFieldIndexes = false;
            TopStruct = new TStruct(Gff.OBJECT_LAST);

            AddStruct(TopStruct);
        }

        #endregion

        #region Constants

        public const uint OBJECT_LAST = 0xFFFFFFFF;
        public const uint OBJECT_INVALID = 0x7F000000;
        public const byte HEADER_SIZE = 2 * 4 * 7; // x7 sections x2 dword properties (offset and count) x4 (size of dword)

        #endregion

        #region Fields

        public TStruct TopStruct;

        private HeaderFileType _fileType;
        private HeaderFileVersion _gffVersion;
        private List<TStruct> _arrayStruct;
        private List<Field> _arrayField;
        private List<Label> _arrayLabel;
        private List<RawDataBlock> _arrayFieldData;
        private List<Index> _arrayFieldIndices;
        private List<TList> _arrayListIndices;
        private bool _builtFieldIndexes;

        #endregion

        #region Properties

        public HeaderFileType FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        public HeaderFileVersion GffVersion
        {
            get { return _gffVersion; }
            set { _gffVersion = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[RawDataSize];
                int index;

                index = 0;
                GetHeaderArray().RawData.CopyTo(data, index);

                index += GetHeaderArraySize();
                GetStructArray().RawData.CopyTo(data, index);

                index += GetStructArraySize();
                GetFieldArray().RawData.CopyTo(data, index);

                index += GetFieldArraySize();
                GetLabelArray().RawData.CopyTo(data, index);

                index += GetLabelArraySize();
                GetFieldDataArray().RawData.CopyTo(data, index);

                index += GetFieldDataArraySize();
                GetFieldIndicesArray().RawData.CopyTo(data, index);

                index += GetFieldIndicesArraySize();
                GetListIndicesArray().RawData.CopyTo(data, index);

                return data;
            }
        }

        public int RawDataSize
        {
            get
            {
                return
                    GetHeaderArraySize() +
                    GetStructArraySize() +
                    GetFieldArraySize() +
                    GetLabelArraySize() +
                    GetFieldDataArraySize() +
                    GetFieldIndicesArraySize() +
                    GetListIndicesArraySize()
                    ;
            }
        }

        #endregion

        #region Methods.Helpers

        private void IndexData(Field field)
        {
            switch ((FieldDataType)field.FieldType.Value)
            {
                case FieldDataType.Struct:
                    field.DataOrDataOffset = new TDword(_arrayStruct.Count - 1);
                    break;

                case FieldDataType.List:
                    field.DataOrDataOffset = new TDword((uint)GetListIndicesArraySize() - (uint)field.FieldData.RawDataSize);
                    break;

                default:
                    field.DataOrDataOffset = new TDword(GetFieldDataArraySize());
                    _arrayFieldData.Add(new RawDataBlock(field.Data));
                    break;
            }
        }

        private void BuildFieldIndicesOnce()
        {
            if (_builtFieldIndexes) return;

            foreach (var structItem in _arrayStruct)
            {
                // single-field list
                if (structItem.Fields.Count == 1)

                    // value is index into field array
                    structItem.DataOrDataOffset = new TDword((uint)structItem.Indices[0]);

                else
                {
                    for (int f = 0; f < structItem.Fields.Count; f++)
                    {
                        // becoming multi-field - struct offset to new region in the field indices array
                        if (f == 0)
                            structItem.DataOrDataOffset = new TDword(_arrayFieldIndices.Count * 4);

                        // add the field index
                        _arrayFieldIndices.Add(new Index(structItem.Indices[f]));
                    }
                }
            }

            _builtFieldIndexes = true;
        }

        private void IndexField(TStruct parentStruct, Field field)
        {
            parentStruct.Fields.Add(field);
            parentStruct.Indices.Add(_arrayField.Count);
            _arrayField.Add(field);
        }

        private void IndexLabel(TStruct parentStruct, Field field)
        {
            var fieldLabel = field.Label.Value;
            var labelIndex = _arrayLabel.FindIndex(l => l.Value == fieldLabel);

            if (labelIndex == -1)
            {
                _arrayLabel.Add(field.Label);
                labelIndex = _arrayLabel.Count - 1;
            }

            // cannot add two fields with an identical label to one struct
            if (parentStruct.Fields.Any(f => f.Label.Value == fieldLabel))
                throw new ApplicationException(string.Format("Struct already contains a field labeled as {0}", field.Label.Value));

            field.LabelIndex = new TDword((uint)labelIndex);
        }

        private void AddStruct(TStruct structItem)
        {
            _arrayStruct.Add(structItem);
        }

        private void AddList(TList list)
        {
            foreach (var structItem in list.Structs)
            {
                list.Indices.Add(_arrayStruct.Count);
                AddStruct(structItem);
            }

            _arrayListIndices.Add(list);
        }

        #endregion

        #region Methods.Interface

        /// <summary>
        /// Indicates whether the gff field data type "fieldType" is a complex type
        /// </summary>
        /// <param name="fieldType"></param>
        internal static bool IsComplex(FieldDataType fieldType)
        {
            switch (fieldType)
            {
                case FieldDataType.Byte:
                case FieldDataType.Char:
                case FieldDataType.Word:
                case FieldDataType.Short:
                case FieldDataType.Dword:
                case FieldDataType.Int:
                case FieldDataType.Float:
                    return false;
                case FieldDataType.Dword64:
                case FieldDataType.Int64:
                case FieldDataType.Double:
                case FieldDataType.CExoString:
                case FieldDataType.ResRef:
                case FieldDataType.CExoLocString:
                case FieldDataType.Void:
                case FieldDataType.Struct:
                case FieldDataType.List:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException("fieldType");
            }
        }

        public void Export(string file)
        {
            FileInfo fi = new FileInfo(file);
            TCResRef filename = new TCResRef(Path.GetFileNameWithoutExtension(file));

            file = fi.DirectoryName + @"\" + filename.ResRef + ".bic";

            using (var fs = new FileStream(file, FileMode.Create))
            using (BinaryWriter wr = new BinaryWriter(fs))
                wr.Write(RawData);
        }

        public void Add(string label, IFieldType gffTypeInstance)
        {
            Add(TopStruct, label, gffTypeInstance);
        }

        public void Add(TStruct parentStruct, string label, IFieldType data)
        {
            if (data is TStruct)
                AddStruct((TStruct)data);

            else if (data is TList)
                AddList((TList)data);

            var field = new Field(data.FieldType, label, data);

            IndexLabel(parentStruct, field);
            IndexField(parentStruct, field);

            if (Gff.IsComplex(data.FieldType))
                IndexData(field);
        }

        public RawDataBlock GetHeaderArray()
        {
            var dataHeader = new RawDataBlock();
            var sizeHeader = GetHeaderArraySize();
            var sizeStruct = GetStructArraySize();
            var sizeField = GetFieldArraySize();
            var sizeLabel = GetLabelArraySize();
            var sizeFieldData = GetFieldDataArraySize();
            var sizeFieldIndices = GetFieldIndicesArraySize();
            var sizeListIndices = GetListIndicesArraySize();

            // file info header
            dataHeader.Add(FileType.RawData);
            dataHeader.Add(GffVersion.RawData);

            // structs
            dataHeader.Add(Gff.HEADER_SIZE + 0);
            dataHeader.Add(sizeStruct / 4 / 3);

            // fields
            dataHeader.Add(Gff.HEADER_SIZE + sizeStruct);
            dataHeader.Add(sizeField / 4 / 3);

            // labels
            dataHeader.Add(Gff.HEADER_SIZE + sizeStruct + sizeField);
            dataHeader.Add(sizeLabel / 16);

            // field data
            dataHeader.Add(Gff.HEADER_SIZE + sizeStruct + sizeField + sizeLabel);
            dataHeader.Add(sizeFieldData);

            // field indices
            dataHeader.Add(Gff.HEADER_SIZE + sizeStruct + sizeField + sizeLabel + sizeFieldData);
            dataHeader.Add(sizeFieldIndices);

            // list indices
            dataHeader.Add(Gff.HEADER_SIZE + sizeStruct + sizeField + sizeLabel + sizeFieldData + sizeFieldIndices);
            dataHeader.Add(sizeListIndices);

            return dataHeader;
        }

        public RawDataBlock GetStructArray()
        {
            var block = new RawDataBlock();

            foreach (var item in _arrayStruct)
                block.Add(item.RawData);

            return block;
        }

        public RawDataBlock GetFieldArray()
        {
            var block = new RawDataBlock();

            foreach (var item in _arrayField)
                block.Add(item.RawData);

            return block;
        }

        public RawDataBlock GetLabelArray()
        {
            var block = new RawDataBlock();

            foreach (var item in _arrayLabel)
                block.Add(item.RawData);

            return block;
        }

        public RawDataBlock GetFieldDataArray()
        {
            var block = new RawDataBlock();

            foreach (var item in _arrayFieldData)
                block.Add(item.RawData);

            return block;
        }

        public RawDataBlock GetFieldIndicesArray()
        {
            BuildFieldIndicesOnce();

            var block = new RawDataBlock();

            foreach (var item in _arrayFieldIndices)
                block.Add(item.RawData);

            return block;
        }

        public RawDataBlock GetListIndicesArray()
        {
            var block = new RawDataBlock();

            foreach (var item in _arrayListIndices)
                block.Add(item.RawData);

            return block;
        }

        public int GetHeaderArraySize()
        {
            return Gff.HEADER_SIZE;
        }

        public int GetStructArraySize()
        {
            var size = 0;

            foreach (var item in _arrayStruct)
                size += item.RawDataSize;

            return size;
        }

        public int GetFieldArraySize()
        {
            var size = 0;

            foreach (var item in _arrayField)
                size += item.RawDataSize;

            return size;
        }

        public int GetLabelArraySize()
        {
            var size = 0;

            foreach (var item in _arrayLabel)
                size += item.RawDataSize;

            return size;
        }

        public int GetFieldDataArraySize()
        {
            var size = 0;

            foreach (var item in _arrayFieldData)
                size += item.RawDataSize;

            return size;
        }

        public int GetFieldIndicesArraySize()
        {
            BuildFieldIndicesOnce();

            var size = 0;

            foreach (var item in _arrayFieldIndices)
                size += item.RawDataSize;

            return size;
        }

        public int GetListIndicesArraySize()
        {
            var size = 0;

            foreach (var item in _arrayListIndices)
                size += item.RawDataSize;

            return size;
        }

        #endregion
    }

    public struct Index : IRawData
    {
        public Index(uint value)
        {
            _value = value;
        }

        public Index(int value)
            : this((uint)value)
        {

        }

        private uint _value;
        public uint Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 4; }
        }
    }

    public struct TChar : IFieldType
    {
        private char InitValue;

        public TChar(char value)
        {
            InitValue = value;
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(InitValue); }
        }

        public int RawDataSize
        {
            get { return 1; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Char; }
        }
    }

    public struct TByte : IFieldType
    {
        private byte _value;
        public byte Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public TByte(byte value)
        {
            _value = value;
        }

        public byte[] RawData
        {
            get { return new byte[] { Value }; }
        }

        public int RawDataSize
        {
            get { return 1; }
        }


        public FieldDataType FieldType
        {
            get { return FieldDataType.Byte; }
        }
    }

    public struct TDword : IFieldType
    {
        public TDword(uint value)
        {
            _value = value;
        }

        public TDword(int value)
        {
            if (value == -1)
                _value = Gff.OBJECT_LAST;
            else
                _value = (uint)(value < 0 ? 0 : value);
        }

        private uint _value;
        public uint Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 4; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Dword; }
        }
    }

    public struct Label : IRawData
    {
        public Label(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                value = string.Empty;

            _value = value.Substring(0, (value.Length > 16) ? 16 : value.Length);
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[RawDataSize];
                var value = Encoding.UTF8.GetBytes(Value);

                value.CopyTo(data, 0);

                for (int i = value.Length; i < RawDataSize; i++)
                    data[i] = 0x00;

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 16; }
        }
    }

    public struct TDouble : IFieldType
    {
        private double _value;
        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public TDouble(double value)
        {
            _value = value;
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 8; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Double; }
        }
    }

    public struct TFloat : IFieldType
    {
        public TFloat(float value)
        {
            _value = value;
        }

        private float _value;
        public float Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 4; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Float; }
        }
    }

    public struct TInt : IFieldType
    {
        public TInt(int value)
        {
            _value = value;
        }

        private int _value;
        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 4; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Int; }
        }
    }

    public struct TShort : IFieldType
    {
        public TShort(short value)
        {
            _value = value;
        }

        private short _value;
        public short Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 2; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Short; }
        }
    }

    public struct TWord : IFieldType
    {
        public TWord(ushort value)
        {
            _value = value;
        }

        private ushort _value;
        public ushort Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] RawData
        {
            get { return BitConverter.GetBytes(Value); }
        }

        public int RawDataSize
        {
            get { return 2; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.Word; }
        }
    }

    public struct TCExoString : IFieldType
    {
        public TCExoString(string value)
        {
            _characters = string.Empty;
            _size = new TDword(0);

            Characters = value;
        }

        private string _characters;
        public string Characters
        {
            get { return _characters; }
            set { _characters = value; _size = new TDword((uint)_characters.Length); }
        }

        private TDword _size;
        public TDword Size
        {
            get { return _size; }
            private set { _size = value; }
        }

        public byte[] RawData
        {
            get
            {
                var dataSize = BitConverter.GetBytes(Characters.Length);
                var dataValue = Encoding.UTF8.GetBytes(Characters);
                var data = new byte[dataSize.Length + dataValue.Length];

                dataSize.CopyTo(data, 0);
                dataValue.CopyTo(data, dataSize.Length);

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 4 + Characters.Length; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.CExoString; }
        }
    }

    public struct TCExoLocString : IFieldType
    {
        private TCExoLocString(bool x = false)
        {
            _stingRef = new TDword(Gff.OBJECT_LAST);
            _subStrings = new List<GenderSubString>();
        }

        public TCExoLocString(TDword stringRef)
            : this(false)
        {
            StringRef = stringRef;
        }

        public TCExoLocString(GenderSubString subString)
            : this(false)
        {
            SubStrings.Add(subString);
        }

        public TCExoLocString(string engString)
            : this(false)
        {
            SubStrings.Add(new GenderSubString(false, 0, engString));
        }

        private TDword _stingRef;
        public TDword StringRef
        {
            get { return _stingRef; }
            set { _stingRef = value; }
        }

        public TDword StringCount
        {
            get { return new TDword((uint)SubStrings.Count); }
        }
        private IList<GenderSubString> _subStrings;
        public IList<GenderSubString> SubStrings
        {
            get
            {
                if (_subStrings == null)
                    _subStrings = new List<GenderSubString>();

                return _subStrings;
            }
        }

        public RawDataBlock SubStringsData
        {
            get
            {
                var data = new byte[SubStringsSize.Value];
                var index = 0;

                foreach (var subString in SubStrings)
                {
                    subString.RawData.CopyTo(data, index);
                    index = index + subString.RawDataSize;
                }

                return new RawDataBlock(data);
            }
        }

        public TDword SubStringsSize
        {
            get
            {
                return new TDword(SubStrings.Sum(sub => sub.RawDataSize));
            }
        }

        public TDword TotalSize
        {
            get
            {
                return new TDword(RawDataSize - 4);
            }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[12 + SubStringsSize.Value];

                TotalSize.RawData.CopyTo(data, 0);
                StringRef.RawData.CopyTo(data, 4);
                StringCount.RawData.CopyTo(data, 8);
                SubStringsData.RawData.CopyTo(data, 12);

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 12 + (int)SubStringsSize.Value; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.CExoLocString; }
        }
    }

    public struct GenderSubString : IRawData
    {
        public GenderSubString(bool isFemale, int languageID, string subString)
        {
            _stringID = new TDword((uint)((isFemale ? 1 : 0) + languageID * 2));
            _subString = new TCExoString(subString);
        }

        private TDword _stringID;
        public TDword StringID
        {
            get { return _stringID; }
            set { _stringID = value; }
        }

        private TCExoString _subString;
        public TCExoString SubString
        {
            get { return _subString; }
            set { _subString = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[RawDataSize];

                StringID.RawData.CopyTo(data, 0);
                SubString.RawData.CopyTo(data, 4);

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 4 + SubString.RawDataSize; }
        }
    }

    public struct TCResRef : IFieldType
    {
        static Regex rgxInvalidChars = new Regex(@"[^a-z0-9_]");

        public TCResRef(string resRef)
        {
            resRef = resRef.ToLower().Trim();
            resRef = rgxInvalidChars.Replace(resRef, string.Empty);

            if (resRef.Length > 16)
                resRef = resRef.Substring(0, 16);

            _resRef = resRef.ToLower().Trim();
            _size = new TByte((byte)resRef.Length);
        }

        private TByte _size;
        public TByte Size
        {
            get { return _size; }
            set { _size = value; }
        }

        private string _resRef;
        public string ResRef
        {
            get { return _resRef; }
            set { _resRef = value.ToLower().Trim(); _size = new TByte((byte)_resRef.Length); }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[RawDataSize];

                Size.RawData.CopyTo(data, 0);

                for (int i = 0; i < ResRef.Length; i++)
                    data[i + 1] = (byte)ResRef[i];

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 1 + ResRef.Length; }
        }

        public FieldDataType FieldType
        {
            get { return FieldDataType.ResRef; }
        }
    }

    public struct HeaderFileType : IRawData
    {
        public HeaderFileType(string fileType)
        {
            fileType = fileType.TrimStart();

            if (string.IsNullOrWhiteSpace(fileType) || fileType.Length > 4)
                throw new ArgumentOutOfRangeException("fileType");

            _fileType = fileType.ToUpper();
        }

        private string _fileType;
        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[4];

                for (int i = 0; i < 4; i++)
                {
                    if (i > FileType.Length - 1)
                        data[i] = 0x20; //space
                    else
                        data[i] = (byte)FileType[i];
                }

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 4; }
        }
    }

    public struct HeaderFileVersion : IRawData
    {
        public HeaderFileVersion(string gffVersion)
        {
            gffVersion = gffVersion.TrimStart();

            if (string.IsNullOrWhiteSpace(gffVersion) || gffVersion.Length > 4)
                throw new ArgumentOutOfRangeException("gffVersion");

            _gffVersion = gffVersion.ToUpper();
        }

        private string _gffVersion;
        public string GffVersion
        {
            get { return _gffVersion; }
            set { _gffVersion = value; }
        }

        public byte[] RawData
        {
            get
            {
                var data = new byte[4];

                for (int i = 0; i < 4; i++)
                {
                    if (i > GffVersion.Length - 1)
                        data[i] = 0x20; //space
                    else
                        data[i] = (byte)GffVersion[i];
                }

                return data;
            }
        }

        public int RawDataSize
        {
            get { return 4; }
        }
    }

    public struct RawDataBlock : IRawData
    {
        public RawDataBlock(byte[] data)
        {
            _data = data;
        }

        public void Add(TDword value)
        {
            Add(value.RawData);
        }

        public void Add(int value)
        {
            Add(new TDword(value));
        }

        public void Add(byte[] block)
        {
            if (_data == null)
            {
                _data = block;
                return;
            }

            var temp = new byte[_data.Length + block.Length];
            _data.CopyTo(temp, 0);
            block.CopyTo(temp, _data.Length);
            _data = temp;
            temp = null;
        }

        private byte[] _data;

        public byte[] RawData
        {
            get
            {
                if (_data == null)
                    _data = new byte[0];

                return _data;
            }
        }

        public int RawDataSize
        {
            get { return _data.Length; }
        }
    }

    #endregion
}