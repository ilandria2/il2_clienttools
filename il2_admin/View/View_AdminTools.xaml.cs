﻿
namespace il2_clienttools.View
{
    #region Namespaces

    using il2_corelib.Interfaces;
    using System.Windows;

    #endregion

    public partial class View_AdminTools : Window, IMinimizeable
    {
        public View_AdminTools()
        {
            InitializeComponent();
        }

        public void Minimize()
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
    }
}
