using System.Reflection;
using System.Runtime.InteropServices;



/************ Local assembly info *************/

[assembly: AssemblyTitle("Admin tools")]
[assembly: AssemblyDescription("A set of tools used by Ilandria 2 project developers")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("8B2DCFDD-7D9C-48EE-A990-D0523AC82A02")]

//VERSION=0.6.149
[assembly: AssemblyInformationalVersion("0.6.149")]
[assembly: AssemblyFileVersion("0.6.149")]
[assembly: AssemblyVersion("0.6.149")]
