﻿
namespace il2_admin
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.View;
    using il2_clienttools.ViewModel;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.IO;
    using System.Reflection;
    using System.Windows;
    using System.Threading;

    #endregion

    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            string M = string.Format("{0}.{1}", GetType().Name, ".OnStartUp()");
            Common.DebugLoggingEnabled = true;
            App.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            Common.LogEntry(M, "Started with args: {0} (Batch mode = {1})", Common.AppArgs, Common.BatchMode);

            var args = Common.AppArgs.ToList();

            if (Common.BatchMode)
            {
                ///////////// checksum tool ///////////////////
                if (args.Contains("-c"))
                {
                    Common.LogEntry(M, "Compute checksum");

                    string file = Common.AppArgFile;

                    if (file == null)
                        throw new ApplicationException("You must specify file to be processed by the checksum tool");

                    Common.LogEntry(M, "Using file: {0}", file);
                    new ViewModel_ToolChecksum(file, true);
                    Environment.Exit(Environment.ExitCode);
                }

                ///////////// missing operation ////////////////
                else 
                    throw new ApplicationException("Unable to run admin tools in batch mode without specifying the operation");

                Environment.Exit(Environment.ExitCode);
            }

            ///////////////// UI mode /////////////////////
            Common.LogEntry(M, "Entering UI mode");
            Common.InitView(typeof(View_AdminTools), typeof(ViewModel_AdminTools), 2, true, true);

            if (args.Contains("-c"))
            {
                var adminTools = Common.MainViewModel as ViewModel_AdminTools;

                if (adminTools == null)
                    throw new ApplicationException("Unable to get the reference to the admin tools object.");

                string file = Common.AppArgFile;
                ViewModel_ToolChecksum toolChecksum;

                if (file == null)
                    toolChecksum = new ViewModel_ToolChecksum();
                else
                    toolChecksum = new ViewModel_ToolChecksum(file);
                
                adminTools.Workspaces.Add(toolChecksum);
                adminTools.SetActiveWorkspace(toolChecksum);
            }
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "DispatcherUnhandledException()");

            if (!Common.BatchMode)
                MessageBox.Show(
                    string.Format(
                        "An error occured:{0}: {1}\n\nStack trace:\n{2}",
                        e.Exception.GetType().FullName, e.Exception.Message, e.Exception.StackTrace
                    ), "Unhandled error occured!", MessageBoxButton.OK, MessageBoxImage.Error
                );
            else
                Common.LogEntry(M, "Unhandled error occured: {0} - {1}\nStack trace:\n{2}", e.Exception.GetType().FullName, e.Exception.Message, e.Exception.StackTrace);
            
            e.Handled = true;
        }
    }
}
