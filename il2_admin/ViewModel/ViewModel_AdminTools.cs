﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.ComponentModel;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows;
using il2_corelib.Interfaces;

    #endregion



    /// <summary>
    /// The ViewModel for the main window called "Admin tools"
    /// </summary>
    public class ViewModel_AdminTools : ViewModel_Window
    {
        #region Fields
        
        private ReadOnlyCollection<ViewModel_Command> _commands;
        private ObservableCollection<ViewModel_Workspace> _workspaces;
        private ICommand _command_CloseActiveWorkspace;

        #endregion

        #region Properties

        /// <summary>
        /// Returns a read-only collection of commands that the UI can display and execute
        /// </summary>
        public ReadOnlyCollection<ViewModel_Command> Tools
        {
            get 
            {
                if (this._commands == null)
                    this._commands = new ReadOnlyCollection<ViewModel_Command>(this.CreateCommands());

                return _commands; 
            }
        }

        /// <summary>
        /// Gets the collection of available workspaces to display.
        /// A 'workspace' is a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<ViewModel_Workspace> Workspaces
        {
            get 
            {
                if (this._workspaces == null)
                {
                    this._workspaces = new ObservableCollection<ViewModel_Workspace>();
                    this._workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }

                return _workspaces; 
            }
        }

        /// <summary>
        /// Returns the active workspace
        /// </summary>
        public ViewModel_Workspace ActiveWorkspace { get { return this.GetActiveWorkspace(); } }
        
        #region Properties.Overrides

        /// <summary>
        /// Gets the title of this ViewModel
        /// </summary>
        public override string DisplayName
        {
            get
            {
                var verExec = AssemblyInfo.GetVersionExecuting();
                var verCall = AssemblyInfo.GetVersionCalling();

                string Title = string.Format("{0}, {1}", verCall, verExec);

                return Title;
            }
        }

        #endregion

        #region Properties.Commands

        /// <summary>
        /// Returns an ICommand object instance from Tools ICommand objects collection which is bound to Ctrl+1 key
        /// </summary>
        public ICommand Command_D1 { get { return this.Tools[0].Command; } }

        /// <summary>
        /// Returns an ICommand object instance from Tools ICommand objects collection which is bound to Ctrl+2 key
        /// </summary>        
        public ICommand Command_D2 { get { return this.Tools[1].Command; } }

        /// <summary>
        /// Returns an ICommand object instance from Tools ICommand objects collection which is bound to Ctrl+3 key
        /// </summary>        
        public ICommand Command_D3 { get { return this.Tools[2].Command; } }

        /// <summary>
        /// Returns the CloseActiveWorkspace command instance
        /// </summary>
        public ICommand Command_CloseActiveWorkspace
        {
            get
            {
                if (this._command_CloseActiveWorkspace == null)
                    this._command_CloseActiveWorkspace =
                        new RelayCommand(
                            param => this.OnWorkspaceRequestClose(this.ActiveWorkspace, EventArgs.Empty)
                        );

                return this._command_CloseActiveWorkspace;
            }
        }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new ViewModel for the main window called "Admin tools"
        /// </summary>
        public ViewModel_AdminTools(ICloseable close)
            : base(close)
        {
        }

        /// <summary>
        /// Creates a new ViewModel for the main window called "Admin tools"
        /// </summary>
        public ViewModel_AdminTools(IMinimizeable minimize)
            : base(minimize)
        {

        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new instance of a read-only collection of commands that the
        /// user interface will be using
        /// </summary>
        /// <returns>A read-only collection of commands</returns>
        private List<ViewModel_Command> CreateCommands()
        {
            int c = 0;
            var KeyBindingModifier = ModifierKeys.Control;
            var CommandChecksum = new RelayCommand(param => this.OpenToolChecksum());
            var Command2 = new RelayCommand(
                        param => System.Windows.MessageBox.Show(
                            "Command 2 is just a placeholder.",
                            "Command 2",
                            System.Windows.MessageBoxButton.OK,
                            System.Windows.MessageBoxImage.Information
                        )
                );
            var Command3 = new RelayCommand(
                        param => System.Windows.MessageBox.Show(
                            "Command 3 is just a placeholder.",
                            "Command 3",
                            System.Windows.MessageBoxButton.OK,
                            System.Windows.MessageBoxImage.Information
                        )
                );

            var Commands = new List<ViewModel_Command>()
            {
                new ViewModel_Command(
                    "Checksum tool",
                    CommandChecksum,
                    ++c > 9 ? null : new KeyBinding(CommandChecksum, (Key)Enum.Parse(typeof(Key), "D" + c.ToString()), KeyBindingModifier)
                ),

                new ViewModel_Command(
                    "Command 2",
                    Command2,
                    ++c > 9 ? null : new KeyBinding(Command2, (Key)Enum.Parse(typeof(Key), "D" + c.ToString()), KeyBindingModifier),
                    false
                ),

                new ViewModel_Command(
                    "Command 3",
                    Command3,
                    ++c > 9 ? null : new KeyBinding(Command3, (Key)Enum.Parse(typeof(Key), "D" + c.ToString()), KeyBindingModifier),
                    false
                ),
            };

            return Commands;
        }

        /// <summary>
        /// Creates a new workspace for a tool called "Checksum tool"
        /// Adds the workspace to the collection
        /// Activates this workspace
        /// </summary>
        private void OpenToolChecksum()
        {
            ViewModel_Workspace workspace = new ViewModel_ToolChecksum();

            this.Workspaces.Add(workspace);
            this.SetActiveWorkspace(workspace);
        }

        /// <summary>
        /// When the Workspaces collection changes then (un)subscribes the
        /// RequestClose event of every added / removed workspace ViewModel
        /// </summary>
        private void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count > 0)
                foreach (ViewModel_Workspace workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if (e.OldItems != null && e.OldItems.Count > 0)
                foreach (ViewModel_Workspace workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        /// <summary>
        /// When the Workspace's RequestClose event fires then the
        /// Dispose method of that workspace is invoked and the workspace
        /// is removed from the Workspaces collection.
        /// </summary>
        private void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            ViewModel_Workspace workspace = sender as ViewModel_Workspace;

            if (workspace != null)
            {
                if (MessageBox.Show(
                    string.Format("Close the following workspace?\n\n{0}", workspace.DisplayName), "Close workspace?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                ((IDisposable)workspace).Dispose();
                this.Workspaces.Remove(workspace);
            }
        }

        /// <summary>
        /// Sets a specific workspace as an active/selected workspace
        /// </summary>
        /// <param name="workspace"></param>
        public void SetActiveWorkspace(ViewModel_Workspace workspace)
        {
            System.Diagnostics.Debug.Assert(this.Workspaces.Contains(workspace));

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Workspaces);

            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }

        /// <summary>
        /// Gets the active workspace of this view model
        /// </summary>
        /// <returns></returns>
        private ViewModel_Workspace GetActiveWorkspace()
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Workspaces);

            if (collectionView != null)
                return collectionView.CurrentItem as ViewModel_Workspace;

            return null;
        }

        #endregion

    }

}
