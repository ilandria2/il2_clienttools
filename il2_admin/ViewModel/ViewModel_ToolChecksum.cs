﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using Helper;
    using Data;
    using Model;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;

    #endregion


    /// <summary>
    /// The ViewModel for a workspace called "Checksum tool"
    /// </summary>
    public class ViewModel_ToolChecksum : ViewModel_Workspace, IDataErrorInfo
    {
        const int MAX_HOPS = 300;

        #region Fields

        private ObservableCollection<Model_FileItem> _fileItems;

        private Model_FileItem _selectedFileItem;

        private CancellationTokenSource _cancelSource;
        
        private bool _computeState;

        private int _selectedFileItemIndex;

        private long _totalFileSize;
        
        private ICommand _command_Add;

        private ICommand _command_Remove;

        private ICommand _command_Save;
        
        private ICommand _command_Load;

        private ICommand _command_Compute;

        private ICommand _command_Cancel;
        
        private Timer _timer;

        private object _timerLock = new object();
        
        private string _definitionFileName;
        private bool _computeHadErrors;

        #endregion

        #region Properties

        #region Properties.CLR

        /// <summary>
        /// Gets or sets whether there were any errors during the compute operation
        /// </summary>
        public bool ComputeHadErrors
        {
            get { return _computeHadErrors; }
            set { _computeHadErrors = value; }
        }

        /// <summary>
        /// Gets or sets the base directory as an origin for the file items
        /// </summary>
        public string DefinitionFileName
        {
            get { return _definitionFileName; }
            set 
            {
                if (_definitionFileName == value)
                    return;

                _definitionFileName = value;

                base.OnPropertyChanged("DefinitionFileName");
            }
        }

        /// <summary>
        /// Gets the instance of a Timer
        /// </summary>
        private Timer Timer
        {
            get
            {
                if (_timer == null)
                    _timer = new Timer(OnTimerHit, null, Timeout.Infinite, 1000);

                return _timer;
            }
        }

        /// <summary>
        /// Gets or sets the CancellationTokenSource for the view model instance
        /// </summary>
        public CancellationTokenSource CancelSource
        {
            set { _cancelSource = value; }
            get
            {
                if (this._cancelSource == null)
                    this._cancelSource = new CancellationTokenSource();

                return this._cancelSource;
            }
        }

        /// <summary>
        /// Gets the CancellationToken from this view model's CancellationTokenSource instance
        /// </summary>
        public CancellationToken CancelToken
        {
            get { return this.CancelSource.Token; }
        }

        /// <summary>
        /// Gets or sets the current checksum computation state value
        /// </summary>
        public bool ComputeState
        {
            get { return this._computeState; }
            set 
            { 
                if (this._computeState == value) return;

                this._computeState = value;

                base.OnPropertyChanged("ComputeState");
                base.OnPropertyChanged("Command_Compute");
                base.OnPropertyChanged("Command_Cancel");
                base.OnPropertyChanged("Command_ComputeCancel");
            }
        }

        /// <summary>
        /// Gets the FileItem objects collection
        /// </summary>
        public ObservableCollection<Model_FileItem> FileItems
        {
            get 
            {
                if (this._fileItems == null)
                    this._fileItems = new ObservableCollection<Model_FileItem>();

                return this._fileItems; 
            }
        }

        /// <summary>
        /// Gets or sets the SelectedFileItem on list
        /// </summary>
        public Model_FileItem SelectedFileItem
        {
            get { return _selectedFileItem; }
            set { _selectedFileItem = value; }
        }

        /// <summary>
        /// Gets or sets the SelectedFileItemIndex on list
        /// </summary>
        public int SelectedFileItemIndex
        {
            get { return _selectedFileItemIndex; }
            set { _selectedFileItemIndex = value; }
        }

        /// <summary>
        /// Gets or sets the overall progress
        /// </summary>
        public decimal OverallProgress
        {
            get 
            {
                this.FileItems.ToList().ForEach((fi) => fi.IndicateStatusChanged());

                return this.FileItems.Sum(fi => 
                    (fi.Status * fi.FileSize) // computed size so far
                    / this.TotalFileSize
                    );
            }
        }

        /// <summary>
        /// Determines the maximum value of the associated progress bar
        /// </summary>
        public decimal OverallProgress_Max 
        {
            get { return 100M; }
            //get { return this.FileItems.Sum(fi => fi.FileSize); }
        }

        /// <summary>
        /// Gets the total file size of all file items loaded
        /// </summary>
        public long TotalFileSize
        {
            get 
            {
                if (this._totalFileSize == 0)
                    this._totalFileSize = this.FileItems.Sum(fi => fi.FileSize);
                
                return this._totalFileSize; 
            }
        }

        #endregion

        #region Properties.Commands

        /// <summary>
        /// Gets the command which adds file(s) into the list
        /// </summary>
        public ICommand Command_Add
        {
            get 
            {
                if (this._command_Add == null)
                    this._command_Add =
                        new RelayCommand(
                            this.Execute_Add,
                            this.CanExecute_Add
                        );

                return this._command_Add; 
            }
        }

        /// <summary>
        /// Gets the command which removes file(s) from the list
        /// </summary>
        public ICommand Command_Remove
        {
            get 
            {
                if (this._command_Remove == null)
                    this._command_Remove =
                        new RelayCommand(
                            this.Execute_Remove,
                            this.CanExecute_Remove
                        );
                
                return _command_Remove; 
            }
        }

        /// <summary>
        /// Gets the command which saves the list
        /// </summary>
        public ICommand Command_Save
        {
            get 
            {
                if (this._command_Save == null)
                    this._command_Save =
                        new RelayCommand(
                            this.Execute_Save,
                            this.CanExecute_Save
                        ); 

                return _command_Save; 
            }
        }

        /// <summary>
        /// Gets the command which can load a pre-saved xml list of file items into the list
        /// </summary>
        public ICommand Command_Load
        {
            get 
            {
                if (this._command_Load == null)
                    this._command_Load =
                        new RelayCommand(
                            this.Execute_Load,
                            this.CanExecute_Load
                        );

                return this._command_Load; 
            }
        }

        /// <summary>
        /// Gets the command which starts the Compute checksum task
        /// </summary>
        public ICommand Command_Compute
        {
            get 
            {
                if (this._command_Compute == null)
                    this._command_Compute =
                        new RelayCommand(
                            this.Execute_Compute,
                            this.CanExecute_Compute
                        );

                return this._command_Compute; 
            }
        }

        /// <summary>
        /// Gets the command which cancels the Compute checksum task
        /// </summary>
        public ICommand Command_Cancel
        {
            get 
            {
                if (this._command_Cancel == null)
                    this._command_Cancel =
                        new RelayCommand(
                            this.Execute_Cancel,
                            this.CanExecute_Cancel
                        );

                return this._command_Cancel; 
            }
        }

        /// <summary>
        /// Based on ComputeState gets the respective Command_Compute or Command_Cancel commands
        /// </summary>
        public ICommand Command_ComputeCancel
        {
            get 
            {
                return this.ComputeState ? this.Command_Cancel : this.Command_Compute;
            }
        }

        #endregion  // commands
        
        #region Properties.Overrides

        /// <summary>
        /// Gets the display name of this view model
        /// </summary>
        public override string DisplayName
        {
            get { return "Checksum tool"; }
        }

        #endregion

        #endregion  // properties

        #region Methods

        #region Commands.CanExecute

        /// <summary>
        /// Determines whether the Add command can execute
        /// </summary>
        private bool CanExecute_Add(object param)
        {
            return !this.ComputeState && DefinitionFileName != null;
        }

        /// <summary>
        /// Determines whether the Remove command can execute
        /// </summary>
        private bool CanExecute_Remove(object param)
        {
            return !this.ComputeState && this.FileItems.Count > 0 && DefinitionFileName != null;
        }

        /// <summary>
        /// Determines whether the Save command can execute
        /// </summary>
        private bool CanExecute_Save(object param)
        {
            return !this.ComputeState && this.FileItems.Count > 0 && DefinitionFileName != null;
        }

        /// <summary>
        /// Determines whether the Load command can execute
        /// </summary>
        private bool CanExecute_Load(object param)
        {
            return !this.ComputeState;
        }

        /// <summary>
        /// Determines whether the command Compute can execute
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private bool CanExecute_Compute(object param)
        {
            return !this.ComputeState && this.FileItems.Count > 0 && DefinitionFileName != null;
        }

        /// <summary>
        /// Determines whether the Cancel command can execute
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private bool CanExecute_Cancel(object param)
        {
            return this.ComputeState;
        }

        #endregion  // Commands.CanExecute

        #region Commands.Execute

        /// <summary>
        /// Performs the following commands:
        /// * Opens the FileOpen dialog window
        /// * Adds the selected file to the list view's ItemsSource collection
        /// </summary>
        private void Execute_Add(object param)
        {
            var isSensitive = param != null && param.ToString() == "Sensitive";

            this.AddFiles(isSensitive);
        }

        /// <summary>
        /// Removes the selected file from the list view's ItemsSource collection
        /// </summary>
        private void Execute_Remove(object param)
        {
            this.RemoveFiles(param);
        }

        /// <summary>
        /// Asks the user to specify the file path
        /// This parent folder of this file will be then taken
        /// Creates a file called il2_content.xml in the specified folder
        /// Fills the file with:
        /// * Header containing date and version
        /// * FileItems collection containing list of FileItem objects from the ListView
        /// </summary>
        private void Execute_Save(object param)
        {
            SaveList();
        }

        /// <summary>
        /// Imports a pre-saved xml list of file items into the list view's source
        /// </summary>
        private void Execute_Load(object param)
        {
            LoadList(param as string);
        }

        /// <summary>
        /// Starts the Compute checksum task
        /// </summary>
        /// <param name="obj"></param>
        private void Execute_Compute(object param)
        {
            ComputeAsync();
        }

        /// <summary>
        /// Cancels the Compute checksum task
        /// </summary>
        /// <param name="param"></param>
        private void Execute_Cancel(object param)
        {
            this.CancelSource.Cancel();
            CleanWorkEnvironment(false, false);
        }

        #endregion  // Commands.Execute

        #region Private helpers
        
        private void CheckGameDir()
        {
            if (Common.BaseDir_NWN == null)
                throw new ApplicationException("Unable to determine game dir");
        }

        /// <summary>
        /// At every period the indicators will be updated
        /// </summary>
        /// <param name="state"></param>
        private void OnTimerHit(object state)
        {
            lock (_timerLock)
            {
                if (!ComputeState)
                    StopTimer();
                UpdateIndicators();
            }
        }

        /// <summary>
        /// Restarts the timer
        /// </summary>
        private void RestartTimer()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "RestartTimer()");
            Common.LogEntry(M, "Restarting timer");
            Timer.Change(0, 250);
        }

        /// <summary>
        /// Stops the timer
        /// </summary>
        private void StopTimer()
        {
            Timer.Change(Timeout.Infinite, Timeout.Infinite);
            UpdateIndicators();
        }

        /// <summary>
        /// Updates the progress indicators based on current progress values
        /// </summary>
        private void UpdateIndicators()
        {
            if (!ComputeState)
                return;

            // force update Verify, Download and Install indicators
            base.OnPropertyChanged("OverallProgress");
        }

        /// <summary>
        /// Computes checksum for file items, waits till the computation is finished, saves the new definition file
        /// </summary>
        private void ComputeChecksumAndSaveResults()
        {
            try
            {
                string M = string.Format("{0}.{1}", GetType().Name, "ComputeChecksumAndSaveResults()");

                if (!this.Command_Compute.CanExecute(null))
                    throw new ApplicationException("Unable to start the Compute process!");

                this.Command_Compute.Execute(null);

                int hops = MAX_HOPS;
                while (ComputeState)
                {
                    Common.LogEntry(M, "Waiting for compute state");
                    Thread.Sleep(1000);

                    if (--hops <= 0)
                        throw new ApplicationException(string.Format("Checksum tool timed out after {0} seconds.", MAX_HOPS));
                }

                if (this.ComputeHadErrors)
                    throw new ApplicationException("There were errors during operation");

                if (!this.Command_Save.CanExecute(null))
                    throw new ApplicationException("Unable to start the Save process!");

                this.Command_Save.Execute(null);

                Common.LogEntry(M, "Waiting interupted by disabled compute state");
            }
            catch (Exception ex)
            {
                this.HandleException(ex);
                Environment.ExitCode = 1;
            }
        }

        /// <summary>
        /// Starts the process that computes the checksum hashes asynchronously
        /// </summary>
        private async void ComputeAsync()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "ComputeAsync()");
            Common.LogEntry(M, "Started");
            this.CleanWorkEnvironment(true, false);

            try
            {
                Common.LogEntry(M, "Building actions list from items...");
                var actions = new List<Action>(this.FileItems.Count);

                foreach (var fileItem in FileItems)
                    if (!fileItem.FileInfo.Exists)
                    {
                        this.CleanWorkEnvironment(false, false);
                        throw new ApplicationException(string.Format("File does not exist: {0}", fileItem.FileInfo.FullName));
                    }

                this.FileItems.ToList().ForEach(item => actions.Add(() =>
                    {
                        try
                        {
                            Common.ComputeChecksumWithUpdate(item, this.CancelToken);
                        }
                        catch (Exception ex)
                        {
                            this.CancelSource.Cancel();
                            HandleException(ex);
                        }
                    }));

                RestartTimer();

                Common.LogEntry(M, "Run actions async with await");
                await Common.RunTasksAsync(
                    actions,
                    Environment.ProcessorCount,
                    this.CancelToken
                ).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }            

            this.CleanWorkEnvironment(false, true);

            if (!Common.BatchMode)
                MessageBox.Show("Finished!", "Finished!", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Cleans the work environment
        /// * Reset main progress bar
        /// * Reset Status, HashValue and HasherType for each object in the FileItems collection
        /// * Renames the button back to "Compute"
        /// </summary>
        private void CleanWorkEnvironment(bool bComputeState, bool bFinished)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CleanEnvironment()");
            Common.LogEntry(M, "Cleaning environment (ComputeState: {0}, Finished: {1})", bComputeState, bFinished);

            if (!bFinished)
            {
                foreach (Model_FileItem fileItem in FileItems)
                {
                    fileItem.Checksum = new Data_Checksum();
                    fileItem.Status = 0;
                }

                //this.OverallProgress = 0;
                this.ComputeHadErrors = false;
            }

            if (bComputeState)
            {
                //  Dispose of the old and create a new instance of the CancellationTokenSource
                this.CancelSource.Dispose();
                this.CancelSource = new CancellationTokenSource();
            }

            this.ComputeState = bComputeState;
            base.OnPropertyChanged("OverallProgress");
        }

        /// <summary>
        /// Creates a list of file items from the source directory and returns the file path of the definition file found in the directory
        /// </summary>
        /// <param name="defDir"></param>
        private void LoadListFromDirectory()
        {
            if (this.DefinitionFileName == null)
                throw new ApplicationException("Missing definition file");

            DirectoryInfo defDir = new DirectoryInfo(Path.GetDirectoryName(DefinitionFileName));
            string[] defFiles = Directory.GetFiles(defDir.FullName, "*.*", SearchOption.AllDirectories);

            if (defFiles == null || defFiles.Length == 0)
                throw new ApplicationException("No source files found in " + defDir.FullName);

            List<Model_FileItem> fileItems = new List<Model_FileItem>();

            this.FileItems.Clear();
            foreach (var file in defFiles)
                if (Path.GetExtension(file).ToLower() != ".xml")
                    this.FileItems.Add(new Model_FileItem(new FileInfo(file), DefinitionFileName));
        }

        /// <summary>
        /// Prompts user to specify the path to a pre-saved il2_content.xml file
        /// Loads this file contents (FileItems collection) and displays in the ListView
        /// </summary>
        private void LoadList(string paramFile)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "LoadList()");
            Common.LogEntry(M, "Started with param: {0}", paramFile);
            CheckGameDir();

            try
            {
                Common.LogEntry(M, "Getting base dir parent");
                var baseDir = Common.BaseDir_NWN.Parent;
                Common.LogEntry(M, "Getting base dir parent: {0}", baseDir);

                FileInfo inXmlFile;

                if (paramFile != null)
                    inXmlFile = new FileInfo(paramFile);
                else
                    inXmlFile = Common.GetOpenFilePath("Please specify the input file path", "XML Files (*.xml)|*.xml", new FileInfo(baseDir.FullName));

                Common.LogEntry(M, "Using input definition file: {0} (isNull: {1}, Exists: {2})", inXmlFile, inXmlFile == null, inXmlFile == null ? false : File.Exists(inXmlFile.FullName));

                if (inXmlFile == null)
                    return;

                if (inXmlFile.Extension.ToLower() != ".xml")
                    throw new ApplicationException("Invalid definition file extension!");

                DefinitionFileName = inXmlFile.FullName;
                this.FileItems.Clear();
                
                if (!inXmlFile.Exists)
                    return;

                Common.LogEntry(M, "Loading file items from definition file...");
                Common.LoadListFromXML(
                    this.FileItems,
                    inXmlFile,
                    (fileItem) => fileItem.PropertyChanged += this.OnModelPropertyChanged);
            }

            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        /// <summary>
        /// Raises the PropertyChanged event for OverallProgress property if one of the FileItem objects' status changes
        /// </summary>
        private void OnModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == "Status")
            //    base.OnPropertyChanged("OverallProgress");
        }

        /// <summary>
        /// Exports files from list (see the event handler for details)
        /// </summary>
        private void SaveList()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "LoadList()");
            CheckGameDir();
  
            try
            {
                FileInfo outXmlFile = new FileInfo(DefinitionFileName);

                if (outXmlFile == null)
                    return;

                Common.RefreshSizesForFileItems(this.FileItems);
                Common.SaveListToXML(this.FileItems, outXmlFile);

                if (!Common.BatchMode)
                    MessageBox.Show("Definition file saved as:\n" + outXmlFile, "Definition file saved", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    Common.LogEntry(M, "Definition file saved as {0}", outXmlFile);
            }

            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        /// <summary>
        /// Opens the FileOpen dialog window
        /// Adds the selected file to the listview
        /// </summary>
        private void AddFiles(bool isSensitive)
        {
            CheckGameDir();

            try
            {
                //  specify multiple files
                List<FileInfo> ListFileInfo = Common.GetOpenFilePathMultiple("Specify a file for which to calculate MD5 checksum", "All files|*", new FileInfo(DefinitionFileName));

                //  cancel pressed - return
                if (ListFileInfo == null)
                    return;

                //  loop through the list of specified files
                foreach (FileInfo fileInfo in ListFileInfo)
                {
                    //  create a new instance of the FileItem object
                    Model_FileItem fileItem = new Model_FileItem(fileInfo, DefinitionFileName);

                    //  mark the file sensitive if chosen by user
                    fileItem.IsSensitive = isSensitive;

                    //  subscribe for changes in the model
                    fileItem.PropertyChanged += this.OnModelPropertyChanged;

                    //  add the above instance to the collection
                    this.FileItems.Add(fileItem);
                }

                //  select the item at index of the last item
                this.SelectedFileItemIndex = 0;//this.FileItems.Count - 1;
            }

            catch (Exception ex)
            {
                HandleException(ex);
                return;
            }
        }

        /// <summary>
        /// Removes the selected item(s) from the listview
        /// </summary>
        private void RemoveFiles(object param)
        {
            try
            {
                // get the ListView's SelectedItems collection bound via CommandParameter
                IList selectedFileItems = param as IList;

                int nIndex = this.SelectedFileItemIndex;

                //  copy the selected items to new array
                Model_FileItem[] arItems = new Model_FileItem[selectedFileItems.Count];
                selectedFileItems.CopyTo(arItems, 0);

                //  remove all items from the listview that are in the array above
                foreach (Model_FileItem fileItem in arItems)
                {
                    fileItem.PropertyChanged -= this.OnModelPropertyChanged;
                    this.FileItems.Remove(fileItem);
                }

                //  select the same row that was selected before deleting (select the last row if not exists)
                nIndex = nIndex > this.FileItems.Count - 1 ? this.FileItems.Count - 1 : nIndex;

                //  select the row at nIndex's index
                this.SelectedFileItemIndex = nIndex;

            }

            catch (Exception ex)
            {
                HandleException(ex);
                return;
            }
        }

        /// <summary>
        /// Displays message of the unhandled exception (includes inner exception if not null)
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "LoadList()");
            string sMessage = string.Empty;

            this.ComputeHadErrors = true;

            sMessage = string.Format("An error occured:\n{0}", ex.Message);

            if (ex.InnerException != null)
                sMessage += string.Format("\n\nFurther details:\n{0}\n{1}", ex.InnerException.Message, Common.FormatStackTrace(ex.InnerException.StackTrace));

            sMessage += string.Format("\n{0}", Common.FormatStackTrace(ex.StackTrace));

            if (!Common.BatchMode)
                MessageBox.Show(sMessage, "An error occured!", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                Common.LogEntry(M, "Error occured: {0}", sMessage);
        }

        #endregion  // Private helpers

        #region Event handlers

        /// <summary>
        /// Raised when the FileItems collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFileItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            base.OnPropertyChanged("OverallProgress_Max");
        }

        #endregion

        #endregion  // Methods

        #region Constructors

        /// <summary>
        /// Creates a new ViewModel for the view called "Checksum tool"
        /// </summary>
        public ViewModel_ToolChecksum()
        {
            string M = string.Format("{0}.{1}", GetType().Name, ".ctor()");
            Common.LogEntry(M, "Starting");
            this.CleanWorkEnvironment(false, false);
            this.FileItems.CollectionChanged += OnFileItemsChanged;
        }

        /// <summary>
        /// Creates a new ViewModel for the view called "Checksum tool" and opens the definition file
        /// </summary>
        public ViewModel_ToolChecksum(string defFile)
            : this()
        {
            string M = string.Format("{0}.{1}", GetType().Name, ".ctor()");
            Common.LogEntry(M, "Starting with def file: {0}", defFile);

            if (!this.Command_Load.CanExecute(null))
                throw new ApplicationException("Unable to start loading the definition file: " + defFile);

            this.DefinitionFileName = defFile; // ignore load, just remember path
            //this.Command_Load.Execute(defFile);
        }

        /// <summary>
        /// Creates a new ViewModel for the view called "Checksum tool", opens the definition file,
        /// computes the checksum for all file items in the definition file and saves it once finished
        /// </summary>
        public ViewModel_ToolChecksum(string defFile, bool compute)
            : this(defFile)
        {
            string M = string.Format("{0}.{1}", GetType().Name, ".ctor()");
            Common.LogEntry(M, "Starting with compute: {0} and def file: {1}", compute, defFile);

            LoadListFromDirectory();
            ComputeChecksumAndSaveResults();
        }
        
        #endregion

        #region IDataErrorInfo.Properties

        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
