﻿
namespace il2_clienttools.View
{
    #region Namespaces

    using il2_corelib.Controls;
    using System;
    using System.Linq;
    using System.Windows;

    #endregion

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class View_Launcher : BorderlessWindow
    {
        public View_Launcher()
        {
            InitializeComponent();
        }

    }
}
