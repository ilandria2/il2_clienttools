﻿
namespace il2_launcher
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.View;
    using il2_clienttools.ViewModel;
    using il2_corelib.Controls;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    #endregion

    public partial class App : Application
    {
        #region Event handles

        /// <summary>
        /// Application entry point
        /// </summary>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            this.OnLauncherStartup(e);
        }

        /// <summary>
        /// Application exit point
        /// </summary>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            this.OnLauncherExit(e);
        }

        /// <summary>
        /// Handle unhandled exception from all threads in current domain
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ExceptionHandler.HandleException(e.ExceptionObject as Exception);
        }

        /// <summary>
        /// Handle Main UI dispatcher's thread exception
        /// </summary>
        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionHandler.HandleException(e.Exception);
            e.Handled = true;
        }

        /// <summary>
        /// Handle AppDomain's task scheduler async exception
        /// </summary>
        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            ExceptionHandler.HandleException(e.Exception);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Handles the application OnExit event
        /// </summary>
        private void OnLauncherExit(ExitEventArgs e)
        {
            try
            {
                /*
                if (Common.ClientSmf != null)
                {
                    Common.ClientSmf.CancelAsync();
                    Common.ClientSmf.Dispose();
                }

                if (Common.ClientGame != null)
                {
                    Common.ClientGame.CancelAsync();
                    Common.ClientGame.Dispose();
                }

                if (Common.ClientGameBinary != null)
                {
                    Common.ClientGameBinary.CancelAsync();
                    Common.ClientGameBinary.Dispose();
                }
                */
            }
            catch { }
        }

        /// <summary>
        /// Handles the application OnStartup event
        /// </summary>
        private void OnLauncherStartup(StartupEventArgs e)
        {
            //  catch unhandled exception
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;


            if (!this.IsOneTimeLaunch())
            {
                Notificator.Show("Already open", "The launcher application is already open.");
                Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
                Application.Current.Shutdown(0);
                return;
            }

            else
            {
                if (!Debugger.IsAttached)
                {
                    //  prevent running the launcher before updater checks and installs the updates
                    if (Common.AppArgs.Length == 1)
                    {
                        Common.QuitAndStartProcess("il2_updater.exe");
                        return;
                    }
                }

                //  check base dir
                if (Common.GetBaseDir(Common.RegistryBaseDirType.BaseDir_NeverwinterNights) == null)
                    throw new InvalidOperationException(Common.ERROR_INVALID_BASEDIR);

                // enable debug logging
                SetDebugLogging(true);

                //  show the login form
                Common.InitView(typeof(View_Login), typeof(ViewModel_Login), 2, true, true);
            }
        }

        /// <summary>
        /// Sets debug logging for the application
        /// </summary>
        /// <param name="logging"></param>
        private void SetDebugLogging(bool logging)
        {
            try
            {
                Common.DebugLoggingEnabled = logging;
            }
            catch
            {
                Notificator.Show("Debug logging", "Unable to set value of debug logging!");
            }
        }

        #endregion

    }


}
