﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Data;
    using il2_clienttools.EventHandlers;
    using il2_clienttools.Helper;
    using il2_clienttools.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Windows.Input;

    #endregion

    /// <summary>
    /// The "Game" tab view's view model
    /// </summary>
    public class ViewModel_MainGame : ViewModel_MainTab
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MainGame tab VM
        /// </summary>
        public ViewModel_MainGame()
        {
            Common.DebugLoggingEnabled = true;
            LoadExistingCharactersAsync();
        }

        #endregion

        #region Fields

        private ObservableCollection<Model_Character> _characters;
        private Model_Character _selectedCharacter;
        private int _selectedCharacterIndex;
        private ViewModel_Character _viewModel_SelectedCharacter;

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets the list of Characters
        /// </summary>
        public ObservableCollection<Model_Character> Characters
        {
            get 
            {
                if (this._characters == null)
                    this._characters = new ObservableCollection<Model_Character>();

                return this._characters; 
            }
        }

        /// <summary>
        /// Indicates whether the player has some characters
        /// </summary>
        public bool HasCharacters 
        {
            get { return Characters.Count > 0; }
        }

        /// <summary>
        /// Specifies the selected item from the character list
        /// </summary>
        public Model_Character SelectedCharacter
        {
            get { return _selectedCharacter; }
            set 
            {
                if (this._selectedCharacter == value) return;

                this._selectedCharacter = value;
                this.ViewModel_SelectedCharacter.Character = _selectedCharacter;

                base.OnPropertyChanged("SelectedCharacter");
                base.OnPropertyChanged("SelectedCharacterIndex");
                base.OnPropertyChanged("HasCharacters");
            }
        }

        /// <summary>
        /// Specifies the selected item index of the character list
        /// </summary>
        public int SelectedCharacterIndex
        {
            get { return _selectedCharacterIndex; }
            set 
            {
                if (this._selectedCharacterIndex == value) return;

                this._selectedCharacterIndex = value;

                base.OnPropertyChanged("SelectedCharacterIndex");
                base.OnPropertyChanged("SelectedCharacter");
                base.OnPropertyChanged("HasCharacters");
            }
        }

        /// <summary>
        /// Gets the View model for the selected character
        /// </summary>
        public ViewModel_Character ViewModel_SelectedCharacter
        {
            get 
            {
                if (_viewModel_SelectedCharacter == null)
                    _viewModel_SelectedCharacter = new ViewModel_Character(null, SelectedCharacter);

                return _viewModel_SelectedCharacter; 
            }
        }

        /// <summary>
        /// Returns true if the user is allowed to create a new player character
        /// </summary>
        public bool AllowedToCreateNewCharacter 
        {
            get
            {
                foreach (var character in Characters)
                    if (!character.IsDead) 
                        return false;

                return true;
            }
        }

        #endregion

        #region Properties.Overrides

        /// <summary>
        /// Gets a name of the view model
        /// </summary>
        public override string DisplayName
        {
            get { return "Game"; }
        }

        #endregion

        #region Commands.Fields

        private ICommand _command_New;
        private ICommand _command_Cancel;
        private ICommand _command_Play;
        private ICommand _command_Create;

        #endregion

        #region Commands.Properties

        /// <summary>
        /// Gets the instance of the Create character command
        /// </summary>
        public ICommand Command_Create
        {
            get
            {
                if (_command_Create == null)
                    _command_Create = new RelayCommand(Execute_Create, CanExecute_Create);

                return _command_Create;
            }
        }

        /// <summary>
        /// Gets the instance of the New character command
        /// </summary>
        public ICommand Command_New
        {
            get
            {
                if (_command_New == null)
                    _command_New = new RelayCommand(Execute_New, CanExecute_New);

                return _command_New;
            }
        }

        /// <summary>
        /// Gets the instance of the Play command
        /// </summary>
        public ICommand Command_Play
        {
            get
            {
                if (_command_Play == null)
                    _command_Play = new RelayCommand(Execute_Play, CanExecute_Play);

                return _command_Play;
            }
        }

        /// <summary>
        /// Gets the instance of the Cancel command
        /// </summary>
        public ICommand Command_Cancel
        {
            get
            {
                if (_command_Cancel == null)
                    _command_Cancel = new RelayCommand(Execute_Cancel, CanExecute_Cancel);

                return _command_Cancel;
            }
        }

        #endregion

        #region Commands.CanExecute

        /// <summary>
        /// Returns true if the Create command can be executed
        /// </summary>
        private bool CanExecute_Create(object obj)
        {
            return ViewModel_SelectedCharacter.CreationMode;
        }

        /// <summary>
        /// Returns true if the New command can be executed
        /// </summary>
        private bool CanExecute_New(object obj)
        {
            return !ViewModel_SelectedCharacter.CreationMode;
        }

        /// <summary>
        /// Returns true if the Play command can be executed
        /// </summary>
        private bool CanExecute_Play(object obj)
        {
            return 
                !ViewModel_SelectedCharacter.CreationMode && 
                SelectedCharacter != null && 
                !ViewModel_Installer.Instance.CheckingForUpdates &&
                !ViewModel_Installer.Instance.ErrorsDuringUpdate;
        }

        /// <summary>
        /// Returns true if the Cancel command can be executed
        /// </summary>
        private bool CanExecute_Cancel(object obj)
        {
            return ViewModel_SelectedCharacter.CreationMode;
        }

        #endregion

        #region Commands.Execute

        /// <summary>
        /// Handles the Execute event of the Create command
        /// </summary>
        private void Execute_Create(object obj)
        {
            ExportSelectedCharacter();
        }

        /// <summary>
        /// Handles the Execute event of the New command
        /// </summary>
        private void Execute_New(object obj)
        {
            Characters.Add(CreateNewCharacter());
            SelectedCharacterIndex = Characters.Count - 1;
            ViewModel_SelectedCharacter.CreationMode = true;
        }

        /// <summary>
        /// Handles the Execute event of the Play command
        /// </summary>
        private void Execute_Play(object obj)
        {
            AttemptToPlaySelectedCharacterAsync();
        }

        private void AttemptToPlaySelectedCharacterAsync()
        {
            try
            {
                string msgPlayCharacter = CompressMessage(Common.User.UserName, ViewModel_SelectedCharacter.Character.PlayerID, Common.EnvironmentType, Common.MESSAGE_TYPE_PLAY_CHARACTER, 0, -1, string.Empty, string.Empty);
                var messageSave = Common.MessageClient(Common.User.UserName, -1, Common.EnvironmentType, Common.MESSAGE_TYPE_SAVE_MESSAGE, 0, -1, msgPlayCharacter);

                if (!Common.PostMessageAsync(Common.URL_GAME_CLIENT, messageSave, CharacterCreatorMessanger_Completed))
                    throw new WebException();
            }
            catch (WebException)
            {
                Notificator.Error("Network error", Common.MESSAGE_NETWORK_ERROR);
            }
        }

        private void ConnectToServer(bool envType)
        {
            string suffixPort = string.Format(":{0}", envType ? "5122" : string.Empty);
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo(Common.BaseDir_NWN.FullName + "\\nwmain.exe");
            process.StartInfo.Arguments = "+connect server.ilandria.com" + suffixPort;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.WorkingDirectory = Common.BaseDir_NWN.FullName;
            process.Start();

            // wait until the process exits
            process.WaitForExit();

            // check whether override needs to be restored and perform it if so
            ViewModel_Installer.Instance.OverrideRestore();
        }

        /// <summary>
        /// Handles the Execute event of the Cancel command
        /// </summary>
        private void Execute_Cancel(object obj)
        {
            CancelCharacterCreation();
        }

        #endregion

        #region Methods.EventHandlers

        /// <summary>
        /// Handles the UploadDataCompleted game client event
        /// </summary>
        private void CharacterCreatorMessanger_Completed(object sender, MessageResponseEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CharacterCreatorMessanger_Completed");
            Common.LogEntry(M, "Start with sender={0}, evArgs={1}", sender, e);

            if (e.Response == null || !e.Response.HasValue)
            {
                Common.LogEntry(M, "Missing messanger response");
                throw new ApplicationException("Missing messanger response");
            }

            Data_MessageResponse response = e.Response.Value;
            Common.LogEntry(M, "Got response: {0}", response);

            if (response.ExceptionObject != null)
            {
                Common.LogEntry(M, "Got exception object from response");

                if (response.ExceptionObject is OperationCanceledException)
                    Notificator.Show("Cancelled!", "Request was cancelled!");
                else
                {
                    Notificator.Error("Error occured!", string.Format("{0}\n{1}", "Character creation failed:", response.ExceptionObject.Message));
                    CancelCharacterCreation();
                }

                Common.LogEntry(M, "Setting IsBusy = {0}", false);
                Common.MainViewModel.IsBusy = false;
            }
            else
            {
                Common.LogEntry(M, "Calling HandleCharacterCreatorMessageResponse({0}, {1})", response, e.UserState);
                HandleCharacterCreatorMessageResponse(response, e.UserState);
            }

            Common.LogEntry(M, "Finished");
        }

        #endregion

        #region Methods.Helpers

        /// <summary>
        /// Posts web client message to return a list of existing player characters
        /// </summary>
        private void LoadExistingCharactersAsync(bool selectLast = false)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "LoadExistingCharactersAsync");
            Common.LogEntry(M, "Start with selectLast={0}", selectLast);

            Common.LogEntry(M, "Setting IsBusy={0}", true);
            Common.MainViewModel.IsBusy = true;

            Common.LogEntry(M, "Creating client message");
            var msgLoadChars = Common.MessageClient(Common.User.UserName, -1, Common.EnvironmentType, Common.MESSAGE_TYPE_LOAD_CHARACTERS, 0, -1, string.Empty);

            Common.LogEntry(M, "Posting client message");
            Common.PostMessageAsync(Common.URL_GAME_CLIENT, msgLoadChars, CharacterCreatorMessanger_Completed, null, selectLast);
        }

        /// <summary>
        /// finishes the character creation after receiving results from the game client
        /// </summary>
        /// <param name="responseData"></param>
        private bool HandleCharacterCreatorMessageResponse(Data_MessageResponse responseData, object userState)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "HandleCharacterCreatorMessageResponse");
            Common.LogEntry(M, "Start with responseData={0}, userState={1}", responseData, userState);

            if (responseData.ClientType != Common.CLIENT_TYPE_GAME)
            {
                Common.LogEntry(M, "Response is not from GameClient - return false");
                return false;
            }

            int responseId = 0;
            int.TryParse(responseData.Data, out responseId);

            Common.LogEntry(M, "Tried to parse responeId={0}", responseId);

            #region MessageType: Get New Player ID

            if (responseData.Type == Common.MESSAGE_TYPE_NEW_PLAYER_ID)
            {
                Common.LogEntry(M, "Handling message type NEW_PLAYER_ID");

                if (responseId == Common.RESPONSE_TYPE_NEW_PLAYER_ID_ERROR)
                {
                    Common.LogEntry(M, "ResponseId is error - showing error.");
                    Notificator.Error("Error occured!", "Unable to obtain new player ID");

                    Common.LogEntry(M, "ResponseId is error - Cancelling character creation");
                    CancelCharacterCreation();

                    Common.LogEntry(M, "ResponseId is error - returning false");
                    return false;
                }

                if (responseId < Common.PLAYER_ID_BASE)
                {
                    Common.LogEntry(M, "ResponseId < PLAYER_ID_BASE - showing error");
                    Notificator.Error("Error occured!", "Invalid player ID received");

                    Common.LogEntry(M, "ResponseId < PLAYER_ID_BASE - cancelling character creation");
                    CancelCharacterCreation();

                    Common.LogEntry(M, "ResponseId < PLAYER_ID_BASE - returning false");
                    return false;
                }

                var user = Common.User;
                var character = ViewModel_SelectedCharacter.Character;

                Common.LogEntry(M, "Setting PlayerID {0} on character", responseId);
                character.PlayerID = responseId;

                Common.LogEntry(M, "Creating client message...");
                var data = Common.MessageClient(user.UserName, responseId, Common.EnvironmentType, Common.MESSAGE_TYPE_CREATE_CHARACTER, 0, -1, string.Empty);

                Common.LogEntry(M, "Posting client message");
                if (!Common.PostMessageAsync(Common.URL_GAME_CLIENT, data, character.RawData, CharacterCreatorMessanger_Completed))
                {
                    Common.LogEntry(M, "Throwing web exception");
                    throw new WebException();
                }
            }

            #endregion

            #region MessageType: Create character
            
            else if (responseData.Type == Common.MESSAGE_TYPE_CREATE_CHARACTER)
            {
                Common.LogEntry(M, "Handling message type CREATE_CHARACTER");

                if (responseId == Common.RESPONSE_TYPE_CREATE_CHARACTER_ERROR)
                {
                    Common.LogEntry(M, "Error - showing");
                    Notificator.Error("Error occured!", "Character creation failed");

                    Common.LogEntry(M, "Error - Canceling character creation");
                    CancelCharacterCreation();

                    Common.LogEntry(M, "Error - Returning false");
                    return false;
                }

                Common.LogEntry(M, "Calling WaitAndKeepCheckingMessageForPlayer");
                WaitAndKeepCheckingMessageForPlayer(Common.User.UserName, responseData.RequestorId, Common.EnvironmentType, Common.MESSAGE_TYPE_CREATE_CHARACTER, 0, responseData.RawDataId, string.Empty, string.Empty);
            }

            #endregion

            #region MessageType: Load characters

            else if (responseData.Type == Common.MESSAGE_TYPE_LOAD_CHARACTERS)
            {
                Common.LogEntry(M, "Handling message type LOAD_CHARACTERS");

                if (responseId == Common.RESPONSE_TYPE_LOAD_CHARACTERS_ERROR)
                {
                    Common.LogEntry(M, "Error - showing");
                    Notificator.Error("Error occured!", "Couldn't load your player characters!");

                    Common.LogEntry(M, "Returning {0}", false);
                    return false;
                }

                try
                {
                    Common.LogEntry(M, "Calling ParseCharacters({0})", responseData.Data);

                    // parse string data and generate a new list
                    var existingCharacters = ParseCharacters(responseData.Data);

                    if (existingCharacters.Count == 0)
                    {
                        Common.LogEntry(M, "No characters parsed - return {0}", true);
                        return true;
                    }

                    // if we have at least one then select the last
                    bool selectLast = existingCharacters.Count > 0 && userState != null && userState is bool && (bool)userState;
                    Common.LogEntry(M, "selectLast = {0}", selectLast);

                    // perform this action on UI thread because we might be in parallel non-UI thread
                    Common.LogEntry(M, "CallOnUI FinishLoadingCharacters({0}, {1})", existingCharacters, selectLast);
                    Common.DoActionOnUi(() => FinishLoadingCharacters(existingCharacters, selectLast));
                }
                catch
                {
                    Common.LogEntry(M, "Error occured - showing and returning false");
                    Notificator.Error("Error occured!", "Couldn't load your player characters!");
                    return false;
                }
                finally
                {
                    Common.LogEntry(M, "Setting IsBusy to {0}", false);
                    Common.MainViewModel.IsBusy = false;
                }
            }

            #endregion

            #region MessageType: Check message

            else if (responseData.Type == Common.MESSAGE_TYPE_CHECK_MESSAGE)
            {
                Common.LogEntry(M, "Handling message type CHECK_MESSAGE");

                #region Parse specific message data from common response

                string[] mergedParams = responseData.Parameter.Split(new char[] { '|' });
                Common.LogEntry(M, "Got merged params: {0}", mergedParams);

                int type = int.Parse(mergedParams[3]);
                int subType = int.Parse(mergedParams[4]);
                string requestor = mergedParams[0];
                int requestorId = int.Parse(mergedParams[1]);
                bool envType = bool.Parse(mergedParams[2]);
                int rawDataId = int.Parse(mergedParams[5]);
                string param = mergedParams[6];
                string status = mergedParams[7];

                int attempts = 0;
                if (userState != null && userState is int)
                    attempts = (int)userState;
                attempts++;

                Common.LogEntry(M, "Parsed parameters: type={0}, subType={1}, req={2}, reqId={3}, env={4}, rawId={5}, param={6}, status={7}, attempts={8}",
                    type, subType, requestor, requestorId, envType, rawDataId, param, status, attempts);
                
                #endregion

                #region Original Message: Create character

                if (type == Common.MESSAGE_TYPE_CREATE_CHARACTER)
                {
                    Common.LogEntry(M, "Message of type CREATE_CHARACTER");
                    if (responseId == Common.RESPONSE_TYPE_CHECK_MESSAGE_ERROR
                        || responseData.Data == "ERROR")
                    {
                        Common.LogEntry(M, "Error - showing");
                        Notificator.Error("Error occured!", "Character creation failed");

                        Common.LogEntry(M, "Error - cancelling character creation");
                        CancelCharacterCreation();

                        Common.LogEntry(M, "Error - returning {0}", false);
                        return false;
                    }

                    if (responseData.Data == "COMPLETED")
                    {
                        Common.LogEntry(M, "Completed - exporting to local folder");
                        Common.LogEntry(M, "exporting to local folder");
                        ViewModel_SelectedCharacter.Character.Export(Path.Combine(Common.BaseDir_NWN.FullName, "servervault", Common.User.UserName));

                        Common.LogEntry(M, "Calling LoadExistingCharactersAsync({0})", true);
                        LoadExistingCharactersAsync(true);

                        Common.LogEntry(M, "Returning {0}", true);
                        return true;
                    }
                    else
                    {
                        if (attempts > Common.MESSAGE_TYPE_CHECK_MESSAGE_ATTEMPTS)
                        {
                            Common.LogEntry(M, "Time out - showing error");
                            Notificator.Error("Timed out", "Server was unable to process the request within the specified timeframe.");

                            Common.LogEntry(M, "Time out - canceling character creation");
                            CancelCharacterCreation();

                            Common.LogEntry(M, "Time out - returning {0}", false);
                            return false;
                        }
                    }
                }

                #endregion

                #region Original Message: Play character

                else if (type == Common.MESSAGE_TYPE_PLAY_CHARACTER)
                {
                    Common.LogEntry(M, "Message of type PLAY_CHARACTER");

                    if (responseId == Common.RESPONSE_TYPE_CHECK_MESSAGE_ERROR
                        || responseData.Data == "ERROR")
                    {
                        Common.LogEntry(M, "Error - setting IsBusy to {0}", false);
                        Common.MainViewModel.IsBusy = false;

                        Common.LogEntry(M, "Error - showing");
                        Notificator.Error("Error occured!", "Character creation failed");

                        Common.LogEntry(M, "Error - returning {0}", false);
                        return false;
                    }

                    if (responseData.Data == "COMPLETED")
                    {
                        Common.LogEntry(M, "Completed - calling CheckForUpdatesAndPlay()");
                        CheckForUpdatesAndPlay();

                        Common.LogEntry(M, "Completed - returning {0}", true);
                        return true;
                    }
                    else
                    {
                        if (attempts > Common.MESSAGE_TYPE_CHECK_MESSAGE_ATTEMPTS)
                        {
                            Common.LogEntry(M, "Time out - setting IsBusy = {0}", false);
                            Common.MainViewModel.IsBusy = false;

                            Common.LogEntry(M, "Time out - showing");
                            Notificator.Error("Timed out", "Server was unable to process the request within the specified timeframe.");

                            Common.LogEntry(M, "Time out - returning", false);
                            return false;
                        }
                    }
                }

                #endregion

                Common.LogEntry(M, "Calling WaitAndKeepCheckingMessageForPlayer(...)");
                WaitAndKeepCheckingMessageForPlayer(requestor, requestorId, envType, type, subType, rawDataId, param, status, attempts);

                Common.LogEntry(M, "Returning {0}", true);
                return true;
            }

            #endregion

            #region MessageType: Save message

            else if (responseData.Type == Common.MESSAGE_TYPE_SAVE_MESSAGE)
            {
                Common.LogEntry(M, "Handling message type SAVE_MESSAGE");

                #region Parse specific message data from common response

                string[] mergedParams = responseData.Parameter.Split(new char[] { '|' });
                Common.LogEntry(M, "Got merged params: {0}", mergedParams);
                int type = int.Parse(mergedParams[3]);
                int subType = int.Parse(mergedParams[4]);
                string requestor = mergedParams[0];
                int requestorId = int.Parse(mergedParams[1]);
                bool envType = bool.Parse(mergedParams[2]);
                int rawDataId = int.Parse(mergedParams[5]);
                string param = mergedParams[6];
                string status = mergedParams[7];

                int attempts = 0;
                if (userState != null && userState is int)
                    attempts = (int)userState;
                attempts++;

                Common.LogEntry(M, "Parsed parameters: type={0}, subType={1}, req={2}, reqId={3}, env={4}, rawId={5}, param={6}, status={7}, attempts={8}",
                    type, subType, requestor, requestorId, envType, rawDataId, param, status, attempts);

                #endregion

                if (responseId == Common.RESPONSE_TYPE_SAVE_MESSAGE_ERROR
                    || responseData.Data == "ERROR")
                {
                    Common.LogEntry(M, "Error - showing");
                    Notificator.Error("Error occured!", "Character creation failed");

                    Common.LogEntry(M, "Returning {0}", false);
                    return false;
                }

                if (responseData.Data == "1")
                {
                    Common.LogEntry(M, "Response success - calling WaitAndKeepCheckingMessageForPlayer(...)");
                    WaitAndKeepCheckingMessageForPlayer(requestor, requestorId, envType, type, subType, rawDataId, param, status, attempts);
                }

                Common.LogEntry(M, "Returning {0}", true);
                return true;
            }

            #endregion

            Common.LogEntry(M, "Returning else true");
            return true;
        }

        /// <summary>
        /// Performs the following actions:
        /// - exits the character creation mode
        /// - clears the current list of loaded characters
        /// - loads the list of existing characters
        /// - optionally selects the last character
        /// </summary>
        /// <param name="existingCharacters"/>
        /// <param name="selectLast"/>
        private void FinishLoadingCharacters(List<Model_Character> existingCharacters, bool selectLast)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "FinishLoadingCharacters");
            Common.LogEntry(M, "Start with existingCharacters={0}, selectLast={1}", existingCharacters, selectLast);

            // disable creation mode on selected character
            if (ViewModel_SelectedCharacter.CharacterIsNotNull)
            {
                Common.LogEntry(M, "Setting creation mode to {0}", false);
                ViewModel_SelectedCharacter.CreationMode = false;
            }

            // clear old list
            Common.LogEntry(M, "Clearing old list of characters");
            Characters.Clear();

            // add characters
            Common.LogEntry(M, "Adding characters");
            foreach (var existingChar in existingCharacters)
                Characters.Add(existingChar);

            // select last character if predefined
            if (selectLast)
            {
                Common.LogEntry(M, "Selecting last character");
                SelectedCharacterIndex = Characters.Count - 1;
            }

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Creates a message request string formatted as parameters delimited by |
        /// </summary>
        /// <param name="user"></param>
        /// <param name="requestorId"></param>
        /// <param name="envType"></param>
        /// <param name="type"></param>
        /// <param name="subType"></param>
        /// <param name="rawDataId"></param>
        /// <param name="param"></param>
        /// <param name="status"></param>
        private string CompressMessage(string user, int requestorId, bool envType, int type, int subType, int rawDataId, string param, string status)
        {
            string mParam = string.Format(
                "{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                user, requestorId, envType, type, subType, rawDataId, string.Format("{0}|{1}", param, status));

            return mParam;
        }

        /// <summary>
        /// Posts web client message to check the status of an existing web client message
        /// </summary>
        /// <param name="requestorId"/>
        private void WaitAndKeepCheckingMessageForPlayer(string user, int requestorId, bool envType, int type, int subType, int rawDataId, string param, string status, int attempts = 0)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "WaitAndKeepCheckingMessageForPlayer");
            Common.LogEntry(M, 
                "Start user={0}, reqId={1}, envType={2}, type={3}, subType={4}, rawId={5}, param={6}, status={7}, attempts={8}", 
                user, requestorId, envType, type, subType, rawDataId, param, status, attempts);

            Common.LogEntry(M, "Setting IsBusy={0}", true);
            Common.MainViewModel.IsBusy = true;

            Common.LogEntry(M, "Queing workItem...");
            ThreadPool.QueueUserWorkItem((state) =>
                {
                    string ML = string.Format("{0}.{1}", M, "QueuedWorkItem");
                    Common.LogEntry(ML, "Started");

                    for (int sec = 0; sec < Common.MESSAGE_TYPE_CHECK_MESSAGE_SLEEP_SECONDS; sec++)
                    {
                        Common.LogEntry(ML, "Sleeping for 1 second and increasing BusyProgress");
                        Thread.Sleep(1000);
                        Common.MainViewModel.BusyProgress += 6.6667f; // max100 / 15 seconds (crontab job schedule)
                    }

                    Common.LogEntry(ML, "Compressing message");
                    string mParam = CompressMessage(user, requestorId, envType, type, subType, rawDataId, param, status);
                    Common.LogEntry(ML, "Compressed message = {0}", mParam);

                    Common.LogEntry(ML, "Creating client message");
                    var messageCheck = Common.MessageClient(Common.User.UserName, -1, Common.EnvironmentType, Common.MESSAGE_TYPE_CHECK_MESSAGE, 0, -1, mParam);

                    Common.LogEntry(ML, "Posting client message...");
                    Common.PostMessageAsync(Common.URL_GAME_CLIENT, messageCheck, CharacterCreatorMessanger_Completed, null, attempts);
                }, null);
        }

        /// <summary>
        /// Parses initializes a list of new character models from messagenr's response data string
        /// </summary>
        /// <param name="responseData"></param>
        private List<Model_Character> ParseCharacters(string responseData)
        {
            #region Decode and clean data

            byte[] tableData = Convert.FromBase64String(responseData);
            string table = Encoding.ASCII.GetString(tableData);
            table = table
                .Replace("</td>", string.Empty)
                .Replace("</tr>", string.Empty)
                .Replace("</table>", string.Empty)
                .Replace("<table>", string.Empty);

            #endregion

            string[] tableRows = table.Split(new string[] { "<tr>" }, StringSplitOptions.RemoveEmptyEntries);
            var existingCharacters = new List<Model_Character>();

            foreach (string row in tableRows)
            {
                string[] columns = row.Split(new string[] { "<td>" }, StringSplitOptions.None);

                #region Validate columns

                if (columns == null)
                    throw new ApplicationException("Unable to parse column data from character list!");

                if (columns.Length != Common.RESPONSE_TYPE_LOAD_CHARACTERS_COLUMN_COUNT + 1)
                    throw new ApplicationException(string.Format("Got {0} columns; Expected {1}", columns.Length, Common.RESPONSE_TYPE_LOAD_CHARACTERS_COLUMN_COUNT));

                #endregion

                #region Parse and set identity fields

                //PlayerID, PlayerNAME, RegisteredOn, CharHeadType, CharSkinColor, CharHairColor, CharGender, CharDescription, CharHitPoints
                var existingChar = new Model_Character();
                DateTime registeredOn = default(DateTime);
                DateTime.TryParse(columns[3], out registeredOn);

                existingChar.PrivateName = columns[2];
                existingChar.PlayerID = int.Parse(columns[1]);
                existingChar.RegisteredOn = registeredOn;

                #endregion

                #region Parse and set in-game fields
                
                // player was at least once in the game
                if (registeredOn != default(DateTime))
                {
                    byte charHeadType = byte.Parse(columns[4]);
                    byte charSkinColor = byte.Parse(columns[5]);
                    byte charHairColor = byte.Parse(columns[6]);
                    byte charGender = byte.Parse(columns[7]);
                    string charDescription = columns[8];
                    int charHitPoints = int.Parse(columns[9]);

                    existingChar.Gender = (Gender)charGender;
                    existingChar.HeadType = charHeadType;
                    existingChar.SkinType = charSkinColor;
                    existingChar.HairType = charHairColor;
                    existingChar.Description = charDescription;
                    existingChar.CurrentHitPoints = charHitPoints;
                }
            
                #endregion

                existingCharacters.Add(existingChar);
            }
            return existingCharacters;
        }

        /// <summary>
        /// Performs the final Check for updates and immediately connects to the game server using the selected character
        /// (already hoping character was copied on the server)
        /// </summary>
        private void CheckForUpdatesAndPlay()
        {
            // disable busy pop up
            Common.MainViewModel.IsBusy = false;

            // check for updates
            var installer = ViewModel_Installer.Instance;
            if (!installer.CheckingForUpdates)
                installer.CheckForUpdates();

            // check if update still needed
            if (installer.ErrorsDuringUpdate ||
                installer.UpdateNeeded)
            {
                Notificator.Error("Update still needed!", "Unable to update local content");
                return;
            }

            // check whether override needs to be backuped and perform backup if so
            installer.OverrideBackup();

            InstallOverrideAndDialog();

            // connect to game server
            ConnectToServer(Common.EnvironmentType);
        }

        private void InstallOverrideAndDialog()
        {
            string dirOverride = Path.Combine(Common.BaseDir_NWN.FullName, "override");

            if (Directory.Exists(dirOverride))
            {
                Directory.Delete(dirOverride, true);
                Directory.CreateDirectory(dirOverride);
            }

            string file = Path.Combine(Common.BaseDir_NWN.FullName, "il2_override.zip");
            Common.DecompressFile(Common.BaseDir_NWN.FullName, file);

            file = Path.Combine(Common.BaseDir_NWN.FullName, "il2_dialog.zip");
            string tlkFile = Path.Combine(Common.BaseDir_NWN.FullName, "dialog.tlk");

            if (File.Exists(tlkFile))
                File.Delete(tlkFile);

            Common.DecompressFile(Common.BaseDir_NWN.FullName, file);
        }

        /// <summary>
        /// Asks user to confirm the input and then attempts to export the character in creation mode
        /// </summary>
        private void ExportSelectedCharacter()
        {
            if (ViewModel_SelectedCharacter.Character == null) 
                return;
            
            AttemptCreateCharacterForUserAsync(Common.User, ViewModel_SelectedCharacter.Character);
        }

        /// <summary>
        /// Attempts to send a client message to create a new character on game server
        /// </summary>
        /// <param name="user"></param>
        /// <param name="character"></param>
        /// <returns>true on success</returns>
        private bool AttemptCreateCharacterForUserAsync(Model_User user, Model_Character character)
        {
            try
            {
                var msgNewPlayerId = Common.MessageClient(user.UserName, -1, Common.EnvironmentType, Common.MESSAGE_TYPE_NEW_PLAYER_ID, 0, -1, character.DisplayName);

                if (!Common.PostMessageAsync(Common.URL_GAME_CLIENT, msgNewPlayerId, CharacterCreatorMessanger_Completed))
                    throw new WebException();
                return true;
            }
            catch (WebException)
            {
                Notificator.Error("Network error", Common.MESSAGE_NETWORK_ERROR);
            }

            return false;
        }

        /// <summary>
        /// Creates a new character instance with a specific list of abilities bound to quickbar slots
        /// </summary>
        private Model_Character CreateNewCharacter()
        {
            var quickSlots = new Dictionary<int, int>();
            var feats = new[] { Common.FEAT_TARGET, Common.FEAT_RUN_MODE, Common.FEAT_INTERACT };

            quickSlots.Add(0, feats[0]);
            quickSlots.Add(1, feats[1]);
            quickSlots.Add(2, feats[2]);

            var character = new Model_Character(Common.PLAYER_ID_NULL, quickSlots, feats);

            character.CurrentHitPoints = character.MaxHitPoints;
            character.CreationMode = true;

            return character;
        }

        /// <summary>
        /// Cancels the character creation mode
        /// </summary>
        private void CancelCharacterCreation()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CancelCharacterCreation");
            Common.LogEntry(M, "Started - setting IsBusy = false");
            Common.MainViewModel.IsBusy = false;

            if (ViewModel_SelectedCharacter.CreationMode)
            {
                Common.LogEntry(M, "Disabling creation mode");
                ViewModel_SelectedCharacter.CreationMode = false;

                Common.LogEntry(M, "Removing selected character");
                Characters.Remove(SelectedCharacter);

                Common.LogEntry(M, "Selecting first character");
                SelectedCharacterIndex = 0;
            }

            Common.LogEntry(M, "Finished");
        }

        #endregion

        #region Methods.Overrides

        protected override void OnDispose()
        {
            base.OnDispose();
        }

        #endregion
    }

    
}
