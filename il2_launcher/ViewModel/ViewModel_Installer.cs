﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Data;
    using il2_clienttools.EventHandlers;
    using il2_clienttools.Helper;
    using il2_clienttools.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Net;
    using System.Threading;

    #endregion

    /// <summary>
    /// Handles the interaction logic of the Installer view for launcher
    /// </summary>
    public class ViewModel_Installer : ViewModel_Base
    {
        #region Constructors

        /// <summary>
        /// Constructs a new instance of the Installer view model object
        /// </summary>
        private ViewModel_Installer()
        {
            string M = GetType().Name;
            Common.LogEntry(M, "Queueing init...");
            ThreadPool.QueueUserWorkItem((state) => { Initialize(); });
        }

        #endregion

        #region Constants

        private static readonly string MESSAGE_DOWNLOAD_FAILED = "Unable to download file {0}.";
        private static readonly string MESSAGE_INIT_FAILED = "Unable to initialize installer";
        private static readonly string NAME_FILE_ITEMS_LIST = "il2_content.xml";
        private static readonly string NAME_FILE_ITEMS_LIST_TEST = "il2_content_test.xml";

        #endregion

        #region Fields

        private string _step;
        private string _speed;
        private string _remaining;
        private string _finishIn;
        private long _bytesVerify;
        private long _bytesVerifyMax;
        private long _bytesDownload;
        private long _bytesDownloadMax;
        private long _bytesInstall;
        private long _bytesInstallMax;
        private bool _downloadingFile;
        private bool _checkingForUpdates;
        private bool _errorsDuringUpdate;
        private long _bytesDownloadedForFile;
        private long _bytesProcessed;
        private IList<Model_FileItem> _fileItemsToUpdate;
        private IList<Model_FileItem> _fileItems;
        private Dictionary<string, Action> _onFileDownloadedActions;
        private CancellationTokenSource _cancelSource;
        private Timer _timer;
        private readonly object _lockDownload = new object();
        private static ViewModel_Installer _instance;
        private int _lockDownloadCount;
        private bool _fileItemsListDownloaded;
        private List<FileInfo> _completedFiles;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the installer instance
        /// </summary>
        public static ViewModel_Installer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ViewModel_Installer();
                return _instance;
            }
        }

        /// <summary>
        /// Gets the instance of a Timer
        /// </summary>
        private Timer Timer
        {
            get 
            {
                if (_timer == null)
                    _timer = new Timer(OnTimerHit, null, Timeout.Infinite, 1000);

                return _timer; 
            }
        }

        /// <summary>
        /// Gets the value of the current step of installer
        /// </summary>
        public string Step
        {
            get { return _step; }
            private set 
            { 
                _step = value;
                base.OnPropertyChanged("Step");
            }
        }

        /// <summary>
        /// Gets the value of the remaining file size to verify / download / install
        /// </summary>
        public string Remaining
        {
            get { return _remaining; }
            private set 
            {
                if (_remaining == value) return;

                _remaining = value;

                base.OnPropertyChanged("Remaining");
            }
        }

        /// <summary>
        /// Gets the value of the average process speed
        /// </summary>
        public string Speed
        {
            get { return _speed; }
            private set 
            {
                if (_speed == value) return;

                _speed = value;

                base.OnPropertyChanged("Speed");
            }
        }

        /// <summary>
        /// Gets the value of the approximately remaining time needed to finish the installments
        /// </summary>
        public string FinishIn
        {
            get { return _finishIn; }
            private set 
            {
                if (_finishIn == value) return;

                _finishIn = value;

                base.OnPropertyChanged("FinishIn");
            }
        }

        /// <summary>
        /// Gets the value of the overall progress of the installer
        /// </summary>
        public float ProgressValue
        {
            get { return DetermineProgressValue(); }
        }

        /// <summary>
        /// Gets the value indicating whether the installer is currently in the process of checking for updates
        /// </summary>
        public bool CheckingForUpdates
        {
            get { return _checkingForUpdates; }
            private set 
            {
                if (_checkingForUpdates == value) return;

                _checkingForUpdates = value;

                base.OnPropertyChanged("CheckingForUpdates");
            }
        }

        /// <summary>
        /// Gets the list of file items
        /// </summary>
        private IList<Model_FileItem> FileItems
        {
            get 
            {
                if (_fileItems == null)
                    _fileItems = new Collection<Model_FileItem>();

                return _fileItems; 
            }
        }

        /// <summary>
        /// Gets or sets the bytes verified so far
        /// </summary>
        private long InternBytesVerify
        {
            get { return _bytesVerify; }
            set
            {
                if (_bytesVerify == value) return;

                _bytesVerify = value;

                base.OnPropertyChanged("ProgressValue");
            }
        }

        /// <summary>
        /// Gets or sets the bytes downloaded so far
        /// </summary>
        private long InternBytesDownload
        {
            get { return _bytesDownload; }
            set
            {
                if (_bytesDownload == value) return;

                _bytesDownload = value;

                base.OnPropertyChanged("ProgressValue");
            }
        }

        /// <summary>
        /// Gets or sets the bytes installed so far
        /// </summary>
        private long InternBytesInstall
        {
            get { return _bytesInstall; }
            set 
            {
                if (_bytesInstall == value) return;

                _bytesInstall = value;

                base.OnPropertyChanged("ProgressValue");
            }
        }

        /// <summary>
        /// Indicates whether an update of the local content is needed
        /// </summary>
        public bool UpdateNeeded 
        { 
            get { return _fileItemsToUpdate != null && _fileItemsToUpdate.Count > 0; } 
        }

        /// <summary>
        /// Indicates whether there were any errors during the install process
        /// </summary>
        public bool ErrorsDuringUpdate
        {
            get { return _errorsDuringUpdate; }
            private set 
            {
                if (_errorsDuringUpdate == value) return;

                _errorsDuringUpdate = value;

                base.OnPropertyChanged("ErrorsDuringUpdate");
            }
        }

        #endregion

        #region Methods.Interface

        /// <summary>
        /// Starts the process of checking for updates (if not started yet)
        /// </summary>
        public void CheckForUpdates()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CheckForUpdates");
            Common.LogEntry(M, "Started");

            if (CheckingForUpdates)
            {
                Common.LogEntry(M, "Already checking - returning");
                return;
            }

            Common.LogEntry(M, "Marking CheckingForUpdates");
            CheckingForUpdates = true;

            Common.LogEntry(M, "Calling StartInstaller()");
            StartInstaller();

            Common.LogEntry(M, "Finished");
        }

        #endregion

        #region Methods.EventHandlers

        private void InstallerMessanger_Completeed(object sender, MessageResponseEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "OnDownloadFileCompleted");

            lock (_lockDownload)
            {
                Common.LogEntry(M, "Started");
                Common.LogEntry(M, "Acquiring lock: {0}", ++_lockDownloadCount);
                Common.LogEntry(M, "Calling CompleteFileDownload");
                HandlerInstallerMessangerResponse(e);
                Common.LogEntry(M, "Releasing lock: {0}", --_lockDownloadCount);
                Common.LogEntry(M, "Finished");
            }
        }

        private void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "OnDownloadFileProgressChanged");

            lock (_lockDownload)
            {
                Common.LogEntry(M, "Started");
                Common.LogEntry(M, "Acquiring lock: {0}", ++_lockDownloadCount);
                Common.LogEntry(M, "Calling ProgressFileDownload");
                ProgressFileDownload(e);
                Common.LogEntry(M, "Releasing lock: {0}", --_lockDownloadCount);
                Common.LogEntry(M, "Finished");
            }
        }

        private void OnModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateProgressFromFileItemChange(sender as Model_FileItem, e.PropertyName);
        }

        #endregion

        #region Methods.Helpers

        /// <summary>
        /// Returns true if the active override folder contains il2_override.txt file
        /// </summary>
        public bool IsCustomOverride()
        {
            if (Common.BaseDir_NWN == null)
                throw new ApplicationException("Unable to determine override type when BaseDir is not defined.");

            return !File.Exists(Path.Combine(Common.BaseDir_NWN.FullName, "override", "il2_override.txt"));
        }

        /// <summary>
        /// Performs the following set of actions:
        /// 1. Checks if the file {NWN_DIR}/override/il2_override.txt exists
        /// 2. If it does not exist then compresses {NWN_DIR}/override folder into {NWN_DIR}/override_backup/override_yyyyMMdd_HHmmss.zip
        /// 3. Cleans the {NWN_DIR}/override folder
        /// </summary>
        public void OverrideBackup()
        {
            if (!IsCustomOverride())
                return;

            string M = string.Format("{0}.{1}", typeof(Common).Name, ".OverrideBackup()");
            string dirSource = Path.Combine(Common.BaseDir_NWN.FullName, "override");
            string dirDest = Path.Combine(Common.BaseDir_NWN.FullName, "override_backup");
            string fileDest = Path.Combine(dirDest, string.Format("override_{0:yyyyMMdd_HHmmss}.zip", DateTime.Now));
            string fileDialogZip = Path.Combine(dirDest, "dialog.zip");

            if (!Directory.Exists(dirDest))
                Directory.CreateDirectory(dirDest);

            if (File.Exists(fileDialogZip))
                File.Delete(fileDialogZip);

            Common.CompressDirectory(dirSource, fileDest);
            Common.CompressFile(Path.Combine(Common.BaseDir_NWN.FullName, "dialog.tlk"), fileDialogZip);
            Common.LogEntry(M, "Created override backup: \"{0}\"", fileDest);

            if (Directory.Exists(dirSource))
            {
                Directory.Delete(dirSource, true);
                Directory.CreateDirectory(dirSource);
            }
        }

        /// <summary>
        /// Performs the following set of actions:
        /// 1. Checks if the file {NWN_DIR}/override/il2_override.txt exists
        /// 2. If it does not exist then:
        /// 2.1 Cleans the {NWN_DIR}/override folder
        /// 2.2 Decompresses the first file {NWN_DIR}/override_backup/override_*.zip into {NWN_DIR}/override
        /// </summary>
        public void OverrideRestore()
        {
            if (IsCustomOverride())
                return;

            string M = string.Format("{0}.{1}", typeof(Common).Name, ".OverrideRestore()");

            string dirSource = Path.Combine(Common.BaseDir_NWN.FullName, "override");
            string[] backups = GetOverrideBackups();

            if (backups == null || backups.Length == 0)
                return;

            string fileBackupOverride = backups[backups.Length - 1];
            string fileBackupDialog = Path.Combine(Common.BaseDir_NWN.FullName, "override_backup", "dialog.zip");
            string fileDialog = Path.Combine(Common.BaseDir_NWN.FullName, "dialog.tlk");

            if (Directory.Exists(dirSource))
            {
                Directory.Delete(dirSource, true);
                Directory.CreateDirectory(dirSource);
            }

            if (File.Exists(fileDialog) && File.Exists(fileBackupDialog))
                File.Delete(fileDialog);

            Common.DecompressFile(dirSource, fileBackupOverride);
            Common.DecompressFile(Common.BaseDir_NWN.FullName, fileBackupDialog);
            Common.LogEntry(M, "Restored override backup: \"{0}\"", fileBackupOverride);
            File.Delete(fileBackupOverride);
            File.Delete(fileBackupDialog);
        }

        private static string[] GetOverrideBackups()
        {
            string dirBackup = Path.Combine(Common.BaseDir_NWN.FullName, "override_backup");

            if (!Directory.Exists(dirBackup))
                Directory.CreateDirectory(dirBackup);

            string[] backups = Directory.GetFiles(dirBackup, "override_*.zip", SearchOption.TopDirectoryOnly);
            return backups;
        }

        /// <summary>
        /// Initializes the installer instance
        /// </summary>
        private void Initialize()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "Initialize");
            Common.LogEntry(M, "Started - Hooking events..");

            Common.LogEntry(M, "Disabling CheckingForUpdates");
            CheckingForUpdates = false;

            Common.LogEntry(M, "Calling CheckForUpdates()");
            CheckForUpdates();

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Cancels the installer process
        /// </summary>
        public void CancelUpdates()
        {
            if (!CheckingForUpdates) return;

            Step = "Cancelling updates...";

            //Common.ClientGame.CancelAsync();
            _cancelSource.Cancel();

            Step = "Updates cancelled.";
        }

        /// <summary>
        /// Prepares the installer environment
        /// </summary>
        private void StartInstaller()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "StartInstaller");
            Common.LogEntry(M, "Started");

            _fileItems = new Collection<Model_FileItem>();
            _fileItemsToUpdate = new Collection<Model_FileItem>();
            _completedFiles = new List<FileInfo>();
            _onFileDownloadedActions = new Dictionary<string, Action>();
            
            if (_cancelSource != null)
            {
                Common.LogEntry(M, "Disposing an existing cancelSource");
                _cancelSource.Dispose();
            }

            Step = "Restoring override";
            OverrideRestore();
            Step = "Initializing...";

            Common.LogEntry(M, "Creating a new instance of cancelSource");
            _cancelSource = new CancellationTokenSource();

            Common.LogEntry(M, "Calling DownloadFile({0}, ()=>{1})", GetDefinitionFile(), "_fileItemsListDownloaded = true");

            _fileItemsListDownloaded = false;
            DownloadFile(new FileInfo(GetDefinitionFile()), () => _fileItemsListDownloaded = true);
            
            while (!_fileItemsListDownloaded) 
                Thread.Sleep(100);
            ContinueWithLoadingFileItems();

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Determines the definition file based on the current environment
        /// </summary>
        private string GetDefinitionFile()
        {
            return Common.EnvironmentType == Common.ENVIRONMENT_LIVE ? NAME_FILE_ITEMS_LIST : NAME_FILE_ITEMS_LIST_TEST;
        }

        /// <summary>
        /// At every period the indicators will be updated
        /// </summary>
        /// <param name="state"></param>
        private void OnTimerHit(object state)
        {
            if (!CheckingForUpdates)
                StopTimer();

            UpdateIndicators();
        }

        /// <summary>
        /// Restarts the timer
        /// </summary>
        private void RestartTimer()
        {
            Timer.Change(0, 250);
        }

        /// <summary>
        /// Stops the timer
        /// </summary>
        private void StopTimer()
        {
            Timer.Change(Timeout.Infinite, Timeout.Infinite);
            UpdateIndicators();
        }

        /// <summary>
        /// Initializes file item collection from localFile
        /// </summary>
        /// <param name="fileItemsFile"></param>
        private void ContinueWithLoadingFileItems()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "ContinueWithLoadingFileItems");
            Common.LogEntry(M, "Start - Calling LoadFileItems()");
            LoadFileItems();

            Common.LogEntry(M, "Calling RestartTimer()");
            RestartTimer();
            
            Common.LogEntry(M, "Calling VerifyLocalContent()");
            VerifyLocalContent();

            Common.LogEntry(M, "Calling DownloadAndInstallUpdates()");
            DownloadAndInstallUpdates();

            Common.LogEntry(M, "Calling CleanUp()");
            CleanUp();
        
            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Starts the "Downloading and installing updates" process
        /// </summary>
        private void DownloadAndInstallUpdates()
        {
            try
            {
                string M = string.Format("{0}.{1}", GetType().Name, "DownloadAndInstallUpdates");
                Common.LogEntry(M, "Start");

                if (ErrorsDuringUpdate)
                {
                    Common.LogEntry(M, "Errors during update - skip");
                    return;
                }

                if (!UpdateNeeded)
                {
                    Common.LogEntry(M, "Update not needed - skip");
                    return;
                }

                Step = "Downloading & installing updates";

                Common.LogEntry(M, "Calling RecalculateProgressValues() and nulify bytesVerifyMax");
                RecalculateProgressValues();
                _bytesVerifyMax = 0;

                Common.LogEntry(M, "Looping through the ToBeUpdates list");

                foreach (var itemToUpdate in _fileItemsToUpdate)
                {
                    Common.LogEntry(M, "File item: {0}", itemToUpdate);
                    Step = string.Format("Downloading file {0}...", itemToUpdate.FileInfo.Name);

                    Common.LogEntry(M, "Calling DownloadFile({0})", itemToUpdate.FileInfo);
                    DownloadFile(itemToUpdate.FileInfo);

                    Common.LogEntry(M, "While _downloadingFile ThrewadSleep(100)");
                    while (_downloadingFile) Thread.Sleep(100); //////// replace with Application.DoEvents or whatever its replaced with in WPF

                    Common.LogEntry(M, "While _downloadingFile - finished, not sleeping anymore");
                    Common.LogEntry(M, "Refreshing itemToUpdate.FileInfo: {0}", itemToUpdate.FileInfo);
                    itemToUpdate.FileInfo.Refresh();

                    if (itemToUpdate.FileInfo.Extension.ToUpper() != ".ZIP")
                    {
                        Common.LogEntry(M, "File extension is not .ZIP - decompressing with update...");
                        Step = string.Format("Installing file {0}...", itemToUpdate.FileInfo.Name);
                        var install = Common.DecompressFileWithUpdate(
                            itemToUpdate.FileInfo, 
                            itemToUpdate.FileSize,
                            (length) => _bytesInstallMax += (long)length - itemToUpdate.FileSize,
                            (bytes) => _bytesInstall += (long)bytes, 
                            _cancelSource.Token);

                        if (!install)
                        {
                            Common.LogEntry(M, "Error during decompression..");
                            ErrorsDuringUpdate = true;
                            Step = string.Format("Error installing file {0}", itemToUpdate.FileInfo.Name);
                            return;
                        }

                        Common.LogEntry(M, "Decompression of {0} success", itemToUpdate);
                    }
                }

                Common.LogEntry(M, "Calling VerifyLocalContent() again");
                VerifyLocalContent();

                Common.LogEntry(M, "Finished");
            }
            catch (Exception ex)
            {
                ErrorsDuringUpdate = true;
                Step = string.Format("Error: {0}-{1}", ex.GetType().Name, ex.Message);
            }
        }

        /// <summary>
        /// Loads file items from the xml file and throws an exception if there were errors
        /// </summary>
        private void LoadFileItems()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "LoadFileItems");
            Common.LogEntry(M, "Start");

            var localFile = new FileInfo(GetDefinitionFile());

            Common.LogEntry(M, "Calling LoadListFromXML()");
            Common.LoadListFromXML(_fileItems as Collection<Model_FileItem>, localFile, (fileItem) => fileItem.PropertyChanged += this.OnModelPropertyChanged);

            if (_fileItems == null || _fileItems.Count == 0)
            {
                Common.LogEntry(M, "Error: null or zero file items.. Throwing exception...");
                throw new ApplicationException(MESSAGE_INIT_FAILED);
            }

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Starts the "Verifying local content" process
        /// </summary>
        private void VerifyLocalContent()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "VerifyLocalContent");
            Common.LogEntry(M, "Start");

            Step = "Verifying local content...";

            Common.LogEntry(M, "Calling RecalculateProgressValues()...");
            RecalculateProgressValues();
            _bytesInstallMax = 0;
            _bytesDownloadMax = 0;

            var updateWasNeeded = UpdateNeeded;
            var error = false;

            Common.LogEntry(M, "Storing updateWasNeeded: {0} and looping through file items..", updateWasNeeded);
            foreach (var fi in _fileItems)
            {
                Common.LogEntry(M, "Refreshing file info for {0}", fi);
                fi.FileInfo.Refresh();

                Common.LogEntry(M, "Checking write access...");
                if (!Common.IsFileWriteable(fi.FileInfo.FullName))
                {
                    ErrorsDuringUpdate = true;
                    Step = string.Format("Need write access for: {0}", fi.FileInfo.Name);
                    Common.LogEntry(M, Step);
                    break;
                }
                else
                {
                    Common.LogEntry(M, "Verifying...");
                    if (!VerifyFileItem(fi))
                    {
                        Common.LogEntry(M, "File is different");
                        if (!updateWasNeeded)
                        {
                            Common.LogEntry(M, "Adding file item to the ToBeUpdated list");
                            _fileItemsToUpdate.Add(new Model_FileItem(fi));
                        }
                        else
                            error = true;

                        Common.LogEntry(M, "Error state: {0}", error);
                    }
                }
            }

            if (updateWasNeeded && !error)
            {
                Common.LogEntry(M, "Nullifying the ToBeUpdates list");
                _fileItemsToUpdate = null;
            }

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Verifies a single file item
        /// </summary>
        /// <param name="item"></param>
        private bool VerifyFileItem(Model_FileItem item)
        {
            try
            {
                if (!item.FileInfo.Exists || item.FileInfo.Length != item.FileSize)
                {
                    FinishFileItem(item);
                    return false;
                }

                if (item.IsSensitive)
                {
                    var newItem = new Model_FileItem(item.FileInfo);

                    newItem.PropertyChanged += OnModelPropertyChanged;
                    Common.ComputeChecksumWithUpdate(newItem, _cancelSource.Token);
                    
                    var equals = newItem.Checksum.Equals(item.Checksum);

                    newItem = null;

                    return equals;
                }
                else
                    FinishFileItem(item);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Updates progress of a file item to 100%
        /// </summary>
        /// <param name="item"></param>
        private void FinishFileItem(Model_FileItem item)
        {
            item.StatusSizeVerified += item.IsSensitive ? item.FileInfo.Length : 1;
            item.Status = 100M;
        }

        /// <summary>
        /// Finishes the install process and cleans up the environment
        /// </summary>
        private void CleanUp()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CleanUp");

            if (ErrorsDuringUpdate)
            { 
                // nothing
            }
            else if (UpdateNeeded)
            {
                Step = "Update still needed after install";
                ErrorsDuringUpdate = true;
            }
            else
                Step = "Ready to play!";

            InternBytesDownload = 0;
            InternBytesInstall = 0;
            InternBytesVerify = 0;
            _completedFiles = null;
            _fileItems = null;
            _fileItemsToUpdate = null;
            CheckingForUpdates = false;
            Common.LogEntry(M, "Last step: {0}", Step);
            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Downloads ftp version of localFile
        /// </summary>
        /// <param name="localFile"></param>
        private void DownloadFile(FileInfo localFile)
        {
            DownloadFile(localFile, null);
        }

        /// <summary>
        /// Downloads ftp version of localFile
        /// </summary>
        /// <param name="localFile"></param>
        private void DownloadFile(FileInfo localFile, Action onFileDownloadedAction)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "DownloadFile");
            Common.LogEntry(M, "Started");

            if (onFileDownloadedAction != null)
            {
                Common.LogEntry(M, "Binding {0} for exec after {1} is downloaded", onFileDownloadedAction.Method.Name, localFile.Name);
                _onFileDownloadedActions[localFile.Name] = onFileDownloadedAction;
            }

            Common.LogEntry(M, "Setting downloadingFile to true");
            _downloadingFile = true;

            Common.LogEntry(M, "Calling ClientGame.DownloadFileStart({0})", localFile.FullName);
            Common.DownloadFileAsync(localFile.FullName, Common.EnvironmentType, InstallerMessanger_Completeed, OnDownloadProgressChanged, localFile);

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// Recalculates the internal progress values 
        /// </summary>
        /// <param name="_fileItemsToUpdate"></param>
        private void RecalculateProgressValues()
        {
            if (_bytesVerifyMax == 0)
                foreach (var fi in _fileItems)
                {
                    fi.FileInfo.Refresh();
                    _bytesVerifyMax += !fi.FileInfo.Exists ? 0 : fi.IsSensitive ? fi.FileInfo.Length : 1;
                }

            if (_bytesDownloadMax == 0 && _bytesInstallMax == 0)
            {
                foreach (var fi in _fileItemsToUpdate)
                {
                    _bytesDownloadMax += Common.GetRemoteFileSize(fi.FileInfo, Common.EnvironmentType);

                    if (fi.FileInfo.Extension.ToUpper() != ".ZIP")
                        _bytesInstallMax += fi.FileSize;
                }
            }

            InternBytesDownload = 0;
            InternBytesVerify = 0;
            InternBytesInstall = 0;
            _bytesProcessed = 0;
        }

        /// <summary>
        /// Calculates the total progress value
        /// </summary>
        /// <returns></returns>
        private float DetermineProgressValue()
        {
            string M = string.Format("{0}.{1}", GetType().Name, "ProgressFileDownload");
            var pgVerify = _bytesVerifyMax == 0 ? 0 : (float)_bytesVerify / _bytesVerifyMax;
            var pgDownload = _bytesDownloadMax == 0 ? 0 : (float)_bytesDownload / _bytesDownloadMax;
            var pgInstall = _bytesInstallMax == 0 ? 0 : (float)_bytesInstall / _bytesInstallMax;
            var pgs = 0;

            #region MAX < CURRENT bug debug section

            // at the begining of installer init the size of decompressed file might differ if il2_content.xml contains older file definitions
            // than the ones uploaded - in such case the Max values are dynamically adjusted at point of decompression
            // So if it happens at this stage it's considered as error because the decompression stage alreadty happened...
            if (_bytesDownloadMax < _bytesDownload ||
                _bytesInstallMax < _bytesInstall)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                else
                    Common.LogEntry(M, "ERROR STATE: Max DL/IN < Current!");
            }

            #endregion

            pgs += _bytesVerifyMax == 0 ? 0 : 1;
            pgs += _bytesDownloadMax == 0 ? 0 : 1;
            pgs += _bytesInstallMax == 0 ? 0 : 1;

            var pgTotal = (pgVerify + pgDownload + pgInstall) / pgs;

            return pgTotal * 100;
        }

        /// <summary>
        /// Updates the installer indicators based on current and max values and previous measurements
        /// </summary>
        private void UpdateIndicators()
        {
            if (!CheckingForUpdates)
            {
                Remaining = Speed = FinishIn = string.Empty;
                return;
            }

            // force update Verify, Download and Install indicators
            base.OnPropertyChanged("ProgressValue");

            var max = _bytesVerifyMax + _bytesDownloadMax + _bytesInstallMax;
            var current = _bytesVerify + _bytesDownload + _bytesInstall;

            // remaining
            var remaining = max - current;
            Remaining = Common.GetFileSizeFormat(remaining, perSecond: false, unitAfter: true);

            // speed
            var increase = current - _bytesProcessed;
            _bytesProcessed = current;
            
            Speed = Common.GetFileSizeFormat(increase, perSecond: true, unitAfter: true);

            // finish in
            var seconds = increase == 0 ? 0 : remaining / increase;

            FinishIn = Common.GetTimeFormat(seconds);
        }

        /// <summary>
        /// Signals the total progress procedure to be fired to refresh as the Status of one of the file items was changed
        /// </summary>
        /// <param name="e"></param>
        private void UpdateProgressFromFileItemChange(Model_FileItem fileItem, string propertyName)
        {
            if (fileItem != null && propertyName == "Status")
            {
                var increase = fileItem.StatusSizeVerifiedIncrease;
                _bytesVerify += increase;
            }
        }

        /// <summary>
        /// Advances the progress based on the amount of bytes received
        /// </summary>
        /// <param name="e"></param>
        private void ProgressFileDownload(DownloadProgressChangedEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "ProgressFileDownload");
            Common.LogEntry(M, "Started");

            if (!(e.UserState is FileInfo))
            {
                Common.LogEntry(M, "e.UserState is not FileInfo - returning");
                return;
            }

            var localFile = (FileInfo)e.UserState;
            Common.LogEntry(M, "localFile: {0}", localFile.FullName);

            // local content is zipped
            if (localFile.Extension.ToUpper() != ".ZIP")
            {
                Common.LogEntry(M, "Extension is not .ZIP - returning");
                return;
            }

            if (_completedFiles.Contains(localFile))
            {
                Common.LogEntry(M, "File already completed - ignore");
                return;
            }

            long sizeIncreased = e.BytesReceived - _bytesDownloadedForFile;

            if (sizeIncreased < 0)
            {
                Common.LogEntry(M, "Received is < 0 - skip");
                return;
            }

            Common.LogEntry(M, "Received {0} bytes, soFar {1} bytes >> increase = {2}", e.BytesReceived, _bytesDownloadedForFile, sizeIncreased );

            Common.LogEntry(M, "Incrementing InternBytesDownload and soFar by {0}", sizeIncreased);
            _bytesDownload += sizeIncreased;
            _bytesDownloadedForFile += sizeIncreased;

            Common.LogEntry(M, "Finished");
        }

        /// <summary>
        /// After file downloads is completed handles errors and performs the associated actions with the related file name
        /// </summary>
        /// <param name="e"></param>
        private void HandlerInstallerMessangerResponse(MessageResponseEventArgs e)
        {
            string M = string.Format("{0}.{1}", GetType().Name, "CompleteFileDownload");
            Common.LogEntry(M, "Started - setting soFar to 0 and downloadingFile to false");

            _bytesDownloadedForFile = 0;
            _downloadingFile = false;
            base.OnPropertyChanged("ProgressValue");

            if (e.Response == null || !e.Response.HasValue)
                throw new ApplicationException("Missing messanger response");

            Data_MessageResponse response = e.Response.Value;
            if (response.ExceptionObject != null)
            {
                if (response.ExceptionObject is OperationCanceledException)
                {
                    Common.LogEntry(M, "Cancelled - returning");
                    return;
                }

                Common.LogEntry(M, "Error: {0}-{1}", response.ExceptionObject.GetType().FullName, response.ExceptionObject.Message);

                if (response.ExceptionObject is WebException)
                {
                    var msg = string.Format(MESSAGE_DOWNLOAD_FAILED, e.UserState is FileInfo ? ((FileInfo)e.UserState).Name : "???");
                    Common.LogEntry(M, "Showing WebException error: {0}", msg);
                    throw new ApplicationException(msg, response.ExceptionObject);
                }
                else
                    throw response.ExceptionObject;
            }

            FileInfo localFile = e.UserState as FileInfo;
            Common.LogEntry(M, "localFile: {0}", localFile);

            if (localFile != null)
            {
                if (_onFileDownloadedActions.ContainsKey(localFile.Name) && _onFileDownloadedActions[localFile.Name] != null)
                {
                    Common.LogEntry(M, "calling bound action: {0}..", _onFileDownloadedActions[localFile.Name]);
                    _onFileDownloadedActions[localFile.Name]();
                    Common.LogEntry(M, "call of bound action: {0} - finished", _onFileDownloadedActions[localFile.Name]);
                }

                Common.LogEntry(M, "Refreshing local file: {0}..", localFile);
                localFile.Refresh();
            }

            _completedFiles.Add(localFile);
            Common.LogEntry(M, "Finished");
        }

        #endregion

        #region Methods.Override

        /// <summary>
        /// Disposes of the instance
        /// </summary>
        protected override void OnDispose()
        {
            base.OnDispose();
            _instance = null;
        }

        #endregion
    }
}
