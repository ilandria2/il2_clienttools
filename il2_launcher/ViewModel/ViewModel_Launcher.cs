﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.Model;
    using il2_clienttools.View;
    using il2_corelib.Controls;
    using il2_corelib.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Reflection;
    using System.Threading;
    using System.Windows.Input;

    #endregion

    /// <summary>
    /// The ViewModel for the main window called "Launcher"
    /// </summary>
    public class ViewModel_Launcher : ViewModel_WindowLauncher
    {
        #region Fields

        private Model_User _user;
        private IList<ViewModel_MainTab> _mainTabs;

        #endregion

        #region Properties.CLR
        
        /// <summary>
        /// Gets the User of this view model
        /// </summary>
        public Model_User User 
        {
            get { return this._user; }
            private set { this._user = value; }
        }

        /// <summary>
        /// Gets the list of MainTab view model instances
        /// </summary>
        public IList<ViewModel_MainTab> MainTabs
        {
            get 
            {
                if (this._mainTabs == null)
                    this._mainTabs = InitMainTabs();

                return this._mainTabs; 
            }
        }

        /// <summary>
        /// Gets the installer instance
        /// </summary>
        public ViewModel_Installer Installer 
        {
            get { return ViewModel_Installer.Instance; }
        }

        #endregion

        #region Commands

        #region Commands.Fields

        private ICommand _command_Logout;
        private ICommand _command_Refresh;

        #endregion

        #region Commands.Properties

        /// <summary>
        /// Gets the command for logging the user out
        /// </summary>
        public ICommand Command_Logout
        {
            get 
            {
                if (this._command_Logout == null)
                    this._command_Logout = new RelayCommand(this.Execute_Logout, this.CanExecute_Logout);

                return this._command_Logout; 
            }
        }

        /// <summary>
        /// Gets the command that refreshes the game server status
        /// </summary>
        public ICommand Command_Refresh
        {
            get
            {
                if (this._command_Refresh == null)
                    this._command_Refresh = new RelayCommand(this.Execute_Refresh, this.CanExecute_Refresh);

                return this._command_Refresh;
            }
        }

        #endregion

        #region Commands.CanExecute

        /// <summary>
        /// Indicates whether the Refresh command can execute or not
        /// </summary>
        private bool CanExecute_Refresh(object obj)
        {
            return true;
        }

        /// <summary>
        /// Indicates whether the Logout command can execute or not
        /// </summary>
        private bool CanExecute_Logout(object obj)
        {
            return this.User != null && !string.IsNullOrWhiteSpace(this.User.UserName);
        }

        #endregion

        #region Commands.Execute

        /// <summary>
        /// Executes the Refresh command
        /// </summary>
        private void Execute_Refresh(object obj)
        {
            ThreadPool.QueueUserWorkItem((state) =>
                { 
                    Common.MainViewModel.IsBusy = true;
                    for (int i = 0; i < 10; i++)
                    {
                        Thread.Sleep(250);
                        Common.MainViewModel.BusyProgress = i * 10;
                    }
                    Common.MainViewModel.IsBusy = false;
                });

        }

        /// <summary>
        /// Executes the Logout command
        /// </summary>
        private void Execute_Logout(object obj)
        {
            AttemptLogoutAsync(this.User.UserName, obj);
            Common.InitView(typeof(View_Login), typeof(ViewModel_Login), 2, true, true);
            this.Command_Close.Execute(null);
            ((IDisposable)this).Dispose();
        }

        #endregion

        #endregion

        #region Properties.Overrides

        /// <summary>
        /// Gets the title of this ViewModel
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return string.Format("{0}{1}", "Launcher", Common.EnvironmentType == Common.ENVIRONMENT_TEST ? " (Test server)" : string.Empty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new ViewModel for the main window
        /// </summary>
        public ViewModel_Launcher(IMinimizeable minimize, Model_User user)
            : this(minimize, user, false)
        {
        }

        /// <summary>
        /// Creates a new ViewModel for the main window with a specification of test or public server
        /// </summary>
        public ViewModel_Launcher(IMinimizeable minimize, Model_User user, bool useTestServer)
            : base(minimize)
        {
            this.User = user;
            Common.EnvironmentType = useTestServer; 
        }
        

        #endregion

        #region Methods.Helpers

        /// <summary>
        /// Sends a logout message for a user
        /// </summary>
        /// <param name="user"></param>
        private bool AttemptLogoutAsync(string user, object param)
        {
            try
            {
                var messageLogout = Common.MessageSmfLogout(user);

                if (!Common.PostMessageAsync(Common.URL_SMF_CLIENT, messageLogout))
                    throw new WebException();
            }
            catch (WebException) { }
            catch (Exception) { }

            return true;
        }

        /// <summary>
        /// Initializes a new list of MainTab view model instances
        /// </summary>
        private IList<ViewModel_MainTab> InitMainTabs()
        {
            IList<ViewModel_MainTab> list = new List<ViewModel_MainTab>();

            list.Add(new ViewModel_MainNews());
            list.Add(new ViewModel_MainGame());
            list.Add(new ViewModel_MainAccount());
            list.Add(new ViewModel_MainContact());

            return list;
        }

        #endregion

        #region Methods.Overrides

        protected override void OnDispose()
        {
            base.OnDispose();
            
            if (Installer is IDisposable)
                ((IDisposable)Installer).Dispose();
        
            foreach (var mainTab in MainTabs)
                if (mainTab is IDisposable)
                    ((IDisposable)mainTab).Dispose();

            MainTabs.Clear();
        }

        #endregion
    }
}
