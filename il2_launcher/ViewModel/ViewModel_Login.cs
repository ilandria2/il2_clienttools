﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Data;
    using il2_clienttools.EventHandlers;
    using il2_clienttools.Helper;
    using il2_clienttools.Model;
    using il2_clienttools.View;
    using il2_corelib.Controls;
    using il2_corelib.Helper;
    using il2_corelib.Interfaces;
    using il2_launcher;
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Windows;
    using System.Windows.Input;
    
    #endregion

    /// <summary>
    /// Interaction logic of the Login form
    /// </summary>
    public class ViewModel_Login : ViewModel_WindowLauncher
    {
        #region Constructors

        /// <summary>
        /// Constructs the interaction logic model for the Login form
        /// </summary>
        /// <param name="minimize"></param>
        public ViewModel_Login(IMinimizeable minimize) : base(minimize)
        {
            if (Common.BaseDir_NWN == null)
                throw new ApplicationException("BaseDir not initialized");

            this.IsReady = true;
            
            //  ready default user from nwnplayer.ini file, section "Player Name=xxxx"
            this.LoginUser = Common.ReadIniValue(
                new FileInfo(Common.BaseDir_NWN.FullName + @"\nwnplayer.ini"), "Player Name");

            if (!string.IsNullOrWhiteSpace(this.LoginUser))
                IsFocusOnPassword = true;
        }

        #endregion

        #region Destructor

        /// <summary>
        /// Destructs the interaction logic model for the Login form
        /// </summary>
        ~ViewModel_Login()
        {
            this.IsReady = false;
        }

        #endregion

        #region Constants

        private const string MESSAGE_REQUESTING_LOG_IN = "Logging in...";
        private const string MESSAGE_LOGIN_ERROR = "Log in error";
        private const string MESSAGE_LOGIN_FAILED = "Invalid username or password";
        private const string MESSAGE_LOGIN_CANCELLED = "Log in cancelled";
        private const string MESSAGE_LOGIN_SUCCESS = "Logged in successfully";

        #endregion

        #region Fields

        private ICommand _command_Login;
        private ICommand _command_Register;
        private ICommand _command_Forgot;
        private ICommand _command_Cancel;
        private string _loginUser;
        private string _userEmail;
        private bool _isReady;
        private string _message;
        private bool _isFocusOnPassword;
        private bool _useTestServer;

        #endregion

        #region Properties.CLR

        /// <summary>
        /// Gets or sets the environment type
        /// </summary>
        public bool EnvironmentType
        {
            get { return Common.EnvironmentType; }
            set { Common.EnvironmentType = value; }
        }

        /// <summary>
        /// Gets or sets the LoginUser property
        /// </summary>
        public string LoginUser
        {
            get { return this._loginUser; }
            set 
            {
                if (this._loginUser == value) return;

                this._loginUser = value;

                base.OnPropertyChanged("LoginUser");
            }
        }

        /// <summary>
        /// Gets or sets the UserEmail property
        /// </summary>
        public string UserEmail
        {
            get { return this._userEmail; }
            set 
            {
                if (this._userEmail == value) return;

                this._userEmail = value;

                base.OnPropertyChanged("UserEmail");
            }
        }

        /// <summary>
        /// Indicates if the login page is ready for user input
        /// </summary>
        public bool IsReady
        {
            set
            {
                if (this._isReady == value) return;
             
                this._isReady = value;

                base.OnPropertyChanged("IsReady");
                base.OnPropertyChanged("Command_LoginOrCancel");
                CommandManager.InvalidateRequerySuggested();
            }

            get { return this._isReady; }
        }

        /// <summary>
        /// Gets or sets the Message that is displayed on the Login form
        /// </summary>
        public string Message
        {
            get { return this._message; }
            set 
            {
                if (this._message == value) return;

                this._message = value;

                base.OnPropertyChanged("Message");
            }
        }

        /// <summary>
        /// Sets the focus on password box
        /// </summary>
        public bool IsFocusOnPassword
        {
            get { return _isFocusOnPassword; }
            set 
            {
                if (value == false) return;
                if (_isFocusOnPassword)
                    _isFocusOnPassword = !_isFocusOnPassword;

                base.OnPropertyChanged("IsFocusOnPassword"); 
 
                _isFocusOnPassword = value;

                base.OnPropertyChanged("IsFocusOnPassword");
            }
        }

        #endregion

        #region Properties.Overrides

        /// <summary>
        /// Gets the display name specific for this view model
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return "Log in";
            }
        }

        #endregion

        #region Commands

        #region Properties.Commands

        /// <summary>
        /// Gets the ICommand instance of the Login command
        /// </summary>
        public ICommand Command_Login
        {
            get 
            {
                if (this._command_Login == null)
                    this._command_Login = new RelayCommand(this.Execute_Login, this.CanExecute_Login);

                return this._command_Login; 
            }
        }

        /// <summary>
        /// Gets the ICommand instance of the Register command
        /// </summary>
        public ICommand Command_Register
        {
            get
            {
                if (this._command_Register == null)
                    this._command_Register = new RelayCommand(this.Execute_Register, this.CanExecute_Register);

                return this._command_Register;
            }
        }

        /// <summary>
        /// Gets the ICommand instance of the Forgot command
        /// </summary>
        public ICommand Command_Forgot
        {
            get
            {
                if (this._command_Forgot == null)
                    this._command_Forgot = new RelayCommand(this.Execute_Forgot, this.CanExecute_Forgot);

                return this._command_Forgot;
            }
        }

        /// <summary>
        /// Gets the ICommand instance of the Cancel command
        /// </summary>
        public ICommand Command_Cancel
        {
            get
            {
                if (this._command_Cancel == null)
                    this._command_Cancel = new RelayCommand(this.Execute_Cancel, this.CanExecute_Cancel);

                return this._command_Cancel;
            }
        }

        /// <summary>
        /// Gets the ICommand instance of the Login or Cancel commands based on the IsReady state
        /// </summary>
        public ICommand Command_LoginOrCancel
        {
            get { return this.IsReady ? this.Command_Login : this.Command_Cancel; }
        }
        
        #endregion

        #region Commands.CanExecute

        /// <summary>
        /// Determines whether the Login command can execute
        /// </summary>
        /// <param name="param">Command parameter (PasswordBox) passed from XAML</param>
        private bool CanExecute_Login(object param)
        {
            var pbox = param as System.Windows.Controls.PasswordBox;
            
            return this.IsReady && pbox.SecurePassword.Length >= 5;
        }

        /// <summary>
        /// Determines whether the Cancel command can execute
        /// </summary>
        private bool CanExecute_Cancel(object param)
        {
            return !this.IsReady;
        }

        /// <summary>
        /// Determines whether the Forgot command can execute
        /// </summary>
        private bool CanExecute_Forgot(object param)
        {
            return this.IsReady;
        }
        
        /// <summary>
        /// Determines whether the Register command can execute
        /// </summary>
        private bool CanExecute_Register(object param)
        {
            return this.IsReady;
        }

        #endregion

        #region Commands.Execute

        /// <summary>
        /// Performs the Cancel command's logic
        /// </summary>
        private void Execute_Cancel(object param)
        {
            /*
            if (_clientSmf != null)
                _clientSmf.CancelAsync();

            if (_clientGame != null)
                _clientGame.CancelAsync();
            */
        }

        /// <summary>
        /// Performs the Forgot command's logic
        /// </summary>
        private void Execute_Forgot(object param)
        {
            Common.OpenUrl(Common.URL_SMF_RESET);
        }

        /// <summary>
        /// Performs the Register command's logic
        /// </summary>
        private void Execute_Register(object param)
        {
            Common.OpenUrl(Common.URL_SMF_REGISTER);
        }

        /// <summary>
        /// Performs the Login command's login
        /// </summary>
        /// <param name="param">Command parameter passed from XAML</param>
        private void Execute_Login(object param)
        {
            AttemptLoginAsync(this.LoginUser, param);
        }

        #endregion

        #endregion

        #region EventHandlers

        /// <summary>
        /// Handles the event which occurs when ClientSmf completes uploading values
        /// </summary>
        private void LoginMessanger_Completed(object sender, MessageResponseEventArgs e)
        {
            if (e.Response == null || !e.Response.HasValue)
                throw new ApplicationException("Missing messanger response");

            Data_MessageResponse response = e.Response.Value;
            if (response.ExceptionObject != null)
            {

                if (response.ExceptionObject is OperationCanceledException)
                    Message = MESSAGE_LOGIN_CANCELLED;
                else
                    Message = response.ExceptionObject is WebException ? response.ExceptionObject.Message : MESSAGE_LOGIN_ERROR;

                IsReady = true;
                IsFocusOnPassword = true;
            }

            else
                this.CompleteLogin(response);
        }

        #endregion

        #region EventHandlers.Overrides
        
        /// <summary>
        /// Placeholder for the OnDispose event
        /// </summary>
        protected override void OnDispose()
        {
            base.OnDispose();
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Attempts to log in a user with a password
        /// </summary>
        /// <param name="param">A password box</param>
        private bool AttemptLoginAsync(string user, object param)
        {
            if (Common.BaseDir_NWN == null)
                throw new ApplicationException("BaseDir not initialized");

            this.IsReady = false;
            this.Message = MESSAGE_REQUESTING_LOG_IN;

            try
            {
                //  store default user to nwnplayer.ini file, section "Player Name=xxxx"
                if (!Common.StoreIniValue(new FileInfo(Common.BaseDir_NWN.FullName + @"\nwnplayer.ini"), "Player Name", this.LoginUser))
                    throw new InvalidOperationException(Common.ERROR_UNABLE_TO_SAVE_INI_VALUE);

                var messageLogin = Common.MessageSmfLogin(user, param as System.Windows.Controls.PasswordBox);

                if (!Common.PostMessageAsync(Common.URL_SMF_CLIENT, messageLogin, LoginMessanger_Completed))
                    throw new WebException();
            }
            catch (WebException)
            {
                this.Message = Common.MESSAGE_NETWORK_ERROR;
                this.IsReady = true;
                return false;
            }
            catch (Exception)
            {
                this.Message = MESSAGE_LOGIN_ERROR;
                this.IsReady = true;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Attempts to check a user on game server and submits a request to create one if it does not exist
        /// </summary>
        private bool AttemptCheckAccountAsync(string user)
        {
            try
            {
                var data = Common.MessageClient(user, -1, Common.EnvironmentType, Common.MESSAGE_TYPE_CHECK_ACCOUNT, 0, -1, string.Empty);

                if (!Common.PostMessageAsync(Common.URL_GAME_CLIENT, data, LoginMessanger_Completed))
                    throw new WebException();
            }
            catch (WebException)
            {
                this.Message = Common.MESSAGE_NETWORK_ERROR;
                this.IsReady = true;
                return false;
            }
            catch (Exception)
            {
                this.Message = MESSAGE_LOGIN_ERROR;
                this.IsReady = true;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Parses the server login response and opens launcher's main view
        /// </summary>
        private bool CompleteLogin(Data_MessageResponse data)
        {
            #region SMF client

            if (data.ClientType == Common.CLIENT_TYPE_SMF)
            {
                #region Expected response format
                //  SUCCESS:
                //  [1]="1"
                //  [2]="VirgiL"
                //  [3]="abc@123.net"

                //  FAILURE:
                //  [1]="0"
                //  [2]="virgil"
                //  [3]="..error-message.."
                #endregion

                //  Successfully logged in
                if (data.Data == "1")
                {
                    this.LoginUser = data.Requestor;
                    this.UserEmail = data.Parameter;

                    AttemptCheckAccountAsync(this.LoginUser);
                    return true;
                }

                //  Failed to log in
                else
                {
                    this.Message = MESSAGE_LOGIN_FAILED;
                    this.IsReady = true;
                    IsFocusOnPassword = true;
                    return false;
                }
            }

            #endregion

            #region GAME client

            else if (data.ClientType == Common.CLIENT_TYPE_GAME)
            {
                #region RESULTS ARRAY FORMAT
                /*
                 * [0]="GAME"   - client type (game, smf)
                 * [1]="1"      - testing environment? 1/0
                 * [2]="0"      - msgType
                 * [3]="0"      - msgSubType
                 * [4]="VirgiL" - msgParameter
                 * [5]="9"      - message specific response
                 * [6]="ERROR TEXT" - error text if any
                 */
                #endregion

                #region MessageType: Check account

                if (data.Type == Common.MESSAGE_TYPE_CHECK_ACCOUNT)
                {
                    int responseCode = int.Parse(data.Data);

                    //  valid states
                    if (new[] { 
                        Common.RESPONSE_TYPE_CHECK_ACCOUNT_ALREADY_EXISTS, 
                        Common.RESPONSE_TYPE_CHECK_ACCOUNT_CREATION_REQUEST_EXISTING, 
                        Common.RESPONSE_TYPE_CHECK_ACCOUNT_CREATION_REQUEST_NEW }.Contains(responseCode))
                    {
                        Common.User = new Model_User(this.LoginUser, this.UserEmail); ;
                        this.Message = MESSAGE_LOGIN_SUCCESS;

                        Common.InitView(typeof(View_Launcher), typeof(ViewModel_Launcher), 2, true, true, Common.User, Common.EnvironmentType);

                        this.IsReady = true;
                        this.Command_Close.Execute(null);
                        ((IDisposable)this).Dispose();
                        return true;
                    }

                    //  Failure during account check / creation
                    else
                    {
                        this.Message = MESSAGE_LOGIN_ERROR;
                        this.IsReady = true;
                        IsFocusOnPassword = true;
                        return false;
                    }
                }

                else
                    throw new ApplicationException("Response for invalid message type received");

                #endregion
            }

            #endregion

            #region OTHER client

            else
            {
                this.Message = MESSAGE_LOGIN_ERROR;
                IsFocusOnPassword = true;
                return false;
            }

            #endregion
        }

        #endregion
    }
}
