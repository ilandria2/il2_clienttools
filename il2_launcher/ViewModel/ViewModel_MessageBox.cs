﻿
namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.ViewModel;
    using il2_corelib.Controls;
    using il2_corelib.Interfaces;
    using System;
    using System.Windows.Input;
    
    #endregion

    public class ViewModel_MessageBox : ViewModel_WindowLauncher
    {
        #region Fields

        private ICommand _command_OK;
        private ICommand _command_Cancel;
        private string _message;
        private bool? _notificatorResult;
        private bool _showCancel;

        #endregion

        #region Properties

        /// <summary>
        /// The message to be displayed on the Notificator (MessageBox)
        /// </summary>
        public string Message
        {
            get { return this._message; }
            set 
            {
                if (this._message == value) return;

                this._message = value;

                base.OnPropertyChanged("Message");
            }
        }

        /// <summary>
        /// Gets or sets the NotificatorResult value
        /// </summary>
        public bool? NotificatorResult
        {
            get { return this._notificatorResult; }
            set 
            {
                if (this._notificatorResult == value) return;

                this._notificatorResult = value;

                base.OnPropertyChanged("NotificatorResult");
            }
        }

        /// <summary>
        /// Gets the Command_OK command representing the confirmation that the message was read
        /// </summary>
        public ICommand Command_OK
        {
            get 
            {
                if (this._command_OK == null)
                    this._command_OK = new RelayCommand(this.Execute_OK);

                return this._command_OK; 
            }
        }

        /// <summary>
        /// Gets the Cancel command
        /// </summary>
        public ICommand Command_Cancel
        {
            get 
            {
                if (_command_Cancel == null)
                    _command_Cancel = new RelayCommand(this.Execute_Cancel);

                return _command_Cancel; 
            }
        }

        /// <summary>
        /// Performs the Cancel command
        /// </summary>
        /// <param name="param"></param>
        private void Execute_Cancel(object param)
        {
            this.NotificatorResult = false;

            base.Command_Close.Execute(param);
        }

        /// <summary>
        /// Performs the OK command
        /// </summary>
        /// <param name="param"></param>
        private void Execute_OK(object param)
        {
            this.NotificatorResult = true;

            base.Command_Close.Execute(param);
        }

        /// <summary>
        /// Gets or sets whether the Cancel button is shown
        /// </summary>
        public bool ShowCancel
        {
            get { return _showCancel; }
            set 
            {
                if (_showCancel == value) return;

                _showCancel = value;

                base.OnPropertyChanged("ShowCancel");
            }
        }

        #endregion

        #region Constructors

        public ViewModel_MessageBox(string title, string message, ICloseable close) : base(close)
        {
            this.DisplayName = title;
            this.Message = message;
        }

        public ViewModel_MessageBox(string title, string message, bool showCancel, ICloseable close)
            : this(title, message, close)
        {
            ShowCancel = showCancel;
        }

        #endregion
    }
}
