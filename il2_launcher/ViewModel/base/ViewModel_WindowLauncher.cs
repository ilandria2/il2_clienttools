﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_corelib.Controls;
    using il2_corelib.Interfaces;

    #endregion

    /// <summary>
    /// This class is an extension of the ViewModel_Window and it's primary purpose
    /// is to give the window additional assembly-sensitive properties
    /// such as version and title
    /// </summary>
    /// <remarks>
    /// This class needs to reside in the Calling assembly
    /// </remarks>
    public abstract class ViewModel_WindowLauncher : ViewModel_Window
    {
        #region Fields

        private string _versionCalling;
        private string _versionExecuting;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new Workspace for window (closeable ViewModel) object
        /// </summary>
        public ViewModel_WindowLauncher(ICloseable close)
            : base(close)
        { }

        /// <summary>
        /// Creates a new Workspace for window (minimalizable and closeable ViewModel) object
        /// </summary>
        public ViewModel_WindowLauncher(IMinimizeable minimizeClose)
            : base(minimizeClose)
        { }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the version of the calling assembly
        /// </summary>
        public string VersionCalling
        {
            get 
            {
                if (string.IsNullOrWhiteSpace(this._versionCalling))
                    this._versionCalling = AssemblyInfo.GetVersionCalling();

                return this._versionCalling; 
            }
        }

        /// <summary>
        /// Gets the version of the executing assembly
        /// </summary>
        public string VersionExecuting
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._versionExecuting))
                    this._versionExecuting = AssemblyInfo.GetVersionExecuting();

                return this._versionExecuting;
            }
        }

        #endregion
    }
}
