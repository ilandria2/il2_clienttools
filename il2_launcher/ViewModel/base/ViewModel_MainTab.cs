﻿namespace il2_clienttools.ViewModel
{
    public abstract class ViewModel_MainTab : ViewModel_Base
    {
#if DEBUG
        ~ViewModel_MainTab()
        {
            System.Diagnostics.Debugger.Log(1, "Disposal", "Finalizing " + this.GetType().FullName + "\n");
        }
#endif
    }
}
