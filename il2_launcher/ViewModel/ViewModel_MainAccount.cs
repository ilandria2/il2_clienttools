﻿namespace il2_clienttools.ViewModel
{
    public class ViewModel_MainAccount : ViewModel_MainTab
    {
        /// <summary>
        /// Gets a name of the view model
        /// </summary>
        public override string DisplayName
        {
            get { return "Account"; }
        }
    }
}
