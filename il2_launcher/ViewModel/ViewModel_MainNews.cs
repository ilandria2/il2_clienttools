﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.Model;

    #endregion

    public class ViewModel_MainNews : ViewModel_MainTab
    {
        #region Constants
        
        private const string JSON_SAMPLE = "[{\"title\":\"Another test article\",\"header\":\"sjdla jdkls jdiojadk asjkld j\",\"body\":\"d adk nsajdas djasid uqiouw89uqw8 e8w q7e89 qwdu qio\",\"avatar\":\"article2.png\",\"deleted_at\":null,\"created_at\":\"2016-05-07 12:54:38\",\"updated_at\":\"2016-05-07 12:54:38\",\"published_at\":\"2016-05-07 00:00:00\",\"published_by\":\"tester\",\"slug\":\"another-test-article\"},{\"title\":\"Test aaa\",\"header\":\"dseffwefwe\",\"body\":\"qewrewrwer\",\"avatar\":\"article2.png\",\"deleted_at\":null,\"created_at\":\"2016-05-07 13:17:15\",\"updated_at\":\"2016-05-07 13:17:15\",\"published_at\":\"2016-05-07 00:00:00\",\"published_by\":\"und3ath\",\"slug\":\"test-aaa\"},{\"title\":\"New article for this site\",\"header\":\"helllo shajksh ajksh ajksha jksa bn\",\"body\":\"adasjkd asdhkas d jkhask dh\",\"avatar\":\"article1.png\",\"deleted_at\":null,\"created_at\":\"2016-05-07 13:31:54\",\"updated_at\":\"2016-05-07 13:31:54\",\"published_at\":\"2016-05-07 00:00:00\",\"published_by\":\"virgil\",\"slug\":\"new-article-for-this-site\"}]";
        
        #endregion

        #region Constructors

        /// <summary>
        /// Constructs the News main tab
        /// </summary>
        public ViewModel_MainNews()
        {
            InitializeArticles(JSON_SAMPLE);
        }

        #endregion

        #region Fields
        
        private Model_ArticleList _articles;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of Articles (news)
        /// </summary>
        public Model_ArticleList Articles
        {
            get { return _articles; }
            private set { _articles = value; }
        }

        #endregion

        #region Methods.Helpers

        private void InitializeArticles(string json)
        {
            Articles = Json.Deserialize<Model_ArticleList>(json);
        }

        #endregion

        #region Methods.Overrides

        /// <summary>
        /// Gets a name of the view model
        /// </summary>
        public override string DisplayName
        {
            get { return "News"; }
        }

        #endregion
    }
}
