﻿namespace il2_clienttools.ViewModel
{
    #region Namespaces

    using il2_clienttools.Helper;
    using il2_clienttools.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Media;

    #endregion

    /// <summary>
    /// Interaction logic for the Character view
    /// </summary>
    public class ViewModel_Character : ViewModel_Workspace
    {
        #region Constructors

        /// <summary>
        /// Constructs a new character view model instance
        /// </summary>
        /// <param name="viewCloseHandler"></param>
        /// <param name="character"></param>
        public ViewModel_Character(Action viewCloseHandler, Model_Character character)
            : base(viewCloseHandler)
        {
            Character = character;
            Initialize();
        }

        #endregion

        #region Fields

        internal static readonly string _headPathFormat = string.Format(Common.ABSOLUTE_ASSEMBLY_URI, "il2_resources", "Images/Characters/head/", "p{0}{1}0_head{2:000}.png");
        internal static readonly int[] _headTypesMale = new int[] { 1, 4, 5, 7, 8, 9, 10, 12, 13, 15, 16, 17, 18, 21, 22, 23, 25, 28, 29, 30, 31, 32, 112, 113, 114, 115, 116, 163, 164 };
        internal static readonly int[] _headTypesFemale = new int[] { 1, 2, 3, 4, 5, 7, 8, 9, 10, 13, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 28, 29, 30, 31, 140, 142, 145, 146, 147, 148, 149, 150, 152, 155, 166, 180, 181, 191 };
        internal static readonly int[] _paletteColorsSkin = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 116, 117, 118, 119, 130, 131, 156, 157, 174, 175};
        internal static readonly int[] _paletteColorsHair = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 116, 117, 118, 119, 124, 125, 126, 127, 128, 129, 130, 131, 156, 157, 174, 175, 48, 50, 60};
        internal static readonly IList<Model_ImageItem> _headModelsMale = new List<Model_ImageItem>();
        internal static readonly IList<Model_ImageItem> _headModelsFemale = new List<Model_ImageItem>();
        private Model_Character _character;
        private ViewModel_WrappedImageList _viewModel_HeadType;
        private ViewModel_WrappedImageList _viewModel_SkinColor;
        private ViewModel_WrappedImageList _viewModel_HairColor;
        private bool _creationMode;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates whether Character is null
        /// </summary>
        public bool CharacterIsNull { get { return _character == null; } }
        
        /// <summary>
        /// Indicates whether character is not null
        /// </summary>
        public bool CharacterIsNotNull { get { return _character != null; } }

        /// <summary>
        /// Gets or sets the creation mode
        /// </summary>
        public bool CreationMode
        {
            get { return _creationMode; }
            set
            {
                if (_creationMode == value) return;

                _creationMode = value;
                Character.CreationMode = value;

                if (_creationMode)
                    RefreshHeadModelSource();

                base.OnPropertyChanged("CreationMode");
                base.OnPropertyChanged("NotCreationMode");
            }
        }

        /// <summary>
        /// Gets or sets the not creation mode
        /// </summary>
        public bool NotCreationMode
        {
            get { return !_creationMode; }
            set
            {
                if (_creationMode == !value) return;

                _creationMode = !value;

                base.OnPropertyChanged("CreationMode");
                base.OnPropertyChanged("NotCreationMode");
            }
        }

        /// <summary>
        /// Represents the character being shown / edited
        /// </summary>
        public Model_Character Character
        {
            get 
            {
                if (_character == null)
                    _character = null;// new Model_Character();

                return _character; 
            }
            set 
            {
                if (_character == value) return;

                // old character - unsubscribe
                if (_character != null && value != null)
                    _character.PropertyChanged -= CharacterChanged;
                
                _character = value;

                if (CreationMode)
                    RefreshHeadModelSource();

                // new character - subscribe
                if (_character != null)
                    _character.PropertyChanged += CharacterChanged;

                base.OnPropertyChanged("Character");
                CharacterChanged(this, new PropertyChangedEventArgs("Character"));
            }
        }

        /// <summary>
        /// View model for Head type
        /// </summary>
        public ViewModel_WrappedImageList ViewModel_HeadType
        {
            get
            {
                if (_viewModel_HeadType == null)
                {
                    _viewModel_HeadType = new ViewModel_WrappedImageList();
                    _viewModel_HeadType.PropertyChanged += HeadTypeChanged;
                }

                return _viewModel_HeadType;
            }
        }

        /// <summary>
        /// View model for Skin color
        /// </summary>
        public ViewModel_WrappedImageList ViewModel_SkinColor
        {
            get
            {
                if (_viewModel_SkinColor == null)
                {
                    _viewModel_SkinColor = new ViewModel_WrappedImageList();
                    _viewModel_SkinColor.PropertyChanged += SkinColorChanged;
                }

                return _viewModel_SkinColor;
            }
        }

        /// <summary>
        /// View model for Hair color
        /// </summary>
        public ViewModel_WrappedImageList ViewModel_HairColor
        {
            get
            {
                if (_viewModel_HairColor == null)
                {
                    _viewModel_HairColor = new ViewModel_WrappedImageList();
                    _viewModel_HairColor.PropertyChanged += HairColorChanged;
                }

                return _viewModel_HairColor;
            }
        }

        /// <summary>
        /// Gets or sets the IsMale property
        /// </summary>
        public bool IsMale
        {
            get { return Character == null ? false : Character.Gender == Gender.Male; }
            set 
            {
                if (Character == null || Character.Gender == Gender.Male) return;
                
                if (value)
                {
                    Character.Gender = Gender.Male;
                    IsFemale = false;
                }

                RefreshHeadModelSource();

                base.OnPropertyChanged("IsMale");
                base.OnPropertyChanged("Character");
                base.OnPropertyChanged("PlayerModel_Body");
            }
        }

        /// <summary>
        /// Gets or sets the IsFemale property
        /// </summary>
        public bool IsFemale
        {
            get { return Character == null ? false : Character.Gender == Gender.Female; }
            set
            {
                if (Character == null || Character.Gender == Gender.Female) return;

                if (value)
                {
                    Character.Gender = Gender.Female;
                    IsMale = false;
                }

                RefreshHeadModelSource();

                base.OnPropertyChanged("IsFemale");
                base.OnPropertyChanged("Character");
                base.OnPropertyChanged("PlayerModel_Body");
            }
        }

        /// <summary>
        /// Gets or sets the IsSkinny property
        /// </summary>
        public bool IsSkinny
        {
            get { return Character == null ? false : Character.Phenotype == 0; }
            set
            {
                if (Character == null || Character.Phenotype == 0) return;

                if (value)
                {
                    Character.Phenotype = 0;
                    IsFat = false;
                }

                base.OnPropertyChanged("IsSkinny");
                base.OnPropertyChanged("IsFat");
                base.OnPropertyChanged("Character");
                base.OnPropertyChanged("PlayerModel_Body");
            }
        }

        /// <summary>
        /// Gets or sets the IsFat property
        /// </summary>
        public bool IsFat
        {
            get { return Character == null ? false : Character.Phenotype == 1; }
            set
            {
                if (Character == null || Character.Phenotype == 1) return;

                if (value)
                {
                    Character.Phenotype = 1;
                    IsSkinny = false;
                }
                base.OnPropertyChanged("IsFat");
                base.OnPropertyChanged("IsSkinny");
                base.OnPropertyChanged("Character");
                base.OnPropertyChanged("PlayerModel_Body");
            }
        }

        /// <summary>
        /// Gets the PlayerModel_Body property
        /// </summary>
        public string PlayerModel_Body
        {
            get 
            {
                return Character == null ? string.Empty : string.Format(Common.ABSOLUTE_ASSEMBLY_URI, "il2_resources", "Images/Characters/body/", ParseBodyModelTexture(Character.Gender, Character.Phenotype, Character.Race, Character.HeadType)); 
            }
        }

        #endregion

        #region Methods.EventHandlers

        /// <summary>
        /// Handles the Hair color changed event
        /// </summary>
        void HairColorChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Character == null) return;

            var hairType = ParseHairType(ViewModel_HairColor.SelectedIndex);

            if (Character.HairType == hairType) return;

            Character.PropertyChanged -= CharacterChanged;
            Character.HairType = hairType;
            Character.PropertyChanged -= CharacterChanged;
        }

        /// <summary>
        /// Handles the Head type changed event
        /// </summary>
        void HeadTypeChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Character == null) return;

            var headType = ParseHeadType(ViewModel_HeadType.SelectedIndex, Character.Gender);

            if (Character.HeadType == headType) return;

            Character.PropertyChanged -= CharacterChanged;
            Character.HeadType = headType;
            base.OnPropertyChanged("PlayerModel_Body");
            Character.PropertyChanged += CharacterChanged;
        }

        /// <summary>
        /// Handles the Skin color changed event
        /// </summary>
        void SkinColorChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Character == null) return;

            var skinType = ParseSkinType(ViewModel_SkinColor.SelectedIndex);

            if (Character.SkinType == skinType) return;

            Character.PropertyChanged -= CharacterChanged;
            Character.SkinType = skinType;
            Character.PropertyChanged -= CharacterChanged;
        }

        /// <summary>
        /// Handles the PropertyChanged event of the child Character object
        /// </summary>
        private void CharacterChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged("CharacterIsNull");
            base.OnPropertyChanged("CharacterIsNotNull");
            
            if (Character == null) return;

            ViewModel_HairColor.SelectedIndex = ParseHairIndex(Character.HairType);
            ViewModel_SkinColor.SelectedIndex = ParseSkinIndex(Character.SkinType);

            // after IsMale or IsFemale is property changed then head type is reset due to refreshing its source
            // we need to reapply after below OnPropertyChanged events
            //***ViewModel_HeadType.SelectedIndex = ParseHeadIndex(Character.HeadType, Character.Gender);
            byte headType = Character.HeadType;

            base.OnPropertyChanged("Character");
            base.OnPropertyChanged("IsMale");
            base.OnPropertyChanged("IsFemale");
            base.OnPropertyChanged("IsSkinny");
            base.OnPropertyChanged("IsFat");
            base.OnPropertyChanged("PlayerModel_Body");

            ViewModel_HeadType.SelectedIndex = ParseHeadIndex(headType, Character.Gender);
        }

        #endregion

        #region Methods.Helpers
        
        /// <summary>
        /// Parses the Body model texture from a combination of gender, phenotype and racialType inputs
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="phenotype"></param>
        /// <param name="raceType"></param>
        /// <returns>Body model texture file name</returns>
        private string ParseBodyModelTexture(Gender gender, int phenotype, Race raceType, int headModel)
        {
            var sRace = "H";
            var sGender = "M";
            var sPheno = "0";

            if (gender == Gender.Female)
                sGender = "F";

            if (phenotype == 1)
                sPheno = "F";

            return string.Format("p{0}{1}{2}_body{3:000}.png", sGender, sRace, sPheno, headModel);
        }

        /// <summary>
        /// Parses the Hair type from selected skin index
        /// </summary>
        private byte ParseHairType(int selectedHairIndex)
        {
            byte HAIR_INVALID = 0;

            if (selectedHairIndex > _paletteColorsHair.Length)
                return HAIR_INVALID;

            return (byte)_paletteColorsHair[selectedHairIndex];
        }

        /// <summary>
        /// Parses the Skin type from selected skin index
        /// </summary>
        private byte ParseSkinType(int selectedSkinIndex)
        {
            byte SKIN_INVALID = 0;

            if (selectedSkinIndex > _paletteColorsSkin.Length)
                return SKIN_INVALID;

            return (byte)_paletteColorsSkin[selectedSkinIndex];
        }

        /// <summary>
        /// Parses the Head type from selected head index
        /// </summary>
        /// <param name="selectedHeadIndex"></param>
        private byte ParseHeadType(int selectedHeadIndex, Gender gender)
        {
            byte HEAD_INVALID = 1;

            if (gender != Gender.Male && gender != Gender.Female)
                return HEAD_INVALID;

            var headTypes = gender == Gender.Male ? _headTypesMale : _headTypesFemale;

            if (selectedHeadIndex > headTypes.Length)
                return HEAD_INVALID;

            return (byte)headTypes[selectedHeadIndex];
        }

        /// <summary>
        /// Initializes the character view model
        /// </summary>
        private void Initialize()
        {
            ViewModel_HeadType.SetHeader("Head type");
            ViewModel_SkinColor.SetHeader("Skin color");
            ViewModel_HairColor.SetHeader("Hair color");

            InitializeHeadModelSources();
            InitializeSkinColorSource();
            InitializeHairColorSource();
        }

        /// <summary>
        /// Initializes the hair color types list
        /// </summary>
        private void InitializeHairColorSource()
        {
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(118, 76, 23)));   // [0]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(103, 62, 18)));   // [1]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(84, 49, 16)));    // [2]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(51, 25, 10)));    // [3]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(119, 44, 24)));   // [4]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(96, 31, 17)));    // [5]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(76, 21, 13)));    // [6]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(54, 12, 12)));    // [7]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(208, 185, 157))); // [8]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(188, 158, 125))); // [9]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(166, 133, 95)));  // [10]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(149, 115, 75)));  // [11]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(122, 91, 62)));   // [12]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(92, 67, 47)));    // [13]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(57, 38, 31)));    // [14]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(21, 13, 13)));    // [15]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(184, 195, 203))); // [16]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(150, 162, 168))); // [17]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(109, 124, 134))); // [18]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(70, 82, 87)));    // [19]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(33, 40, 50)));    // [20]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(10, 17, 25)));    // [21]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(6, 9, 12)));      // [22]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(3, 3, 3)));       // [23]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(117, 80, 65)));   // [116]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(73, 32, 25)));    // [117]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(69, 29, 23)));    // [118]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(29, 5, 5)));      // [119]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(71, 59, 57)));    // [124]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(56, 42, 41)));    // [125]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(30, 20, 20)));    // [126]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(25, 13, 14)));    // [127]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(123, 93, 62)));   // [128]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(93, 62, 31)));    // [129]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(59, 34, 12)));    // [130]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(36, 15, 3)));     // [131]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(122, 72, 49)));   // [156]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(101, 55, 33)));   // [157]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(30, 20, 20)));    // [174]
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(53, 31, 10)));    // [175]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(153, 117, 84)));  // [48]

            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(143, 120, 99)));  // [50]
            
            ViewModel_HairColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(46, 46, 46)));    // [60]
        }

        /// <summary>
        /// Initializes the skin color types list
        /// </summary>
        private void InitializeSkinColorSource()
        {
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(213, 171, 133))); // [0]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(198, 150, 112))); // [1]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(186, 130, 93)));  // [2]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(162, 113, 81)));  // [3]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(134, 99, 76)));   // [4]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(110, 79, 60)));   // [5]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(90, 63, 46)));    // [6]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(63, 43, 32)));    // [5]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(215, 187, 137))); // [6]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(187, 144, 94)));  // [7]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(137, 110, 77)));  // [8]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(93, 70, 47)));    // [9]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(194, 167, 135))); // [116]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(175, 144, 116))); // [117]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(149, 115, 92)));  // [118]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(123, 87, 69)));   // [119]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(149, 123, 94)));  // [130]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(133, 105, 72)));  // [131]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(212, 182, 153))); // [156]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(179, 151, 116))); // [157]

            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(86, 60, 57)));    // [174]
            ViewModel_SkinColor.ItemsSource.Add(new Model_ImageItem(Color.FromRgb(133, 105, 72)));  // [175]
        }

        /// <summary>
        /// Initializes the head types list
        /// </summary>
        private void InitializeHeadModelSources()
        {
            // male
            _headModelsMale.Clear();

            foreach (var item in _headTypesMale)
                _headModelsMale.Add(new Model_ImageItem(string.Format(_headPathFormat, "M", "H", item), "Select head model", null));

            // female
            _headModelsFemale.Clear();

            foreach (var item in _headTypesFemale)
                _headModelsFemale.Add(new Model_ImageItem(string.Format(_headPathFormat, "F", "H", item, "Select head model", null)));
        }

        /// <summary>
        /// Refreshes head type list for the selected gender type
        /// </summary>
        private void RefreshHeadModelSource()
        {
            ViewModel_HeadType.SelectedIndex = 0;

            if (IsMale)
                ViewModel_HeadType.ItemsSource = _headModelsMale;
            else if (IsFemale)
                ViewModel_HeadType.ItemsSource = _headModelsFemale;
        }

        /// <summary>
        /// Converts HeadType into SelectedHeadIndex
        /// </summary>
        /// <param name="headType"></param>
        private int ParseHeadIndex(byte headType, Gender gender)
        {
            int HEAD_INVALID = 1;

            if (gender != Gender.Male && gender != Gender.Female) 
                return HEAD_INVALID;

            var headTypes = gender == Gender.Male ? _headTypesMale : _headTypesFemale;

            for (int i = 0; i < headTypes.Length; i++)
                if (headTypes[i] == headType)
                    return i;

            return HEAD_INVALID;
        }

        /// <summary>
        /// Converts SkinType into SelectedSkinIndex
        /// </summary>
        /// <param name="skinType"></param>
        private int ParseSkinIndex(byte skinType)
        {
            return (int)skinType;
        }

        /// <summary>
        /// Converts HairType into SelectedHairIndex
        /// </summary>
        /// <param name="hairType"></param>
        private int ParseHairIndex(byte hairType)
        {
            return (int)hairType;
        }

        #endregion
    }
}
