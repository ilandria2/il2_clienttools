using System.Reflection;
using System.Runtime.InteropServices;



/************ Local assembly info *************/

[assembly: AssemblyTitle("Launcher")]
[assembly: AssemblyDescription("Client tools for Ilandria 2 players.")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("B49A0B05-1816-421B-8BB1-E0FAA8695DAE")]

//VERSION=0.6.167
[assembly: AssemblyInformationalVersion("0.6.167")]
[assembly: AssemblyFileVersion("0.6.167")]
[assembly: AssemblyVersion("0.6.167")]
