﻿
namespace il2_clienttools.Helper
{
    #region Namespaces

    using il2_clienttools.View;
    using il2_clienttools.ViewModel;
    using il2_corelib.Controls;
    using il2_corelib.Interfaces;
    using System;
    using System.Windows;
    
    #endregion

    /// <summary>
    /// Custom MessageBox-based notification class
    /// </summary>
    public static class Notificator
    {
        internal static bool? Show(string title, string message)
        {
            return Show(title, message, isError: false, showCancel: false);
        }

        internal static bool Prompt(string title, string message)
        {
            bool? confirmed = Show(title, message, isError: false, showCancel: true);

            return confirmed.HasValue && confirmed.Value;
        }


        /// <summary>
        /// Shows an informative readonly titled message box
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        private static bool? Show(string title, string message, bool isError, bool showCancel)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                return Application.Current.Dispatcher.Invoke(() => Show(title, message, isError, showCancel));
            }

            try
            {
                var view = new View_MessageBox();
                var viewModel = new ViewModel_MessageBox(title, message, showCancel, view as ICloseable);

                view.DataContext = viewModel;

                view.ShowDialog();

                return viewModel.NotificatorResult;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error: \n" + ex.Message);
                return false;
            }
        }

        internal static void Error(string title, string message)
        {
            Show(title, message, isError: true, showCancel: false);
        }
    }
}
