﻿using il2_clienttools.Helper;
using System;
using System.Diagnostics;

namespace il2_launcher
{
    public static class ExceptionHandler
    {
        /// <summary>
        /// Common exception handler
        /// </summary>
        /// <param name="ex">The exception to be handled</param>
        internal static void HandleException(Exception ex)
        {
            if (Debugger.IsAttached)
                Debugger.Break();

            if (HandleKnownException(ex)) return;

            var iexName = ex.InnerException == null ? "Inner exception" : ex.InnerException.GetType().FullName;
            var iexMessage = ex.InnerException == null ? "<none>" : ex.InnerException.Message;

            var message = string.Format("{0}:\n{1}\n\n{2}:\n{3}", ex.GetType().FullName, ex.Message, iexName, iexMessage);
            
            Notificator.Error("Error occured", message);
        }

        /// <summary>
        /// Tries to handles known unhandled exception - returns true on success
        /// </summary>
        /// <param name="ex"></param>
        internal static bool HandleKnownException(Exception ex)
        {
            string message = string.Empty;

            if (ex is InvalidOperationException)
            {

            }

            else if (ex.InnerException != null && ex.InnerException is InvalidOperationException)
            {
                var iex = (InvalidOperationException)ex.InnerException;

                if (string.IsNullOrWhiteSpace(iex.Message)) return false;

                if (iex.Message == Common.ERROR_INVALID_BASEDIR)
                {
                    message = Common.ERROR_INVALID_BASEDIR;
                }
            }

            if (!string.IsNullOrWhiteSpace(message))
            {
                Notificator.Error("Error occured", message);
                return true;
            }

            return false;
        }
    }
}
