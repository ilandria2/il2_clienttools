### What is this repository? ###

* The "il2_clienttools" repository is used by a team who develops the parent/main project labeled as "Ilandria 2"
* The project labeled as "Ilandria 2" is a non-commerecial game mod project for a PC game labeled as "Neverwinter Nights" which was developed by a company "BioWare".
* The game has a built-in construction set (IDE) called "Toolset" which allows the developers to build their own game worlds and implement custom game mechanics.
* Usually every game world in the Neverwinter Nights game also comes up with it's custom content that the player needs to install before he/she will be able to join the game / constribute in the development.
* The primary purpose of the "il2_clienttools" sub-project is to develop a set of Client tools for both players and admins/developers/contributors to be able to properly install/manage the custom content, easily connect to the game server and use the right admin tools to contribute in the development of the parent/main project "Ilandria 2"



### How do I get set up? ###

* Developed on Visual Studio 2013 for Windows Desktop Update 4, Windows 8.1/10
* C# .NET solution with several projects (wpf/console applications and class library)
* .NET framework v4.5 (so it will not work on WinXP, WinServer2003 etc.)



### Who do I talk to? ###

* Repository owner or admin
mailto:dev.virgil@gmail.com

* Other community or team contact
http://www.ilandria.com


_______________________________________
This file was last edited on: 
2016-01-11