﻿
using System;
using System.IO;

namespace IncrementAssemblyVersion
{
    class Program
    {
        static void Main(string[] args)
        {
            string inFile, inComment;
            string version = string.Empty, versionNew = string.Empty;
            string[] lines;
            int verBuild;

            #region Register events

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            #endregion

            #region Check and validate input

            if (args.Length < 2)
            {
                About();
                return;
            }


            inFile = args[0];
            inComment = args[1];

            Console.WriteLine(string.Format("IncrementAssemblyVersion \"{0}\" \"{1}\"", inFile, inComment));

            if (string.IsNullOrEmpty(inFile))
                throw new ArgumentException("The input file argument is empty.");

            if (!File.Exists(inFile))
                throw new ArgumentException("The input file argument points to a non-existing file.");

            if (string.IsNullOrEmpty(inComment))
                throw new ArgumentException("The input comment argument is empty.");
            #endregion

            #region Read the assembly file

            lines = File.ReadAllLines(inFile);

            #endregion

            #region Get current version

            foreach (var line in lines)
            {
                if (line.StartsWith(inComment))
                {
                    version = line.Substring(inComment.Length);
                    break;
                }
            }

            if (string.IsNullOrEmpty(version))
                throw new InvalidOperationException("The assembly file doesn't contain the version comment symbol \"" + inComment + "\"");

            #endregion

            #region Get and increment Build version
            
            // if Version = 0.4.13 then new version = 0.4.14
            if (!int.TryParse(version.Substring(version.LastIndexOf(".") + 1), out verBuild))
                throw new InvalidOperationException("Unable to determine build version from \"" + version + "\"");

            versionNew = version;
            versionNew = versionNew.Substring(0, version.LastIndexOf(".") + 1) + (verBuild + 1).ToString();

            #endregion

            #region Update versions

            //Example of line with a version information to update
            //[assembly: AssemblyInformationalVersion("0.4.12")]

            int n = 0;
            foreach (var line in lines)
            {
                if (line.Contains(version) && (
                    line.Contains(inComment) ||
                    line.Contains("AssemblyVersion(") ||
                    line.Contains("AssemblyFileVersion(") ||
                    line.Contains("AssemblyInformationalVersion(")))
                { 
                    lines[n] = line.Replace(version, versionNew);
                }

                n++;
            }
            #endregion

            #region Save the assembly file
            File.WriteAllLines(inFile, lines);
            #endregion
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;

            Console.WriteLine(ex.GetType().FullName);
            Console.WriteLine(ex.Message);
            Console.ReadKey();
            Environment.Exit(-1);
        }

        static void About()
        {
            Console.WriteLine(
                "About IncrementAssemblyVersion:\n" +
                "Automatically increments versions in a assembly info file.\n" +
                "Searches for line:\n" +
                "[assembly: AssemblyVersion(\"0.4.12\")]\n" +
                "and increments last version member by one.\n\n" +

                "Usage:\n" +
                "IncrementAssemblyVersion \"path to assemblyVersion.cs\" \"comment containing version\"\n\n" +

                "Example:\n" +
                "IncrementAssemblyVersion \"c:/repo/my repository/AssemblyVersion.cs\" \"//VERSION=\""
            );

#if DEBUG
            Console.WriteLine("\n\n--- press a key to exit ---");
            Console.ReadKey();
#endif
        }
    }
}
