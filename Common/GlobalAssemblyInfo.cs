﻿using System.Reflection;



/*********** Global assembly info *************/

[assembly: AssemblyProduct("Ilandria 2")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("Copyright © Ilandria 2 2015")]
[assembly: AssemblyTrademark("")]

#if DEBUG
    [assembly: AssemblyConfiguration("Debug")]
#else
    [assembly: AssemblyConfiguration("Release")]
#endif

